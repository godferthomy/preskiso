var THREEx	= THREEx	|| {}

THREEx.createGrassTufts	= function(positions, cameraYAngle){
	// create the initial geometry
	cameraYAngle = cameraYAngle || 0;
	var baseHeight = 2;
	var geometry	= new THREE.PlaneGeometry(2, baseHeight);
	// var y = geometry.height/2;
	// geometry.applyMatrix( new THREE.Matrix4().makeRotationX( -Math.PI / 2 ) );
	geometry.applyMatrix( new THREE.Matrix4().makeTranslation( 0, baseHeight / 2, 0 ) );


	// Tweat the normal for better lighting
	// - normals from http://http.developer.nvidia.com/GPUGems/gpugems_ch07.html
	// - normals inspired from http://simonschreibt.de/gat/airborn-trees/
	geometry.faces.forEach(function(face){
		face.vertexNormals.forEach(function(normal){
			normal.set(0.0,1.0,0.0).normalize()
		})
	})
	
	// create each tuft and merge their geometry for performance
	var translateMatrix = new THREE.Matrix4();
	var rotateMatrix = new THREE.Matrix4();
	var scaleMatrix = new THREE.Matrix4();
	var mergedGeo	= new THREE.Geometry();
	for(var i = 0; i < positions.length; i++){
		var position	= positions[i]			
		// var baseAngle	= Math.PI*2*Math.random()
		var baseAngle	= cameraYAngle// - Math.PI/2*(0.5 - Math.random());

		var nPlanes	= 3;

		translateMatrix.makeTranslation( position.x, position.y, position.z );

		for(var j = 0; j < nPlanes; j++){
			var angle	= baseAngle+j*Math.PI /2/nPlanes
			// var angle	= baseAngle+j*Math.PI/nPlanes
			var heightScale = Math.random() * 1.8 + 0.2;
			var widthScale = Math.random() * 1.8 + 0.2;

			rotateMatrix.makeRotationY( angle );
			scaleMatrix.makeScale( widthScale, heightScale, 1);

			// First plane
			var geo = geometry.clone();
			geo.applyMatrix( scaleMatrix );
			geo.applyMatrix( rotateMatrix );
			geo.applyMatrix( translateMatrix );
			mergedGeo.merge( geo );
			// var object3d	= new THREE.Mesh(geometry, material)
			// object3d.rotateY(angle)
			// object3d.position.copy(position)
			// THREE.GeometryUtils.merge( mergedGeo, object3d );

			// The other side of the plane
			// - impossible to use ```side : THREE.BothSide``` as 
			//   it would mess up the normals

			rotateMatrix.makeRotationY( angle + Math.PI );
			scaleMatrix.makeScale( widthScale, heightScale, 1);

			var geo = geometry.clone();
			geo.applyMatrix( scaleMatrix );
			geo.applyMatrix( rotateMatrix );
			geo.applyMatrix( translateMatrix );
			mergedGeo.merge( geo );
			// var object3d	= new THREE.Mesh(geometry, material)
			// object3d.rotateY(angle+Math.PI)
			// object3d.position.copy(position)
			// THREE.GeometryUtils.merge( mergedGeo, object3d );
		}
	}


	// load the texture
	var textureUrl	= THREEx.createGrassTufts.baseUrl+'images/grass01.png'
	var texture	= THREE.ImageUtils.loadTexture(textureUrl)
	// build the material
	// var material	= new THREE.MeshPhongMaterial({
	// 	map		: texture,
	// 	color		: 0x555555,
	// 	emissive	: 0x5d5f3f,
	// 	alphaTest	: 0.7,
	// })
	var material	= new THREE.MeshLambertMaterial({
		map		: texture,
		color		: 0x555555,
		emissive	: 0x5d5f3f,
		alphaTest	: 0.7,
	})
	// create the mesh
	var mesh	= new THREE.Mesh(mergedGeo, material)
	return mesh	
	return new THREE.Mesh(
		new THREE.BoxGeometry(1, 1, 1),
		material
	);
}

THREEx.createGrassTufts.baseUrl	= "";


