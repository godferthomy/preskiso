// This THREEx helper makes it easy to handle window resize.
// It will update renderer and camera when window is resized.
//
// # Usage
//
// **Step 1**: Start updating renderer and camera
//
// ```var windowResize = THREEx.WindowResize(aRenderer, aCamera)```
//    
// **Step 2**: Start updating renderer and camera
//
// ```windowResize.stop()```
// # Code

//

/** @namespace */
var THREEx	= THREEx 		|| {};

/**
 * Update renderer and camera when the window is resized
 * 
 * @param {Object} renderer the renderer to update
 * @param {Object} Camera the camera to update
*/
THREEx.WindowResize	= function(renderer, camera, container, ratio){
	var callback	= function(){
		// notify the renderer of the size change
		var width = window.innerWidth, height = window.innerHeight;
		if (!isNaN(ratio)) {
			var actualRatio = width / height;
			var ratioDiff = actualRatio / ratio;
			if (ratioDiff > 1) {
				width /= ratioDiff;
			} else if (ratioDiff < 1) {
				height /= ratioDiff;
			}
		}
		renderer.setSize( width, height );
		container.style.height = height + "px";
		container.style.width = width + "px";
		// update the camera
		camera.aspect	= width / height;
		camera.updateProjectionMatrix();
	}
	// bind the resize event
	window.addEventListener('resize', callback, false);
	// return .stop() the function to stop watching window resize
	callback();
	return {
		/**
		 * Stop watching window resize
		*/
		stop	: function(){
			window.removeEventListener('resize', callback);
		}
	};
}
