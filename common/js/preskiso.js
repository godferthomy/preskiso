var PRESKISO = {
	Constants: {
		KeyboardGamepadId: -1000,
		PlayerModels: [
			"classic",
			"cool",
			"girl",
			"papy"
		],
		PlayerColors: [
			{r: 0.89, g: 0.59, b: 0.12},
			{r: 0.22, g: 0.97, b: 0.67},
			{r: 1, g: 0, b: 0},
			{r: 1, g: .5, b: 0},
			{r: 1, g: 1, b: 0},
			{r: 0, g: 1, b: 0},
			{r: 0, g: 1, b: .5},
			{r: 0, g: 1, b: 1},
			{r: 0, g: 0, b: 1},
			{r: .5, g: 0, b: 1},
			{r: 1, g: 0, b: 1},
		],
		GameModes: [
			{
				id: 0,
				name: "Zombie Invasion",
				minTeams: 1,
				maxTeams: 2,
				maps: [
					"default", "big"
				],
				params: [
					{name: "ZI_maxZombies", "visible_name": "Maximum Zombies", type:"range", from: 5, to: 30, step: 5},
					{name: "ZI_difficulty", "visible_name": "Difficulty", type:"range", from: 1, to: 5},
					{name: "friendlyFire", "visible_name": "Friendly Fire", type:"boolean"},
				]
			},
			{
				id: 1,
				name: "Death Match",
				minTeams: 2,
				maxTeams: 4,
				maps: [
					"default"
				],
				params: [
					{name: "friendlyFire", "visible_name": "Friendly Fire", type:"boolean"},
				]
			},
			{
				id: 2,
				name: "Capture the Flag",
				minTeams: 2,
				maxTeams: 4,
				maps: [
					"default"
				],
				params: [
					{name: "friendlyFire", "visible_name": "Friendly Fire", type:"boolean"},
				]
			},
			{
				id: 3,
				name: "Capture the Pigs",
				minTeams: 1,
				maxTeams: 4,
				maps: [
					"default"
				],
				params: [
					{name: "CTP_difficulty", "visible_name": "Difficulty", type:"range", from: 1, to: 5},
					{name: "friendlyFire", "visible_name": "Friendly Fire", type:"boolean"},
				]
			},
		],
		Maps: [
			{
				id: 0,
				name: "Default",
				path: "default",
				gamemodes: [0, 1, 2, 3],
				maxTeams: 3,
			},
			{
				id: 1,
				name: "Big",
				path: "big",
				gamemodes: [0],
				maxTeams: 1,
			},
			{
				id: 2,
				name: "Tunnel",
				path: "tunnel",
				gamemodes: [0, 3],
				maxTeams: 1,
			},
			{
				id: 3,
				name: "Empty",
				path: "empty",
				gamemodes: [0, 3],
				maxTeams: 1,
			},
		],

	}
};
