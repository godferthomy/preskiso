var PRESKISO = PRESKISO || {};
PRESKISO.SettingsManager = (function () {
	function SettingsManager() {
		this.getItem = function (key) {
			return localStorage.getItem(key);
		}
		this.setItem = function (key, value) {
			return localStorage.setItem(key, value);
		}
		this.removeItem = function (key) {
			return localStorage.removeItem(key);
		}
		this.clear = function () {
			return localStorage.clear();
		}

		this.getGameSetting = function (key) {
			return this.getItem("GS_" + key);
		}
		this.setGameSetting = function (key, value) {
			return this.setItem("GS_" + key, value);
		}
	}
	var singleton = new SettingsManager();

	if (singleton.getItem("init") !== "OK") {
		singleton.setItem("init", "OK"); // no need to reset if this is present
		singleton.setItem("masterVolume", 0.5);
	}

	return singleton;
})();
