PRESKISO.MenuNavigationController = (function () {
	var menuNavigationControllerInstance;

	function MenuNavigationController() {
		this.currentMenuController = undefined;
		this.menuControllers = {};
	}
	MenuNavigationController.prototype.init = function (menuControllers) {
		this.menuControllers = {};
		for (var i = 0; i < menuControllers.length; i++) {
			this.menuControllers[menuControllers[i].id] = menuControllers[i];
		}
		this.showMenu(menuControllers[0].id);

		gamepad.bind(Gamepad.Event.CONNECTED, gamepadConnected);
		gamepad.bind(Gamepad.Event.DISCONNECTED, gamepadDisconnected);
	}

	MenuNavigationController.prototype.listen = function () {
		window.addEventListener("keydown", onKeydown);
		gamepad.bind(Gamepad.Event.BUTTON_DOWN, gamepadButtonDown);
	}

	MenuNavigationController.prototype.stopListening = function () {
		gamepad.unbind(Gamepad.Event.BUTTON_DOWN, gamepadButtonDown);
	}

	MenuNavigationController.prototype.showMenu = function (id) {
		if (this.currentMenuController != undefined) {
			this.currentMenuController.hide();
			// if ($showingMenu.attr("id") == "playerpick_menu") {
			// 	cancelPlayerPicking();
			// }
		}
		this.currentMenuController = this.menuControllers[id];
		this.currentMenuController.show();

		// if ($showingMenu.attr("id") == "playerpick_menu") {
		// 	initiatePlayerPicking();
		// }
	}

	MenuNavigationController.prototype.restrictToGamepadId = function (gamepadIndex) {
		this.gamepadIdRestriction = gamepadIndex;
	}

	MenuNavigationController.prototype.onGamepadButtonDown = function (e) {
		window.console.log(this.gamepadIdRestriction, e.gamepad.index);
		if (this.gamepadIdRestriction == undefined || this.gamepadIdRestriction == e.gamepad.index) {
			this.currentMenuController.onGamepadButtonDown(e);
		}
	}

	MenuNavigationController.prototype.onKeyDown = function (e) {
		window.console.log(this.gamepadIdRestriction, -1);
		if (this.gamepadIdRestriction == undefined || this.gamepadIdRestriction == PRESKISO.Constants.KeyboardGamepadId) {
			this.currentMenuController.onKeyDown(e);
		}
	}

	function gamepadConnected(gamepad) {
		for (var i = 0; i < gamepads.length; i++) {
			if (gamepads[i].index == gamepad.index) {
				return;
			}
		}
		gamepads.push(gamepad);
	}

	function gamepadDisconnected(gamepad) {
		for (var i = 0; i < gamepads.length; i++) {
			if (gamepads[i].index == gamepad.index) {
				gamepads.splice(i, 1);
				return;
			}
		}
	}

	function gamepadButtonDown(e) {
		menuNavigationControllerInstance.onGamepadButtonDown(e);
	}

	function onKeydown(e) {
		menuNavigationControllerInstance.onKeyDown(e);
	}

	menuNavigationControllerInstance = new MenuNavigationController();
	return menuNavigationControllerInstance;
})();
