if (window.hasOwnProperty('AudioContext') && !window.hasOwnProperty('webkitAudioContext'))
    window.webkitAudioContext = AudioContext;
var PRESKISO = PRESKISO || {};
PRESKISO.AudioManager = (function () {
	var nextSoundId = 0;
	function AudioManager() {
		this.rootFolder = "sounds/";
		this.time = 0;
		var buffers = {};

		var resources = [];
		var multipleSources = {};


		var ctx = new webkitAudioContext();
		var listenerObject;
		var sounds = [];

		var initCallback, preloadLeftCount = 0;

		var masterVolume = PRESKISO.SettingsManager.getItem("masterVolume");

		var compressor = ctx.createDynamicsCompressor();
		// Create a AudioGainNode to control the main volume.
		var mainVolume = ctx.createGain();
		// Connect the main volume node to the context destination.
		mainVolume.connect(compressor);
		mainVolume.gain.value = masterVolume;

		var filter = ctx.createBiquadFilter();
		compressor.connect(filter);
		filter.connect(ctx.destination);
		filter.type = 0;
		filter.frequency.value = 100000;

		var ongoingSounds = [];
		var ongoingLoopedSounds = [];

		// filter.frequency.value = 440;


		// compressor.connect(ctx.destination);

		this.preload = function () {
			// setTimeout(initCallback, 0);
			// return;
			for (var i = 0; i < resources.length; i++) {
				preloadLeftCount += this.getSoundFilesNames(resources[i]).length;
				this.preloadSound(resources[i]);
			}
		}

		this.preloadSound = function (name) {
			var fileNames = this.getSoundFilesNames(name);
	        buffers[name] = [];
	        for (var i = 0; i < fileNames.length; i++) {
	        	var file = fileNames[i];
				var request = new XMLHttpRequest();
				// window.console.log("Preloading " + file + "...");
				request.open("GET", file, true);
		  		request.responseType = "arraybuffer";
				var index = i;
				(function (_this, _request, _name, _index) {
					var _buffer;
					request.onload = function(res) {
						var audioData = _request.response;
						ctx.decodeAudioData(audioData, function(buffer) {
					        buffers[_name][_index] = buffer;
						    preloadLeftCount--;
					    	if (typeof detailCallback === "function") {
					    		detailCallback();
					    	}
						    if (preloadLeftCount == 0 && typeof initCallback === "function") {
						    	initCallback();
						    }
					    });
					}
				})(this, request, name, index);
				request.send();
			}
		}

		this.init = function (_resources, _multipleSources, callback, dCallback) {
			resources = _resources;
			multipleSources = _multipleSources;
			initCallback = callback;
			detailCallback = dCallback;
			this.preload();
		}

		var getSoundBuffer = function (resourceName) {
			var resourceBuffers = buffers[resourceName];
			var index = Math.floor(Math.random() * resourceBuffers.length);
			return resourceBuffers[index];
		}

		this.setMasterVolume = function (value) {
			value = Math.max(0, Math.min(1, value));
			masterVolume = value;
			mainVolume.gain.value = value;
		}

		this.pause = function () {
			this.time = PRESKISO.GameController.getCurrentTime();
			for (var i = 0, il = ongoingSounds.length; i < il; i++) {
				var sound = ongoingSounds[i];
				sound.source.stop();
				sound.playedTime = (this.time - sound.startTime) / sound.playbackRate;
			}
			for (var i = 0, il = ongoingLoopedSounds.length; i < il; i++) {
				var sound = ongoingLoopedSounds[i];
				sound.source.stop();
				sound.playedTime = ((this.time - sound.startTime) / sound.playbackRate) % sound.sourceBuffer.duration;
			}
		}

		this.resume = function () {
			this.time = PRESKISO.GameController.getCurrentTime();
			for (var i = 0, il = ongoingSounds.length; i < il; i++) {
				this.resumeSound(ongoingSounds[i]);
			}
			for (var i = 0, il = ongoingLoopedSounds.length; i < il; i++) {
				this.resumeSound(ongoingLoopedSounds[i]);
			}
		}

		this.mute = function () {
			mainVolume.gain.value = 0;
		}

		this.unmute = function () {
			mainVolume.gain.value = masterVolume;
		}

		this.getMasterVolume = function (value) {
			return mainVolume.gain.value;
		}

		this.setListener = function (listenerObj) {
			listenerObject = listenerObj;
		}
		this.tick = function (delta, time) {
			this.time = time;
			ctx.listener.setPosition(listenerObject.position.x, listenerObject.position.y, listenerObject.position.z);
			var orientation = new THREE.Vector3(0, 0, -1);
			listenerObject.localToWorld(orientation);
			orientation.sub(listenerObject.position);
			var up = new THREE.Vector3(0, 1, 0);
			listenerObject.localToWorld(up);
			up.sub(listenerObject.position);
			ctx.listener.setOrientation(orientation.x, orientation.y, orientation.z, up.x, up.y, up.z);

			for (var i = 0; i < sounds.length; i++) {
				sounds[i].tick(delta);
			}
			while (ongoingSounds.length > 0 && ongoingSounds[0].endTime <= time) {
				ongoingSounds.splice(0, 1);
			}
		}
		// this.getSoundBuffer = function (resource)
		this.getSoundFileName = function (resource) {
			if (multipleSources[resource] !== undefined) {
				return "sounds/" + resource + Math.floor(Math.random() * multipleSources[resource]) + ".mp3";
			} else {
				return "sounds/" + resource + ".mp3";
			}
		}
		this.getSoundFilesNames = function (resource) {
			var array = [];
			if (multipleSources[resource] !== undefined) {
				for (var i = 0; i < multipleSources[resource]; i++) {
					array.push(this.rootFolder + resource + i + ".mp3");
				}
			} else {
				array.push(this.rootFolder + resource + ".mp3");
			}
			return array;
		}

		this.addObjectSound = function (object, resource, volume) {
			var sound = new ObjectSound(ctx, object, this.getSoundFileName(resource), volume);

			// Hook up the sound volume control to the main volume.
			sound.getOutputNode().connect(mainVolume);

			sounds.push(sound);
		}

		this.playSound = function (position, resource, volume, options) {
			options = options || {};
			if (volume === undefined) {
				volume = 1;
			}

			var endPoint = mainVolume;
			if (options.channel == "music") {
				endPoint = mainVolume
			}

			var sound = {
				id: nextSoundId++
			};
			// Create an object with a sound source and a volume control.
			sound.source = ctx.createBufferSource();
			sound.volume = ctx.createGain();
			sound.volume.gain.value = volume;

			// Connect the sound source to the volume control.
			sound.source.connect(sound.volume);

			if (position !== undefined && position instanceof THREE.Vector3) {
				sound.panner = ctx.createPanner();
				sound.panner.setPosition(position.x, position.y, position.z);
				sound.volume.connect(sound.panner);
				// Hook up the sound volume control to the main volume.
				sound.panner.connect(endPoint);
			} else {
				sound.volume.connect(endPoint);
			}

			// Make the sound source loop.
			sound.loop = sound.source.loop = !!options.loop;

			if (options.randomizePlaybackRateBy !== undefined) {
				sound.source.playbackRate.value = 1 + (Math.random() - 0.5) * options.randomizePlaybackRateBy;
			}
			sound.playbackRate = sound.source.playbackRate.value;

			sound.source.buffer = getSoundBuffer(resource);
			sound.sourceBuffer = sound.source.buffer;


			sound.startTime = this.time;
			sound.endTime = sound.source.buffer.duration / sound.playbackRate + sound.startTime;
			this.soundStarted(sound);

			sound.source.start(0);
			return sound;
		}

		this.resumeSound = function (sound) {
			window.console.log(sound);
			sound.source.disconnect(0);
			sound.source = ctx.createBufferSource();
			sound.source.connect(sound.volume);
			sound.source.loop = sound.loop;
			sound.source.playbackRate.value = sound.playbackRate;
			sound.source.buffer = sound.sourceBuffer;
			sound.source.start(0, sound.playedTime);
		}

		this.soundStarted = function (sound) {
			if (sound.loop) {
				ongoingLoopedSounds.push(sound);
			} else {
				var time = sound.endTime;
				var i, il;
				for (i = 0, il = ongoingSounds.length; i < il; i++) {
					if (ongoingSounds[i].endTime > time) {
						ongoingSounds.splice(i, 0, sound);
						break;
					}
				}
				if (i == il) {
					// we reached the end
					ongoingSounds.push(sound);
				}
			}
		}

		this.cancelSound = function (sound) {
			var array = sound.loop ? ongoingLoopedSounds : ongoingSounds;
			PRESKISO.Util.arrayRemoveObjectWithAttribute(array, "id", sound.id);
			sound.source.stop();
		};



		var ObjectSound = function ( ctx, object, resource, volume ) {
			// Create an object with a sound source and a volume control.
			this.source = ctx.createBufferSource();
			this.volume = ctx.createGain();
			this.volume.gain.value = volume | 1;
			this.panner = ctx.createPanner();

			// Connect the sound source to the volume control.
			this.source.connect(this.volume);

			this.volume.connect(this.panner);

			// Make the sound source loop.
			this.source.loop = true;

			// Load a sound file using an ArrayBuffer XMLHttpRequest.
			// var that = this;
			// var request = new XMLHttpRequest();
			// request.open("GET", soundFileName, true);
			// request.responseType = "arraybuffer";
			// request.onload = function() {
			// 	var audioData = request.response;

			// 	ctx.decodeAudioData(audioData, function(buffer) {
			//         myBuffer = buffer;
			//         that.source.buffer = myBuffer;
			//     });
			// }
			// request.send();

			this.source.buffer = getSoundBuffer(resource);
			this.source.start();

			this.getOutputNode = function () {
				return this.panner;
			}
			this.tick = function ( delta ) {
				this.panner.setPosition(object.position.x, object.position.y, object.position.z);
				return;
			}
		}
	}
	return new AudioManager();
})();
