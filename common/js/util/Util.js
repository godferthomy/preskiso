var PRESKISO = PRESKISO || {};

PRESKISO.Util = ( function () {
	var Util = {};
	Util.inherit = function( targetClass, parentClassOrObject ){
		if ( parentClassOrObject.constructor == Function ) 
		{ 
			targetClass.prototype = Object.create(parentClassOrObject.prototype);
			targetClass.prototype.constructor = targetClass;
			targetClass.prototype.parent = parentClassOrObject.prototype;
		} 
		else 
		{ 
			//Pure Virtual Inheritance 
			targetClass.prototype = parentClassOrObject;
			targetClass.prototype.constructor = targetClass;
			targetClass.prototype.parent = parentClassOrObject;
		}
		return targetClass;
	}

	Util.arrayObjectIndexOf = function (myArray, property, searchTerm) {
	    for(var i = 0, len = myArray.length; i < len; i++) {
	        if (myArray[i][property] === searchTerm) return i;
	    }
	    return -1;
	}

	Util.arrayRemoveObjectWithAttribute = function (myArray, property, searchTerm) {
		var index;
		if ((index = Util.arrayObjectIndexOf(myArray, property, searchTerm)) !== -1) {
			myArray.splice(index, 1);
		}
	}

	Util.intersectHittableObjects = function (raycaster, objects, recursive) {
		var intersects = raycaster.intersectObjects(objects, recursive);
		var result = [];
		for (var i = 0, maxI = intersects.length; i < maxI; i++) {
			if (intersects[i].object.isHittable) {
				result.push(intersects[i]);
			}
		}
		return result;
	}

	Util.intersectFirstHittableObject = function (raycaster, objects, recursive, ignoreObjectsById) {
		ignoreObjectsById = ignoreObjectsById || []
		var intersects = raycaster.intersectObjects(objects, recursive);
		var result = [];
		for (var i = 0, maxI = intersects.length; i < maxI; i++) {
			if (intersects[i].object.isHittable && ignoreObjectsById.indexOf(intersects[i].object.id) == -1) {
				return intersects[i];
			}
		}
		return undefined;
	}

	var getHittableDescendants = function (object, result) {
		if (object.isHittable) {
			result.push(object);
		}
		for (var i = 0; i < object.children.length; i++) {
			var obj = object.children[i];
			getHittableDescendants(obj, result);
		}
	};

	Util.getHittableObjects = function (object) {
		var result = [];
		getHittableDescendants(object, result);
		return result;
	}

	Util.get3dToMapPosition = function (position, map, ratio, freeOnly) {
        var x = Math.max(Math.min(position.x / ratio, map.length - 1), 0);
        var y = Math.max(Math.min(position.z / ratio, map[0].length - 1), 0);
        if (freeOnly && map[Math.round(x)][Math.round(y)] == 0) {
            for (var xOffset = -0.5; xOffset <= 0.5; xOffset+= 0.5) {
                for (var yOffset = -0.5; yOffset <= 0.5; yOffset+= 0.5) {
                    if (map[Math.round(x+xOffset)][Math.round(y+yOffset)] != 0) {
                        x = Math.round(x+xOffset);
                        y = Math.round(y+yOffset);
                        yOffset = xOffset = 1;
                    }
                }
            }
        } else {
            x = Math.round(x);
            y = Math.round(y);
        }
        return {
            x: x,
            y: y
        };
    }

    Util.cloneMaterial = function (material) {
    	window.console.log("CLONING", material);
    	var newMaterial = material.clone();
    	if (material._physijs !== undefined) {
    		newMaterial = Physijs.createMaterial(newMaterial, material._physijs.friction, material._physijs.restitution);
    	}
    	newMaterial.impactSound = material.impactSound;
    	return newMaterial;
    }

    Util.setMaterialField = function (material, attribute, value) {
    	if (material instanceof THREE.MeshFaceMaterial) {
    		for (var i = 0; i < material.materials.length; i++) {
    			material.materials[i][attribute] = value;
    		}
    	} else {
    		material[attribute] = value;
    	}
    }

    Util.setCollisionGroup = function (object, group, mask) {
    	object._physijs.collision_type = PRESKISO.CollisionGroups[group];
    	object._physijs.collision_masks = (mask === undefined) ? PRESKISO.CollisionMasks[group] : PRESKISO.CollisionMasks[mask];
    }

    return Util;
})();
