var PRESKISO = PRESKISO || {};
PRESKISO.Loader = (function () {
    function Loader() {
        this.loader = new THREE.JSONLoader();
        this.resources = {};
        this.toLoad = [];
        this.loading = [];

        this.setRoot = function (root) {
            this.root = root;
        }

        this.queueObject = function (name) {
            this.toLoad.push(name);
        }

        this.queueObjects = function (names) {
            for (var i = 0; i < names.length; i++) {
                this.queueObject(names[i]);
            }
        }

        this.load = function (loadCallback, modelLoadCallback) {
            this.loadCallback = loadCallback;
            this.modelLoadCallback = modelLoadCallback;
            while (this.toLoad.length > 0) {
                var name = this.toLoad.pop();
                this.loadModel(name);
            }
        }

        this.loadModel = function (name) {
            var that = this;
            this.loading.push(name);
            this.loader.load([this.root, name, ".js"].join(""), function (geometry, materials) {
                that.modelLoaded(name, arguments);
            });
        }

        this.modelLoaded = function (name, data) {
            var resource = this.resources[name] = {};
            // resource.geometry = new THREE.BoxGeometry(1, 1, 1);
            // for(var z = 0; z< resource.geometry.faces.length; z++)
            //     resource.geometry.faces[z].materialIndex = 0;
            // resource.materials = [new THREE.MeshBasicMaterial()];
            resource.geometry = data[0];
            if (data[1] !== undefined) {
                resource.materials = data[1];
                resource.material = new THREE.MeshFaceMaterial(resource.materials);
            }
            this.loading.splice(this.loading.indexOf(name), 1);

            // if (typeof this.modelLoadCallback === "function") {
            //     this.modelLoadCallback({
            //         "left": this.loading.length
            //     });
            // }

            if (this.loading.length === 0 && typeof(this.loadCallback) === "function") {
                this.loadCallback();
            }
        }

        this.getResource = function (name) {
            return this.resources[name] || undefined;
        }
    };
    return new Loader();
})();
