PRESKISO.MenuController = (function () {
	var MENU_DIRECTIONS = {
		left: 0,
		top: 1,
		right: 2,
		bottom: 3,
	};
	var MENU_DIRECTIONS_DATA_NAMES = [
		"toleft",
		"totop",
		"toright",
		"tobottom",
	];
	var IS_BACK_ITEM_ATTRIBUTE = "isbackitem";
	var SUBMENU_ATTRIBUTE = "submenu";

	function MenuController($el) {
		if ($el) {
			this.id = $el.attr("id");
			this.$el = $el;
			this.backMenuId = this.$el.data("backmenu");
			this.navigation = {};
		}
	}

	MenuController.prototype.init = function (items) {
		if (items != undefined && items.length > 0) {
			for (var i = 0; i < items.length; i++) {
				this.$el.append(this.createItem(items[i].id, items[i].content, items[i].options));
			}
			this.assignBasicNavigation();
		}
		this.initNavigation();
	}

	MenuController.prototype.createItem = function (id, content, options) {
		options = options || {};
		id = this.id + "_" + id;
		var $el = $('<li id="' + id + '" class="menu_item">' + content + '</li>');
		if (options.submenu) {
			$el.data(SUBMENU_ATTRIBUTE, options.submenu);
		}
		if (options.isBackItem) {
			$el.data(IS_BACK_ITEM_ATTRIBUTE, true);
		}
		return $el;
	}

	MenuController.prototype.assignBasicNavigation = function () {
		var $children = this.$el.find(".menu_item");
		for (var i = 0; i < $children.length; i++) {
			var previous = i - 1;
			if (previous < 0) {
				previous = $children.length - 1;
			}
			var next = (i + 1) % $children.length;
			$children.eq(i).data({totop: $children[previous].id, tobottom: $children[next].id});
		}
	}

	MenuController.prototype.initNavigation = function () {
		var navigation = this.navigation = {};
		$menuEl = this.$el;
		var that = this;
		this.$menuItems = this.$el.find(".menu_item");
		this.$menuItems.each(function (index) {
			var id = $(this)[0].id;
			var itemNavigation = navigation[id] = {};
			for (var i = 0; i < 4; i++) {
				var neighborId = $(this).data(MENU_DIRECTIONS_DATA_NAMES[i]);
				if (neighborId) {
					itemNavigation[i] = $menuEl.find("#" + neighborId);
				}
			}
			if ($(this).data(IS_BACK_ITEM_ATTRIBUTE)) {
				itemNavigation[IS_BACK_ITEM_ATTRIBUTE] = true;
			}
			itemNavigation[SUBMENU_ATTRIBUTE] = $(this).data(SUBMENU_ATTRIBUTE);
		});
		window.console.log(this.navigation);
	}

	MenuController.prototype.show = function () {
		this.$el.parent().append(this.$el); // put back on top of the container
		this.$el.stop().fadeIn(300);
		if (this.$menuItems.length > 0) {
			$selection_outliner.show();
			if (this.$selectedMenuItem == undefined) {
				this.selectMenuItem(this.$menuItems.eq(0));
			} else {
				this.selectMenuItem(this.$selectedMenuItem);
			}
		} else {
			$selection_outliner.hide();
		}
	}

	MenuController.prototype.hide = function () {
		this.$el.stop().fadeOut(400);
	}

	MenuController.prototype.selectMenuItem = function ($el) {
		var position = $el.offset();
		var width = $el.outerWidth();
		var height = $el.outerHeight();
		var values = {
			width: width,
			height: height,
			top: position.top,
			left: position.left
		};
		// if ($selectedMenuItem == undefined) {
		// 	$selection_outliner.css(values);
		// } else {
		$selection_outliner.stop().animate(values, 100, "swing");
		// }
		this.$selectedMenuItem = $el;
	}

	MenuController.prototype.navigateTowards = function (direction) {
		if (this.$selectedMenuItem == undefined) {
			return;
		}
		var $target = this.navigation[this.$selectedMenuItem.attr("id")][direction];
		if ($target != undefined) {
			PRESKISO.AudioManager.playSound(undefined, "../../common/sounds/navigate_menu", 1);
			this.selectMenuItem($target);
		} else {
			if (direction == MENU_DIRECTIONS.left) {
				this.addMenuItem(-1);
			} else if (direction == MENU_DIRECTIONS.right) {
				this.addMenuItem(1);
			}
		}
	}

	MenuController.prototype.activateMenuItem = function ($el) {
		$el = $el || this.$selectedMenuItem;
		if ($el !== undefined) {
			var itemNavigation = this.navigation[this.$selectedMenuItem.attr("id")];
			if (itemNavigation[IS_BACK_ITEM_ATTRIBUTE]) {
				this.goToBackMenu();
			} else if (itemNavigation[SUBMENU_ATTRIBUTE]) {
				var submenu = itemNavigation[SUBMENU_ATTRIBUTE];
				this.goToMenu(itemNavigation[SUBMENU_ATTRIBUTE]);
			} else {
				var $valueItem = $el.find(".menu_item_value");
				if ($valueItem) {
					if ($valueItem.is("input[type='checkbox']")) {
						$valueItem.prop("checked", !$valueItem.prop("checked"));
						$valueItem.change();
					}
				}
			}
		}
	}

	MenuController.prototype.addMenuItem = function (value, $el) {
		$el = $el || this.$selectedMenuItem;
		var $valueItem = $el.find(".menu_item_value");
		if ($valueItem) {
			var value = parseFloat($valueItem.val()) + value * parseFloat($valueItem.attr("step"))
			$valueItem.val(value);
			$valueItem.change();
		}
	}

	MenuController.prototype.goToMenu = function (menuId) {
		var sound = (menuId != this.backMenuId) ? "../../common/sounds/open_menu" : "../../common/sounds/close_menu";
		PRESKISO.AudioManager.playSound(undefined, sound, 1);
		PRESKISO.MenuNavigationController.showMenu(menuId);
	}

	MenuController.prototype.goToBackMenu = function () {
		if (this.backMenuId != undefined) {
			this.goToMenu(this.backMenuId);
			this.$selectedMenuItem = undefined;
		}
	}

	MenuController.prototype.onGamepadButtonDown = function (e) {
		switch (e.control) {
			case "DPAD_UP":
				this.navigateTowards(MENU_DIRECTIONS.top);
				break;
			case "DPAD_DOWN":
				this.navigateTowards(MENU_DIRECTIONS.bottom);
				break;
			case "DPAD_LEFT":
				// if (!isPlayerPicking) {
					this.navigateTowards(MENU_DIRECTIONS.left);
				// } else {
				// 	movePlayerSide(e.gamepad, -1)
				// }
				break;
			case "DPAD_RIGHT":
				// if (!isPlayerPicking) {
					this.navigateTowards(MENU_DIRECTIONS.right);
				// } else {
				// 	movePlayerSide(e.gamepad, 1)
				// }
				break;
			case "FACE_1":
				this.activateMenuItem();
				break;
			case "FACE_2":
				this.goToBackMenu();
				break;
			case "RIGHT_TOP_SHOULDER":
				this.addMenuItem(1);
				break;
			case "LEFT_TOP_SHOULDER":
				this.addMenuItem(-1);
				break;
			case "START_FORWARD":
				// if (isPlayerPicking) {
				// 	startGame();
				// }
				break;
		}
	}

	MenuController.prototype.onKeyDown = function (e) {
		switch (event.code) {
			case "ArrowUp":
				this.navigateTowards(MENU_DIRECTIONS.top);
				break;
			case "ArrowDown":
				this.navigateTowards(MENU_DIRECTIONS.bottom);
				break;
			case "ArrowLeft":
					this.navigateTowards(MENU_DIRECTIONS.left);
				break;
			case "ArrowRight":
					this.navigateTowards(MENU_DIRECTIONS.right);
				break;
			case "Enter":
				this.activateMenuItem();
				break;
			case "Backspace":
				this.goToBackMenu();
				break;
			case "P":
				this.addMenuItem(1);
				break;
			case "M":
				this.addMenuItem(-1);
				break;
		}
	}

	return MenuController;
})();
