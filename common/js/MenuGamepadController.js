var MenuGamepadController = (function () {


	function gamepadsChanged(gamepads) {
		for (var i = gamepadControllers.length; i < gamepads.length; i++) {
			for (var j = 0; j < DEBUG_MULTIPLY_PLAYERS; j++) {
				var gamepadController = new PRESKISO.GamepadController(player, i, gamepads[i]);
				gamepadControllers[i * DEBUG_MULTIPLY_PLAYERS + j] = gamepadController;
			}
		}
	}

	function gamepadChanged(gamepadId, gamepad) {
		newState = {axes: [], buttons: []};
		for (var i = 0; i < gamepad.axes.length; i++) {
			var axeValue = 0;
			if (Math.abs(gamepad.axes[i]) > THRESHOLD) {
				axeValue = gamepad.axes[i];
				if (axeValue > 0) {
					axeValue -= THRESHOLD;
				} else {
					axeValue += THRESHOLD;
				}
				axeValue /= THRESHOLDINV;
			}
			newState.axes[i] = axeValue;
		}
		for (var i = 0; i < gamepad.buttons.length; i++) {
			newState.buttons[i] = {
				pressed: gamepad.buttons[i].pressed,
				value: gamepad.buttons[i].value
			};
		}
		if (previousState !== undefined) {
			compareStates(previousState, newState);
		}
		previousState = newState;
	}

	function compareStates(previousState, newState) {
		for (var i = 0; i < newState.buttons.length; i++) {
			if (newState.buttons[i].pressed && !previousState.buttons[i].pressed) {
				buttonDown(i);
			} else if (!newState.buttons[i].pressed && previousState.buttons[i].pressed) {
				buttonUp(i);
			}
		}
	}

	var buttonDown = function (buttonId) {
		// switch (buttonId) {
		// 	case 0:
		// 		// A
		// 		player.jump();
		// 		break;
		// 	case 1:
		// 		// B
		// 		player.prepareGrenade();
		// 		// player.rush();
		// 		break;
		// 	case 2:
		// 		// X
		// 		player.isInteracting = true;
		// 		break;
		// 	case 3:
		// 		// X
		// 		player.startReloading();
		// 		break;
		// 	case 4:
		// 		// LB
		// 		player.toggleRotationMode();
		// 		player.isSniping = true;
		// 		break;
		// 	case 5:
		// 		// RB
		// 		player.isShooting = true;
		// 		break;
		// 	case 7:
		// 		// RT
		// 		break;
		// 	case 9:
		// 		// START
		// 		gameController.playPause(gamepadId);
		// 		break;
		// 	case 10:
		// 		// LJOY
		// 		player.toggleMovementMode();
		// 		break;
		// 	case 11:
		// 		// RJOY
		// 		player.toggleRotationMode(true);
		// 		break;
			case 12:
				// DPAD UP
				navigateTowards(MENU_DIRECTIONS.TOP);
				break;
			case 13:
				// DPAD DOWN
				navigateTowards(MENU_DIRECTIONS.BOTTOM);
				break;
			case 14:
				// DPAD LEFT
				navigateTowards(MENU_DIRECTIONS.LEFT);
				break;
			case 15:
				// DPAD RIGHT
				navigateTowards(MENU_DIRECTIONS.RIGHT);
				break;
		}
	}

	var buttonUp = function (buttonId) {
		// switch (buttonId) {
		// 	case 1:
		// 		// B
		// 		player.throwGrenade();
		// 		break;
		// 	case 4:
		// 		// LB
		// 		player.toggleRotationMode();
		// 		player.isSniping = false;
		// 		break;
		// 	case 5:
		// 		// RB
		// 		player.isShooting = false;
		// 		break;
		// 	case 7:
		// 		// RT
		// 		break;
		// }
	}

	MenuGamepadController = {
		init: function () {
			gamepadSupport.setGamepadsObserver(gamepadsChanged);
			gamepadSupport.init();
		}
	}

	return MenuGamepadController;
})();
