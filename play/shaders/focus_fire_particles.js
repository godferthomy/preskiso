var focusFireParticlesShader = {
    vertex: [
        "uniform float baseSize;",
        "uniform float baseAlpha;",
        "uniform float time;",
        "uniform float lifeSpan;",
        "attribute float size;",
        "attribute float alpha;",
        "attribute float birth;",
        "attribute vec3 direction;",

        "varying float vAlpha;",
        "varying float vAge;",

        "void main() {",
            "vAge = (time - birth) / lifeSpan;",
            "vAlpha = baseAlpha * alpha * min(vAge * 5.0, 1.0 - vAge);",

            "vec3 newPosition = vec3(position.x + sin(position.x + time * 7.0) / size / 20.0, position.y, position.z + cos(position.y + time * 5.0) / size / 20.0) + direction * vAge / 5.0 * (1.0 - size / 2.0);",
            "vec4 mvPosition = modelViewMatrix * vec4( newPosition, 1.0 );",


            "gl_Position = projectionMatrix * mvPosition;",
            "gl_PointSize = baseSize * size * ( 100.0 / length( gl_Position.xyz ) ) * (1.0 - vAge * 0.6);",


        "}",
    ].join("\n"),
    fragment: [
        "uniform sampler2D texture;",

        "varying float vAlpha;",
        "varying float vAge;",

        "vec3 desaturate(vec3 color, float amount)",
        "{",
            "vec3 gray = vec3(dot(vec3(0.2126,0.7152,0.0722), color));",
            "return vec3(mix(color, gray, amount));",
        "}",

        "void main() {",
            "vec4 color = texture2D( texture, gl_PointCoord );",
            "color.a = vAlpha;",
            "gl_FragColor = vec4(desaturate(color.xyz, vAge * 1.0), color.a);",
        "}",
    ].join("\n")
};

