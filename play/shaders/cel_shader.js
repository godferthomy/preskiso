/*
	Gives THREE.js Lambert Material a Toon / Cel look
*/
THREE.ShaderLib['cel'] = THREE.ShaderLib['lambert'];
THREE.ShaderLib['cel'].fragmentShader = THREE.ShaderLib['cel'].fragmentShader.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
THREE.ShaderLib['cel'].fragmentShader = "uniform vec3 diffuse;\n" + THREE.ShaderLib['cel'].fragmentShader.substr(0, THREE.ShaderLib['cel'].fragmentShader.length-1);
THREE.ShaderLib['cel'].fragmentShader += [
	"#ifdef USE_MAP",
	"	gl_FragColor = texture2D( map, vUv );",
	"#else",
	"	gl_FragColor = vec4(diffuse, 1.0);",
	"#endif",
	"	vec3 basecolor = vec3(gl_FragColor[0], gl_FragColor[1], gl_FragColor[2]);",
	"	float alpha = gl_FragColor[3];",
	"	float vlf = vLightFront[0];",
	// Clean and simple //
	"	if (vlf< 0.50) { gl_FragColor = vec4(mix( basecolor, vec3(0.0), 0.5), alpha); }",
	"	if (vlf>=0.50) { gl_FragColor = vec4(mix( basecolor, vec3(0.0), 0.3), alpha); }",
	// "	if (vlf>=0.75) { gl_FragColor = vec4(mix( basecolor, vec3(1.0), 0.0), alpha); }",
	"	if (vlf>=0.95) { gl_FragColor = vec4(mix( basecolor, vec3(1.0), 0.3), alpha); }",
	//"	gl_FragColor.xyz *= vLightFront;",
	"}"
	].join("\n");