var focusParticlesShader = {
    vertex: [
        "uniform float speed;",
        "uniform float baseSize;",
        "uniform float baseAlpha;",
        "uniform float time;",
        "attribute float size;",
        "attribute float alpha;",
        "attribute float birth;",

        "varying float vAlpha;",

        "void main() {",
            "vAlpha = baseAlpha * alpha * min((time - birth) * speed, 1.0);",

            "vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",

            "gl_PointSize = baseSize * size * ( 150.0 / length( mvPosition.xyz ) );",

            "gl_Position = projectionMatrix * mvPosition;",

        "}",
    ].join("\n"),
    fragment: [
        "uniform sampler2D texture;",

        "varying float vAlpha;",

        "void main() {",
            "vec4 color = texture2D( texture, gl_PointCoord );",
            "color.a *= vAlpha;",
            "gl_FragColor = color;",
        "}",
    ].join("\n")
};

