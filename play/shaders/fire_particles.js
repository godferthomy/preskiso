var fireParticlesShader = {
    vertex: [
        "uniform float speed;",
        "uniform float baseSize;",
        "uniform float baseAlpha;",
        "uniform float time;",
        "attribute float size;",
        "attribute float alpha;",
        "attribute float birth;",
        "attribute float fireIndex;",

        "varying float vAlpha;",
        "varying float vFireIndex;",

        "void main() {",
            "vAlpha = baseAlpha * alpha * max(1.0 - (time - birth) * 6.0 / speed, 0.0);",
            "vFireIndex = min((fireIndex + (time - birth) * 6.0 / speed), 2.0);",

            "vec3 newPosition = vec3(position.x + sin(position.z + time * position.z) / size / 10.0, position.y + sin(time) * 2.0, position.z + cos(position.x + time * 2.0) / size / 10.0);",
            "vec4 mvPosition = modelViewMatrix * vec4( newPosition, 1.0 );",

            "gl_PointSize = baseSize * size * ( 100.0 / length( mvPosition.xyz ) )* max(1.0 - (time - birth) * 1.5 / speed, 0.0);",
            // "gl_PointSize = size * 10.0* max(0.5 - (time - birth) * 3.0 / speed, 0.0);",

            "gl_Position = projectionMatrix * mvPosition;",

        "}",
    ].join("\n"),
    fragment: [
        "uniform sampler2D fire0;",
        "uniform sampler2D fire1;",
        "uniform sampler2D fire2;",

        "varying float vAlpha;",
        "varying float vFireIndex;",

        "vec3 desaturate(vec3 color, float amount)",
        "{",
            "vec3 gray = vec3(dot(vec3(0.2126,0.7152,0.0722), color));",
            "return vec3(mix(color, gray, amount));",
        "}",

        "void main() {",
            "vec4 color = texture2D( fire0, gl_PointCoord ) * max(1.0-vFireIndex, 0.0)",
            " + texture2D( fire1, gl_PointCoord ) * (1.0 - abs(vFireIndex-1.0))",
            " + texture2D( fire2, gl_PointCoord ) * max(vFireIndex - 1.0, 0.0);",
            "color.a = vAlpha;",
            "gl_FragColor = vec4(desaturate(color.xyz, (1.0 - vAlpha)/1.0), color.a);",
            // "gl_FragColor = vec4(color.xyz / (1.0 - vAlpha), color.a);",
            // "gl_FragColor = color;",
        "}",
    ].join("\n")
};

