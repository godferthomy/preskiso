PRESKISO.KeyboardController = function (player) {
	var THRESHOLD = 0.1, THRESHOLDINV = 1 - THRESHOLD;

	var GrenadeKey = "KeyZ";
	var ShootingButton1 = "ControlLeft";
	var ShootingButton2 = "ControlRight";

	var pressedKeys = {};

	var keyDown = function (e) {
		pressedKeys[e.code] = true;
		switch (e.code) {
			case "Space":
				player.jump();
				break;
			case GrenadeKey:
				player.prepareGrenade();
				break;
			case "KeyF":
				player.isInteracting = true;
				break;
			case "KeyR":
				player.startReloading();
				break;
			case "KeyX":
				player.startMeleeAttacking();
				break;
			case ShootingButton1:
			case ShootingButton2:
				player.startShooting();
				break;
			case "Escape":
				PRESKISO.GameController.pause(PRESKISO.Constants.KeyboardGamepadId);
				break;
			case "Tab":
				player.toggleMovementMode();
				break;
			case "Backquote":
				player.toggleRotationMode(true);
				break;
			case "Digit1":
				player.equipWeapon(PRESKISO.Constants.WeaponTypes.HANDGUN);
				break;
			case "Digit2":
				player.equipWeapon(PRESKISO.Constants.WeaponTypes.SHOTGUN);
				break;
			case "Digit3":
				player.equipWeapon(PRESKISO.Constants.WeaponTypes.RIFLE);
				break;
			case "Digit4":
				player.equipWeapon(PRESKISO.Constants.WeaponTypes.HEAVY);
				break;
		}
		updateMovementFromKeys();
		e.preventDefault();
            return;
	}

	var keyUp = function (e) {
		pressedKeys[e.code] = false;
		switch (e.code) {
			case GrenadeKey:
				player.throwGrenade();
				break;
			case ShootingButton1:
			case ShootingButton2:
				player.stopShooting();
				break;
		}
		updateMovementFromKeys();
		e.preventDefault();
            return;
	}

	var onMouseMove = function (e) {
		updateMovementFromKeys();
	}

	var updateMovementFromKeys = function () {
		var xMovement = 0, yMovement = 0;
		if (pressedKeys["KeyA"]) xMovement ++;
		if (pressedKeys["KeyD"]) xMovement --;
		if (pressedKeys["KeyW"]) yMovement ++;
		if (pressedKeys["KeyS"]) yMovement --;
		var movementFromKeys = new THREE.Vector3(xMovement, 0, yMovement);
		player.setMovementFromKeys(movementFromKeys);

		// var target = new THREE.Vector3(-newState.axes[2], 0, -newState.axes[3]);
		// if (player.movementRelativeToRotation && target.lengthSq() == 0) {
		// 	target = new THREE.Vector3(-newState.axes[0], 0, -newState.axes[1]);
		// }
		// player.setTarget(target);

		// var rotationFromKeys = new THREE.Vector3(newState.buttons[7].value - newState.buttons[6].value, -newState.axes[2], 0);
		// player.setRotationFromKeys(rotationFromKeys);
		var mousePosition = getMousePositionOnScreen();
		player.setTarget(mousePosition);
		var bcLength = mousePosition.distanceTo(player.object.position);
		var ac = new THREE.Vector3().subVectors(mousePosition, player.object.position).setY(0);
		var acLength = ac.length();
		var xAngle = -Math.acos(acLength/bcLength);
		if (mousePosition.y < player.object.position.y)
			xAngle *= -1;

		player.setRotationFromKeys(new THREE.Vector3(xAngle, 0, 0));
	}
	
	window.addEventListener("keydown", keyDown);
	window.addEventListener("keyup", keyUp);
    window.addEventListener("mousemove", onMouseMove, false );
}
