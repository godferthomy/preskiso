var PRESKISO = PRESKISO || {};

PRESKISO.GameController = (function () {
	function GameController() {
		var physijsToEntityIds = {};
		var objectEntities = {};
		var entities = [];
		var pillEntities = [];
		var gamepadControllers = [];
		var keyboardController;
		// var playerColors = [
		// 	// new THREE.Color(0.89, 0.59, 0.12),
		// 	// new THREE.Color(0.22, 0.97, 0.67),
		// 	// new THREE.Color(1, 0, 0),
		// 	// new THREE.Color(1, .5, 0),
		// 	// new THREE.Color(1, 1, 0),
		// 	// new THREE.Color(0, 1, 0),
		// 	// new THREE.Color(0, 1, .5),
		// 	new THREE.Color(0, 1, 1),
		// 	new THREE.Color(0, 0, 1),
		// 	new THREE.Color(.5, 0, 1),
		// 	new THREE.Color(1, 0, 1),
		// ];
		// shuffle(playerColors);
		var players = [];
		var playersById = {};
		var nextTeamId = 0;
		var teams = [];
		var cameraController;
		var hudController;
		var isRunning = true;
		var pausePlayer = undefined;

		var hittableObjects = [];
		var pillHittableObjects = [];
		var nonPillHittableObjects = [];
		var dimmableObjects = [];
		var dimmedEntities = [];
		var interactiveObjects = [];

		var toRemove = [], toAdd = [];

		var lastSpawn = 0;
		var zombieSpawnPoints = [];
		var playerSpawnPoints = [];
		// var powerupSpawnPoints = [];
		// var powerups = {};

		var currentTime = 0;

		var friendlyFire = PRESKISO.SettingsManager.getGameSetting("friendlyFire") == "true";
		window.console.log("FF", friendlyFire);
		var that = this;

		var gameConfig = JSON.parse(PRESKISO.SettingsManager.getItem("gameConfig"));

		this.setMap = function (stringMap) {
			this.mapTo3dRatio = 5;
			var gen = new PRESKISO.WallGenerator(this, this.mapTo3dRatio);
			this.map = gen.mapFromString(stringMap);
			gen.generate(this.map);

			// var gridHelper = new THREE.GridHelper( Math.max(this.map.length, this.map[0].length) * this.mapTo3dRatio, this.mapTo3dRatio);
			// gridHelper.position.set((this.map[0].length + .5) * this.mapTo3dRatio, 0.05, (this.map.length + .5) * this.mapTo3dRatio);
			// scene.add( gridHelper );
			var emptyPoints = [];
			for (var x = 0; x < this.map.length; x++) {
				for (var y = 0; y < this.map[0].length; y++) {
					switch (this.map[x][y]) {
						case PRESKISO.WallGenerator.Types.ZOMBIE:
							zombieSpawnPoints.push(new THREE.Vector3(x * this.mapTo3dRatio, 2.5, y * this.mapTo3dRatio));
							break;
						case PRESKISO.WallGenerator.Types.PLAYER:
							playerSpawnPoints.push(new THREE.Vector3(x * this.mapTo3dRatio, 0, y * this.mapTo3dRatio));
							break;
						case PRESKISO.WallGenerator.Types.AMMO:
							var powerup = new PRESKISO.AmmoPowerupEntity(new THREE.Vector3(x * this.mapTo3dRatio, 0, y * this.mapTo3dRatio), { permanent: true });
							this.addEntity(powerup);
							break;
						case PRESKISO.WallGenerator.Types.HEALTH:
							var powerup = new PRESKISO.HealthPowerupEntity(new THREE.Vector3(x * this.mapTo3dRatio, 0, y * this.mapTo3dRatio), { permanent: true });
							this.addEntity(powerup);
							break;
						case PRESKISO.WallGenerator.Types.BARREL:
							var nbBarrels = Math.random() * 4;
							var takenSpots = {};
							var centerPosition = new THREE.Vector3(x * this.mapTo3dRatio, 0, y * this.mapTo3dRatio);
							for (var i = 0; i < nbBarrels; i++) {
								var spot = 0;
								while (takenSpots[spot]) {
									spot = Math.floor(Math.random() * 4);
								}
								takenSpots[spot] = true;
								var position = new THREE.Vector3((Math.floor(spot / 2) - 0.5) * this.mapTo3dRatio * 0.4, 1.5, ((spot % 2) - 0.5) * this.mapTo3dRatio * 0.4);
								position.add(centerPosition);
								var offset = new THREE.Vector3(Math.random() - .5, 0, Math.random() - .5).multiplyScalar(this.mapTo3dRatio * .2);
								position.add(offset);
								var barrel = new PRESKISO.BarrelEntity(.8, 2.5, position);
								this.addEntity(barrel);
							}
							break;
						case PRESKISO.WallGenerator.Types.TREE:
							var tree = new PRESKISO.TreeEntity(new THREE.Vector3(x * this.mapTo3dRatio, 0, y * this.mapTo3dRatio));
							this.addEntity(tree);
							break;
					}
					var center = new THREE.Vector3(x * this.mapTo3dRatio, 0, y * this.mapTo3dRatio);
					var maxI = Math.random() * 6 + 4;
					if (this.map[x][y] < 800) {
						maxI *= 3;
					} else {
						maxI *= 2;
					}
					for (var i = 0; i < maxI; i++) {
						emptyPoints.push(new THREE.Vector3(Math.random() - .5, 0, Math.random() - .5).multiplyScalar(this.mapTo3dRatio).add(center));
					}
				}
			}
			var mesh = THREEx.createGrassTufts(emptyPoints, cameraFlat.rotation.y + Math.PI / 2);
			scene.add(mesh);
			mesh.receiveShadow = true;

			PRESKISO.AI.PathFinder.AStar.init(this.map, this.mapTo3dRatio);

			var x = this.map.length * this.mapTo3dRatio;
			var y = this.map[0].length * this.mapTo3dRatio;
			// light.target.position.set(x / 1.9/*2*/, 0, y / 2);
			// light.position.add(light.target.position);
			// light.shadowCameraLeft = -x / 2 * 1.3;
			// light.shadowCameraTop = -y / 2 * 0.8;
			// light.shadowCameraRight = x / 2 * 1.4;
			// light.shadowCameraBottom = y / 2 * 1.2;
		}


		this.init = function (container, stringMap) {
			this.container = container;
			// this.stringMap = stringMap;

			initScene();

			this.addEntity(new PRESKISO.GroundEntity());




			cameraFlat = new THREE.Object3D();

			if (PRESKISO.SettingsManager.getGameSetting("cameraType") == "orthographic") {
				camera = new THREE.OrthographicCamera(0, 1, 0, 1, 1, 1000);
				cameraController = new PRESKISO.OrthographicCameraController(camera, 0, []);
			} else {
				camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 1, 1000);
				cameraController = new PRESKISO.CameraController(camera, 0, []);
			}
			THREEx.WindowResize(renderer, camera, container);


			this.setMap(stringMap);


			// light.position.set( 20, 30, -30 );
			// // light.target.position = camera.position;
			// light.castShadow = true;
			// light.shadowCameraLeft = -100;
			// light.shadowCameraTop = -100;
			// light.shadowCameraRight = 100;
			// light.shadowCameraBottom = 100;
			// camera.add(light);

			PRESKISO.AudioManager.setListener(camera);

			var configTeams = gameConfig.teams;
			for (var i = 0; i < configTeams.length; i++) {
				var team = new PRESKISO.Team(i, "Team " + (i + 1), new THREE.Color().copy(configTeams[i].color));
				team.addSpawningArea(playerSpawnPoints.shift(), this.mapTo3dRatio);
				var players = configTeams[i].players;
				for (var j = 0; j < players.length; j++) {
					var player = this.addPlayer(players[j].id, team, players[j].model);
					if (player.playerId == PRESKISO.Constants.KeyboardGamepadId) {
						keyboardController = new PRESKISO.KeyboardController(player);
					}
				}
				teams.push(team);
			}

			gamepadSupport.init();
			gamepadSupport.stopPolling();

			if (gameConfig.gameMode == 0) {
				gameMode = new PRESKISO.ZombieInvasionGameMode(this);
			} else if (gameConfig.gameMode == 1) {
				gameMode = new PRESKISO.DeathMatchGameMode(this);
			} else if (gameConfig.gameMode == 2) {
				gameMode = new PRESKISO.CTFGameMode(this);
			} else if (gameConfig.gameMode == 3) {
				gameMode = new PRESKISO.CTPGameMode(this);
			}
			gameMode.init();

			hudController = new PRESKISO.HudController(this, container);
			hudController.init();

			// PRESKISO.AudioManager.playSound(undefined, "musics/zombie_ambience", 3, {loop: true});
		}

		this.updateMapValue = function (position, value) {
			PRESKISO.AI.PathFinder.AStar.updateTileValue(position, value);
		}

		this.getCameraController = function () {
			return cameraController;
		}

		this.getPlayers = function () {
			return players;
		}

		this.getTeams = function () {
			return teams;
		}

		this.getZombieSpawnPoints = function () {
			return zombieSpawnPoints;
		}

		this.getGameMode = function () {
			return gameMode;
		}

		this.addPlayer = function (playerId, team, model) {
			var player = new PRESKISO.PlayerEntity(playerId, team, { name: "Player " + (players.length + 1), color: team.color, model: PRESKISO.Constants.PlayerModels[model] });
			team.addPlayer(player);
			players.push(player);
			playersById[playerId] = player;
			this.spawnPlayer(playerId);
			cameraController.addTrackedObject(player.getObject());
			return player;
		}

		this.spawnPlayer = function (playerId) {
			var player = playersById[playerId];
			this.addEntity(player);
		}

		this.respawnPlayer = function (playerId) {
			var player = playersById[playerId];
			this.removeEntity(player);
			this.spawnPlayer(playerId);
		}

		function gamepadsChanged(gamepads) {
			for (var i = gamepadControllers.length; i < gamepads.length; i++) {
				// var player = that.addPlayer(i);
				var player;
				for (var j = 0; j < players.length; j++) {
					if (players[j].playerId == gamepads[i].index) {
						var gamepadController = new PRESKISO.GamepadController(players[j], i, gamepads[i]);
						gamepadControllers[i] = gamepadController;
						break;
					}
				}
			}
		}

		this.hitEntity = function (targetObject, sourceEntity, power, worldPosition, objectPosition, impactVector) {
			worldPosition = worldPosition || targetObject.position;
			objectPosition = objectPosition || new THREE.Vector3(0, 0, 0);
			if (objectEntities[targetObject.id] !== undefined) {
				targetEntity = objectEntities[targetObject.id];
				var sourcePlayer;
				if (sourceEntity instanceof PRESKISO.PlayerEntity) {
					sourcePlayer = sourceEntity;
				} else {
					sourcePlayer = sourceEntity.player;
				}
				var sourceTeam = sourceEntity.team;
				if (friendlyFire || (sourcePlayer !== undefined && sourcePlayer.id == targetEntity.id) || targetEntity.team === undefined) {
					// friendlyFire is on, target is source, or the target has no team, no further check needed
					targetEntity.getHit(power, objectPosition, sourceEntity);
				} else {
					// friendlyFire is off
					// if the target and source are from the same team
					if (sourcePlayer !== undefined) {
						sourceTeam = sourcePlayer.team;
					}
					if (sourceTeam === undefined || sourceTeam.id != targetEntity.team.id) {
						targetEntity.getHit(power, objectPosition, sourceEntity);
					}
				}
				if (impactVector) {
					impactVector.multiplyScalar(targetEntity.impactFactor);
					if (impactVector.lengthSq() > 1000000) {
						impactVector.setLength(1000);
					}
					if (!targetEntity.carrier) {
						targetEntity.applyImpulse(impactVector, worldPosition, objectPosition);
					}
					// var targetObject = targetEntity.getObject();
					// var objectPosition = worldPosition.clone();
					// targetObject.worldToLocal(objectPosition);
					// targetEntity.getObject().applyImpulse(impactVector, objectPosition);
				}

				var sound = targetObject.material.impactSound || targetEntity.getImpactSound();
				if (sound) {
					PRESKISO.AudioManager.playSound(worldPosition, sound, Math.sqrt(power) * 2, { randomizePlaybackRateBy: 0.15 });
				}
				// if (targetEntity instanceof PRESKISO.PlayerEntity && targetEntity.health <= 0) {
				// 	// var player = targetEntity;
				// 	// if (sourceEntity instanceof PRESKISO.PlayerEntity) {
				// 	// 	sourceEntity.player.kills++;
				// 	// }
				// 	// this.respawnPlayer(player.playerId);
				// }
			} else {
				// audioManager.playSound(worldPosition, "impacts/wood_impact", power / 2);
			}
		};

		this.playerDiedAndDisappeared = function (player) {
			gameMode.playerDiedAndDisappeared(player);
		}

		this.entityDied = function (entity) {
			if (entity.lastHitEntity) {
				var sourceEntity = entity.lastHitEntity;
				var player;
				if (sourceEntity.player !== undefined) {
					player = sourceEntity.player;
				} else if (sourceEntity instanceof PRESKISO.PlayerEntity) {
					player = sourceEntity;
				}
				if (player !== undefined) {
					if (player == entity) {
					} else if (player.team == entity.team) {
						player.kills--;
					} else {
						player.kills++;
					}
				}
			}
			gameMode.entityDied(entity, entity.lastHitEntity);
		}

		this.addEntity = function (entity) {
			if (entity === undefined) {
			} else {
				toAdd.push(entity);
			}
		}

		this._addEntity = function (entity) {
			var obj = entity.getObject();
			scene.add(obj);
			if (entity.isDimmable) {
				dimmableObjects.push(obj);
			}
			if (entity.isHittable) {
				var entityHittableObjects = entity.getHittableObjects();
				for (var i = 0; i < entityHittableObjects.length; i++) {
					hittableObjects.push(entityHittableObjects[i]);
					objectEntities[entityHittableObjects[i].id] = entity;
					if (entity.isPill) {
						pillHittableObjects.push(entityHittableObjects[i]);
					} else {
						nonPillHittableObjects.push(entityHittableObjects[i]);
					}
				}
			}
			if (entity.isInteractive) {
				interactiveObjects.push(obj);
			}
			// objectEntities[obj.id] = entity;
			entities.push(entity);
			if (entity.isPill) {
				pillEntities.push(entity);
			}
			entity.isInScene = true;
			// if (entity.hasBody) {
			// 	world.add(entity.getBody());
			// }
			entity.objectAdded(scene);
		}

		this.removeEntity = function (entity) {
			toRemove.push(entity);
		}

		this._removeEntity = function (entity) {
			var obj = entity.getObject();
			if (obj.parent !== undefined) {
				obj.parent.remove(obj);
			}
			PRESKISO.Util.arrayRemoveObjectWithAttribute(entities, "id", entity.id);


			if (entity.isDimmable) {
				window.console.log("Removing dimmable entity O_O", entity);
				PRESKISO.Util.arrayRemoveObjectWithAttribute(dimmableObjects, "id", obj.id);
				PRESKISO.Util.arrayRemoveObjectWithAttribute(dimmedEntities, "id", entity.id);
			}
			if (entity.isHittable) {
				var entityHittableObjects = entity.getHittableObjects();
				for (var i = 0; i < entityHittableObjects.length; i++) {
					PRESKISO.Util.arrayRemoveObjectWithAttribute(hittableObjects, "id", entityHittableObjects[i].id);
					if (entity.isPill) {
						PRESKISO.Util.arrayRemoveObjectWithAttribute(pillHittableObjects, "id", entityHittableObjects[i].id);
					} else {
						PRESKISO.Util.arrayRemoveObjectWithAttribute(nonPillHittableObjects, "id", entityHittableObjects[i].id);
					}
					if (entity.isInteractive) {
						PRESKISO.Util.arrayRemoveObjectWithAttribute(interactiveObjects, "id", entityHittableObjects[i].id);
					}
					// hittableObjects.splice(hittableObjects.indexOf(entityHittableObjects[i]), 1);
					delete objectEntities[entityHittableObjects[i].id];
				}
			}
			if (entity.isPill) {
				PRESKISO.Util.arrayRemoveObjectWithAttribute(pillEntities, "id", entity.id);
			}
			// if (entity.hasBody) {
			// 	world.remove(entity.getBody());
			// }

			entity.isInScene = false;
			entity.objectRemoved(scene);
		}

		this.getEntityFromObject = function (object) {
			return objectEntities[object.id];
		}

		this.getCurrentTime = function () {
			return currentTime;
		}

		this.onAnimationFrame = function () {
			this.tick(clock.getDelta(), clock.getElapsedTime());
			if (isRunning) {
				var that = this;
				requestAnimationFrame(function () { that.onAnimationFrame(); });
			}
		}

		this.tick = function (delta, time) {
			gamepadSupport.tick();

			currentTime = time;

			PRESKISO.AI.PathFinder.AStar.updateWithEntitiesWeights(this.getPillEntities());

			for (var i = entities.length - 1; i >= 0; i--) {
				entities[i].tick(delta, time);
			}

			PRESKISO.AudioManager.tick(delta, time);
			cameraController.tick(delta, time);

			for (var i = 0; i < toRemove.length; i++) {
				this._removeEntity(toRemove[i]);
			}
			toRemove = [];
			for (var i = 0; i < toAdd.length; i++) {
				this._addEntity(toAdd[i]);
			}
			toAdd = [];
			gameMode.tick(delta, time);
			for (var i = 0, maxI = teams.length; i < maxI; i++) {
				teams[i].tick(delta, time);
			}
			hudController.tick(delta, time);

			// for (var i = entities.length - 1; i >= 0; i--) {
			// 	entities[i].prepareForPhysics();
			// }

			//  		world.step(delta);

			this.manageDimmableObjects(delta);

			THREE.AnimationHandler.update(delta);

			renderer.render(scene, camera);
			render_stats.update();

			// var didSimulate = scene.simulate( delta + this.simulationMiss, 2 );
			// if (!didSimulate) {
			// 	this.simulationMiss += delta;
			// } else {
			// 	this.simulationMiss = 0;
			// }
			scene.simulate(undefined, 2);
		};

		this.manageDimmableObjects = (function () {
			var raycaster = new THREE.Raycaster(new THREE.Vector3(), new THREE.Vector3());
			var direction = new THREE.Vector3();// raycaster.ray.direction;
			return function (delta) {
				var previousDimmedEntities = dimmedEntities;
				dimmedEntities = [];

				var targets = this.getPlayers();
				// var targets = pillEntities;
				for (var i = 0; i < targets.length; i++) {
					if (!targets[i].isAlive()) {
						continue;
					}
					direction.copy(targets[i].object.position);
					direction.sub(camera.position);
					raycaster.far = direction.length() - 1;
					direction.normalize();
					raycaster.set(camera.position, direction);
					var intersects = raycaster.intersectObjects(dimmableObjects);//, true );
					for (var j = 0; j < intersects.length; j++) {
						var entity = objectEntities[intersects[j].object.id];
						if (entity === undefined) {
							window.console.log("Dimming a not dimmable object O_O", intersects[j], entity);
						}
						// toDim.push(obj);
						// if (Util.arrayObjectIndexOf(dimmedEntities, "id", entity.id) == -1) {
						dimmedEntities.push(entity);
						entity.setDimmed(true);
						// }
					}
				}

				for (var i = 0; i < previousDimmedEntities.length; i++) {
					var entity = previousDimmedEntities[i];
					if (PRESKISO.Util.arrayObjectIndexOf(dimmedEntities, "id", entity.id) == -1) {
						entity.setDimmed(false);
					}
				}

				for (var i = 0; i < dimmableObjects.length; i++) {
					var entity = objectEntities[dimmableObjects[i].id];
					entity.manageDimming(delta);
				}

			};
		})();

		this.getHittableObjects = function () {
			return hittableObjects;
		}

		this.getInteractiveObjects = function () {
			return interactiveObjects;
		}

		this.getPillHittableObjects = function () {
			return pillHittableObjects;
		}

		this.getNonPillHittableObjects = function () {
			return nonPillHittableObjects;
		}

		this.getEntities = function () {
			return entities;
		}

		this.getPillEntities = function () {
			return pillEntities;
		}

		this.start = function () {
			var that = this;
			requestAnimationFrame(function () { that.onAnimationFrame(); });
		}

		this.pause = function (gamepadId) {
			if (!isRunning) {
				return;
			}
			this.pauseGamepad = gamepad;
			var hudEl = this.container.children[0];
			isRunning = false;

			PRESKISO.AudioManager.pause();
			clock.stop();
			// gamepadSupport.startPolling();
			pausePlayer = player;
			window.console.log(gamepad);
			PRESKISO.MenuNavigationController.restrictToGamepadId(gamepadId);
			setTimeout(function () {
				PRESKISO.MenuNavigationController.showMenu("pause_menu");
				PRESKISO.MenuNavigationController.listen();
			}, 50);
			$(hudEl).addClass("paused");
		}

		this.resume = function () {
			if (isRunning) {
				return;
			}
			var hudEl = this.container.children[0];
			isRunning = true;
			// for (var i = 0; i < gamepadControllers; i++) {
			// 	gamepadControllers[i].reset();
			// }

			PRESKISO.AudioManager.resume();
			clock.start();
			// gamepadSupport.stopPolling();
			var that = this;
			requestAnimationFrame(function () { that.onAnimationFrame(); });

			$(hudEl).removeClass("paused");
			PRESKISO.MenuNavigationController.stopListening();
		}
		gamepadSupport.setGamepadsObserver(gamepadsChanged);
	}
	return new GameController();
})();
