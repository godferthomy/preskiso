PRESKISO.CameraController = function (camera, offset, trackedObjects) {
	var trackedObjects = trackedObjects || [];
	// this.offset = offset || new THREE.Vector3(0, 15, -15);
	this.offset = offset || new THREE.Vector3(4, 10, -5);
	this.target = undefined;
	this.dist = undefined;

	camera.position.copy(this.offset);
	// camera.position.y += 20;
	camera.lookAt(new THREE.Vector3(0, 0, 0));
	cameraFlat.position.copy(this.offset).multiplyScalar(-1);
	cameraFlat.position.y = 0.05;
	cameraFlat.lookAt(new THREE.Vector3(0, 0, 0));
	cameraFlat.updateMatrixWorld();

	this.addTrackedObject = function (object) {
		trackedObjects.push(object);
	};

	this.removeTrackedObject = function (object) {
		PRESKISO.Util.arrayRemoveObjectWithAttribute(trackedObjects, "id", object.id);
	}

	this.tick = function (delta, time) {
		if (trackedObjects.length == 0) {
			return;
		}
		var first = trackedObjects[0];
		var minX = first.position.x, maxX = minX,
			minZ = first.position.z, maxZ = minZ;
		var position = new THREE.Vector3();
		for (var i = 1; i < trackedObjects.length; i++) {
			position.setFromMatrixPosition( trackedObjects[i].matrixWorld )
			minX = Math.min(minX, position.x - 3);
			maxX = Math.max(maxX, position.x + 3);
			minZ = Math.min(minZ, position.z - 7);
			maxZ = Math.max(maxZ, position.z + 3);
		}
		var target = new THREE.Vector3((minX+maxX) / 2, 0, (minZ+maxZ) / 2);
		var dist = Math.max((maxX - minX) / camera.aspect, (maxZ - minZ)) / 1.2;
		// var dist = Math.sqrt(Math.pow(maxX - minX, 2), Math.pow(maxZ - minZ, 2));
		dist = Math.max(dist, 15);
		if (this.target !== undefined) {
			var lerpFactor = delta * 2;
			this.target.lerp(target, lerpFactor);
			this.dist = this.dist * (1-lerpFactor) + dist * lerpFactor;
		} else {
			this.target = target;
			this.dist = dist;
		}
		light.position.set( 20, 30, -30 ).add(this.target);
		light.target.position.set( 0, 0, 0 ).add(this.target);
		var position = new THREE.Vector3().addVectors(this.target, this.offset);
		// var dist = Math.sqrt((maxX - minX) * (maxX - minX) + (maxZ - minZ) * (maxZ - minZ));
		// var alpha = (180 - camera.fov) / 2;
		// alpha = alpha / 180 * Math.PI;
		// var a = dist * Math.sin(alpha) / Math.sin(alpha * 2);
		// position.y += a;

		var cameraDistance = this.dist / 2 * Math.tan((180 - camera.fov) / 2 / 180 * Math.PI);
		// position.y += cameraDistance;
		// position.y = Math.max(20, position.y);

		camera.position.copy(position);
		camera.translateOnAxis(new THREE.Vector3(0, -.1, 1), cameraDistance);
		// camera.lookAt(this.target);
	};
}
