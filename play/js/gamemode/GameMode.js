PRESKISO.GameMode = ( function () {
	GameMode = function (options) {

		this.nextPlayerSpawnTimes = [];

		this.reviveDelay = 0;
	}

	GameMode.prototype.init = function () {
	}

	GameMode.prototype.tick = function (delta, time) {
		for (var i = this.nextPlayerSpawnTimes.length - 1; i >= 0; i--) {
			if (time > this.nextPlayerSpawnTimes[i].time) {
				PRESKISO.GameController.respawnPlayer(this.nextPlayerSpawnTimes[i].playerId);
				this.nextPlayerSpawnTimes.splice(i, 1);
			}
		}
	}

	GameMode.prototype.entityDied = function (entity, lastHitEntity) {
	}

	GameMode.prototype.playerDiedAndDisappeared = function (player) {
		this.nextPlayerSpawnTimes.push({
			playerId: player.playerId,
			time: PRESKISO.GameController.getCurrentTime() + this.reviveDelay
		});
		PRESKISO.GameController.removeEntity(player);
	}


	return GameMode;
})();
