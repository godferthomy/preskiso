PRESKISO.ZombieInvasionGameMode = ( function () {
	function ZombieInvasionGameMode(options) {
		PRESKISO.GameMode.call(this);

		this.zombieSpawnPoints = PRESKISO.GameController.getZombieSpawnPoints();
		this.nbZombies = 0;
		this.nextZombieSpawnTime = undefined;
		this.maxZombies = PRESKISO.SettingsManager.getGameSetting("ZI_maxZombies");
		this.difficulty = parseInt(PRESKISO.SettingsManager.getGameSetting("ZI_difficulty"));
		this.zombieSpeed = 60 + (this.difficulty - 1) * 10;
		this.zombieHealth = 60 + (this.difficulty - 1) * 20;
		this.acquisitionDistance = Math.pow(this.difficulty + 1, 4) * PRESKISO.GameController.mapTo3dRatio;
		options = options || {};

		this.reviveDelay = 1;

		this.heartbeatSound = PRESKISO.AudioManager.playSound(undefined, "heartbeat", 10, {loop: true});

		
	}
	PRESKISO.Util.inherit(ZombieInvasionGameMode, PRESKISO.GameMode);

	ZombieInvasionGameMode.prototype.tick = function (delta, time) {
		this.parent.tick.call(this, delta, time);
		if (this.nextZombieSpawnTime === undefined) {
			if (this.nbZombies < this.maxZombies) {
				this.nextZombieSpawnTime = time + 0.5;
			}
		} else if (time > this.nextZombieSpawnTime) {
			this.nbZombies++;
			this.nextZombieSpawnTime = undefined;
			var zombie = new PRESKISO.ZombieEntity(this.zombieHealth, this.zombieSpeed, this.acquisitionDistance);
			var position = this.zombieSpawnPoints[Math.floor(Math.random() * this.zombieSpawnPoints.length)];
	        zombie.getObject().position.copy(position);
	        PRESKISO.GameController.addEntity(zombie);
		}

		var players = PRESKISO.GameController.getPlayers();
		var totalHealth = 0;
		for (var i = 0; i < players.length; i++) {
			totalHealth += players[i].health;
		}
		var averageHealth = totalHealth / players.length;
		if (averageHealth > 50) {
			// filter.frequency.value = 100000;
			this.heartbeatSound.volume.gain.value = 0;
			this.heartbeatSound.source.playbackRate.value = 1;
		} else {
			// filter.frequency.value = 80 + averageHealth * 10;
			this.heartbeatSound.volume.gain.value = 30 + (50 - averageHealth);
			this.heartbeatSound.source.playbackRate.value = 1 - (50 - averageHealth) / 200;
		}
		// PRESKISO.AudioManager.cancelSound(this.reloadSound);

	}

	ZombieInvasionGameMode.prototype.entityDied = function (entity, lastHitEntity) {
		if (entity instanceof PRESKISO.ZombieEntity) {
			this.nbZombies--;
			if (lastHitEntity !== undefined) {
				var killer = lastHitEntity;
				if (lastHitEntity.player) {
					killer = lastHitEntity.player;
				}
				if (killer instanceof PRESKISO.PlayerEntity) {
					killer.score++;
					// lastHitEntity.team.score++;
				}
			}
		}
	}

	return ZombieInvasionGameMode;
})();
