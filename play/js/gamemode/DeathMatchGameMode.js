PRESKISO.DeathMatchGameMode = ( function () {
	function DeathMatchGameMode(options) {
		PRESKISO.GameMode.call(this);

		options = options || {};

		this.reviveDelay = 3;
	}
	PRESKISO.Util.inherit(DeathMatchGameMode, PRESKISO.GameMode);

	DeathMatchGameMode.prototype.tick = function (delta, time) {
		this.parent.tick.call(this, delta, time);
		var players = PRESKISO.GameController.getPlayers();
		for (var i = 0; i < players.length; i++) {
			players[i].score = players[i].kills;
		}
	}

	DeathMatchGameMode.prototype.entityDied = function (entity, lastHitEntity) {
	}

	return DeathMatchGameMode;
})();
