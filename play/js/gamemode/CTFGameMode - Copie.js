PRESKISO.CTFGameMode = ( function ( PRESKISO ) {
	function CTFGameMode(options) {
		PRESKISO.GameMode.call(this);

		options = options || {};

		this.nextPlayerSpawnTimes = [];

		this.teamsObjects = [];		
	}
	PRESKISO.Util.inherit(CTFGameMode, PRESKISO.GameMode);

	CTFGameMode.prototype.init = function () {
		return;
		var teams = PRESKISO.GameController.getTeams();
		for (var i = 0; i < teams.length; i++) {
			var flag = new PRESKISO.Flag(PRESKISO.GameController, this, teams[i], teams[i].spawnAreas[0]);
			var flagHolder = new PRESKISO.FlagHolder(PRESKISO.GameController, this, teams[i], teams[i].spawnAreas[0]);
			var teamObjects = {
				flag: flag,
				flagHolder: flagHolder
			};
			PRESKISO.GameController.addEntity(flag);
			PRESKISO.GameController.addEntity(flagHolder);
			this.teamsObjects.push(teamObjects);
		}

		// this.flagHolder = new PRESKISO.FlagHolder(this, this.spawnAreas[0].center.clone());
		// scene.add(this.flagHolder.getObject());

		// this.flag = new PRESKISO.Flag(this);
		// scene.add(this.flag.getObject());

		// this.resetFlag();
	}

	CTFGameMode.prototype.tick = function (delta, time) {
		this.parent.tick.call(this, delta, time);
		return;

		// this.flag.tick(delta);
		// this.flagHolder.tick(delta);
		var teams = PRESKISO.GameController.getTeams();
		for (var i = 0; i < teams.length; i++) {
			var team = teams[i];
			var teamObjects = this.teamsObjects[i];
			this.tickForTeam(team, teamObjects, delta, time);
		}
	}

	CTFGameMode.prototype.tickForTeam = function (team, teamObjects, delta, time) {
		if (teamObjects.flag.isInBase) {
			var players = team.getPlayers();
			for (var i = 0; i < players.length; i++) {
				var player = players[i];
				if (player.isCarryingFlag && teamObjects.flagHolder.isSteppedOnBy(player)) {
					player.dropFlag();
					// flag.team.resetFlag();
					team.score++;
					PRESKISO.AudioManager.playSound(null, "success", 3);
				}
			}
		}

		if (!teamObjects.flag.isCarried) {
			var players = PRESKISO.GameController.getPlayers();
			for (var i = 0; i < players.length; i++) {
				var player = players[i];
				if (teamObjects.flag.isInRange(player)) {
					if (player.team === team) {
						this.resetFlag(i);
					} else {
						player.takeFlag(teamObjects.flag);
					}
				}
			}
		}
	}
	
	CTFGameMode.prototype.resetFlag = function (teamId) {
		var flag = this.teamsObjects[teamId].flag;
		var flagHolder = this.teamsObjects[teamId].flagHolder;
		flag.getObject().position.copy(this.flagHolder.getObject().position).setComponent(1, 1.7);
		flag.reset();
	}

	CTFGameMode.prototype.entityDied = function (entity, lastHitEntity) {
		if (entity instanceof PRESKISO.ZombieEntity) {
			this.nextZombieSpawnTime = PRESKISO.GameController.getCurrentTime() + 0.5;
			this.nbZombies--;
		}
	}

	return CTFGameMode;
})(PRESKISO);
