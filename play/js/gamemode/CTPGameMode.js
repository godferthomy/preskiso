PRESKISO.CTPGameMode = ( function () {
	function CTPGameMode(options) {
		PRESKISO.GameMode.call(this);

		options = options || {};

		this.nextPlayerSpawnTimes = [];
		this.nbPigs = 0;
		this.maxPigs = 5;
		this.zombieSpawnPoints = PRESKISO.GameController.getZombieSpawnPoints();
		this.difficulty = PRESKISO.SettingsManager.getGameSetting("CTP_difficulty");
		this.pigSpeed = 50 + (this.difficulty - 1) * 20;

		this.teamsObjects = [];		
	}
	PRESKISO.Util.inherit(CTPGameMode, PRESKISO.GameMode);

	CTPGameMode.prototype.init = function () {
		for (var i = 0; i < 10; i++) {
		}
		var teams = PRESKISO.GameController.getTeams();
		for (var i = 0; i < teams.length; i++) {
			// var flag = new PRESKISO.FlagEntity(this, teams[i], teams[i].spawnAreas[0].center);
			var flagHolder = new PRESKISO.FlagHolderEntity(this, teams[i], teams[i].spawnAreas[0].center);
			var teamObjects = {
				// flag: flag,
				flagHolder: flagHolder
			};
			// PRESKISO.GameController.addEntity(flag);
			PRESKISO.GameController.addEntity(flagHolder);
			this.teamsObjects.push(teamObjects);
		}
	}

	CTPGameMode.prototype.tick = function (delta, time) {
		this.parent.tick.call(this, delta, time);
		if (this.nextPigSpawnTime === undefined) {
			if (this.nbPigs < this.maxPigs) {
				this.nextPigSpawnTime = time + 0.2;
			}
		} else if (time > this.nextPigSpawnTime) {
			this.nbPigs++;
			this.nextPigSpawnTime = undefined;
			var pig = new PRESKISO.PigEntity(this.pigSpeed);
			var position = this.zombieSpawnPoints[Math.floor(Math.random() * this.zombieSpawnPoints.length)];
	        pig.getObject().position.copy(position);
			PRESKISO.GameController.addEntity(pig);
		}

		// var teams = PRESKISO.GameController.getTeams();
		// for (var i = 0; i < teams.length; i++) {
		// 	var team = teams[i];
		// 	var teamObjects = this.teamsObjects[i];
		// 	this.tickForTeam(team, teamObjects, delta, time);
		// }
	}

	CTPGameMode.prototype.tickForTeam = function (team, teamObjects, delta, time) {
		if (teamObjects.flag.isInBase) {
			var players = team.getPlayers();
			for (var i = 0; i < players.length; i++) {
				var player = players[i];
				if (player.isCarryingEntity() && teamObjects.flagHolder.isSteppedOnBy(player)) {
					var carriedEntity = player.getCarriedEntity();
					if (carriedEntity instanceof PRESKISO.FlagEntity) {
						var flag = carriedEntity;
						player.dropCarriedEntity(flag);
						flag.reset();
						player.score++;
						// team.score++;
						PRESKISO.AudioManager.playSound(null, "success", 3);
					}
				}
			}
		}
		// teamObjects.flag.tick(delta, time);
	}
	
	CTPGameMode.prototype.resetFlag = function (teamId) {
		var flag = this.teamsObjects[teamId].flag;
		flag.reset();
	}

	CTPGameMode.prototype.entityDied = function (entity, lastHitEntity) {
		if (entity instanceof PRESKISO.PigEntity) {
			this.nbPigs--;
			// var killer = lastHitEntity;
			// if (lastHitEntity.player) {
			// 	killer = lastHitEntity.player;
			// }
			// if (killer instanceof PRESKISO.PlayerEntity) {
			// 	killer.score++;
			// 	// lastHitEntity.team.score++;
			// }
		}
	}

	return CTPGameMode;
})();
