PRESKISO.CTFGameMode = ( function () {
	function CTFGameMode(options) {
		PRESKISO.GameMode.call(this);

		options = options || {};

		this.nextPlayerSpawnTimes = [];

		this.teamsObjects = [];		
	}
	PRESKISO.Util.inherit(CTFGameMode, PRESKISO.GameMode);

	CTFGameMode.prototype.init = function () {
		var teams = PRESKISO.GameController.getTeams();
		for (var i = 0; i < teams.length; i++) {
			var flag = new PRESKISO.FlagEntity(this, teams[i], teams[i].spawnAreas[0].center);
			var flagHolder = new PRESKISO.FlagHolderEntity(this, teams[i], teams[i].spawnAreas[0].center);
			var teamObjects = {
				flag: flag,
				flagHolder: flagHolder
			};
			PRESKISO.GameController.addEntity(flag);
			PRESKISO.GameController.addEntity(flagHolder);
			this.teamsObjects.push(teamObjects);
		}
	}

	CTFGameMode.prototype.tick = function (delta, time) {
		this.parent.tick.call(this, delta, time);

		var teams = PRESKISO.GameController.getTeams();
		for (var i = 0; i < teams.length; i++) {
			var team = teams[i];
			var teamObjects = this.teamsObjects[i];
			this.tickForTeam(team, teamObjects, delta, time);
		}
	}

	CTFGameMode.prototype.tickForTeam = function (team, teamObjects, delta, time) {
		if (teamObjects.flag.isInBase) {
			var players = team.getPlayers();
			for (var i = 0; i < players.length; i++) {
				var player = players[i];
				if (player.isCarryingEntity() && teamObjects.flagHolder.isSteppedOnBy(player)) {
					var carriedEntity = player.getCarriedEntity();
					if (carriedEntity instanceof PRESKISO.FlagEntity) {
						var flag = carriedEntity;
						player.dropCarriedEntity(flag);
						flag.reset();
						player.score++;
						// team.score++;
						PRESKISO.AudioManager.playSound(null, "success", 3);
					}
				}
			}
		}
		// teamObjects.flag.tick(delta, time);
	}
	
	CTFGameMode.prototype.resetFlag = function (teamId) {
		var flag = this.teamsObjects[teamId].flag;
		flag.reset();
	}

	return CTFGameMode;
})();
