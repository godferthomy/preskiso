var PRESKISO = PRESKISO || {};
PRESKISO.LoadingManager = (function () {
	function LoadingManager() {

		var audioResources = [
			"../../common/sounds/open_menu", "../../common/sounds/close_menu", "../../common/sounds/navigate_menu",
	        "impacts/wood_impact", "impacts/concrete_impact", "impacts/body_impact", "impacts/metal_impact",
	        "empty_weapon",
	        "grenade_throw", "grenade_explosion", "grenade_bounce", "rocket_explosion",
	        "headshot", "wilhelm", "success", "flag_taken", "metal_fall",
	        "ammo_pickup", "weapon_pickup", "health_pickup",
	        "melee_attack", "melee_hit",

	        "pig_normal", "pig_stressed", "pig_panicked", "pig_death",

	        "zombie_death", "zombie_attack", "heartbeat",

	        "door_start_opening", "door_finish_closing",

			// "musics/zombie_ambience",
	        // "musics/Spring_Mvt_1_Allegro", "musics/doom_theme",
	    ];

	    for (var i = 0; i < PRESKISO.Constants.Weapons.length; i++) {
	        var weaponData = PRESKISO.Constants.Weapons[i];
	        audioResources.push(weaponData.sound);
	        if (weaponData.reloadSound !== undefined) {
	            audioResources.push(weaponData.reloadSound);
	        }
	    }

	    var audioResourcesCount = 0;
	    var audioMultipleSources = {
	        "impacts/wood_impact": 4,
	        "impacts/body_impact": 3,
	        "impacts/concrete_impact": 6,
	        "pan_kitsu": 6,
	        "pan_gros_loup": 6,
	        "grenade_explosion": 7,
	        "grenade_bounce": 2,
	        "zombie_death": 3,
	        "zombie_attack": 7,
	        "rocketlauncher": 2,
	        "uzi": 3,
	        "melee_hit": 2,
	        "pig_normal": 3,
	        "pig_stressed": 3,
	        "pig_panicked": 3,
	    };
	    for (var i = 0; i < audioResources.length; i++) {
	    	audioResourcesCount += audioMultipleSources[audioResources[i]] || 1;
	    }

	    var modelCount = 2;
	    PRESKISO.Loader.queueObjects(["pill-classic", "pill-cool", "pill-girl", "pill-papy", "pill-arm", "pill-arm2", "pill-zombie", "pig", "weapons/grenade", "flagholder", "tree"]);
	    var weapons = PRESKISO.Constants.Weapons;
	    for (var i = 0; i < weapons.length; i++) {
	        PRESKISO.Loader.queueObject(weapons[i].model);
	        modelCount++;
	        if (weapons[i].ammoModel !== undefined) {
	            PRESKISO.Loader.queueObject(weapons[i].ammoModel);
	        	modelCount++;
	        }
	    }
	    modelCount *= 1;

	    var that = this;

	    var totalLoaded, totalToLoad = audioResourcesCount + 1 /* map */ + modelCount + 10 /* gameController */;

	    this.load = function (callback) {
	    	this.loadingBar = $("#loading_bar_value");
	    	this.loadingMessage = $("#loading_message");
	    	this.loadingPill = $("#loading_pill");
	    	this.callback = callback;
	    	totalLoaded = 0;
	    	this.loadModels();
	    }

	    function audioFileLoaded() {
	    	totalLoaded++;
	    	that.updateProgress(totalLoaded);
	    }

	    this.loadAudio = function () {
    		this.loadingMessage.text("Loading sounds");
	    	(function (_this) {
		    	PRESKISO.AudioManager.init(audioResources, audioMultipleSources, function () {
		    		_this.updateProgress(audioResourcesCount + modelCount + 1 /* map */);
		    		_this.initGameController();
		    	}, audioFileLoaded);
		    })(this);
	    }

	    this.loadMap = function () {
	    	var gameConfig = JSON.parse(PRESKISO.SettingsManager.getItem("gameConfig"));
	    	var mapFileName = PRESKISO.Constants.Maps[gameConfig.map].path;
	    	this.loadingMessage.text("Loading map");
	    	(function (_this) {
	    		jQuery.get('maps/' + mapFileName + '.map', null, function(data) {
	    			_this.updateProgress(modelCount + 1 /* map */);
	    			_this.map = data;
	    			_this.loadAudio();
	    		}, "text");
		    })(this);
	    }

	    this.loadModels = function () {
	    	this.loadingMessage.text("Loading models");
		    (function (_this) {
			    PRESKISO.Loader.load(function () {
			    	_this.updateProgress(modelCount);
			        _this.loadMap();
			    }/*, modelLoadCallback*/);
		    })(this);
	    }

	    this.initGameController = function () {
	    	this.loadingMessage.text("Loading game");
	    	var gameController = PRESKISO.GameController.init(container, this.map);
	    	this.updateProgress(audioResourcesCount + 1 /* map */ + modelCount + 10 /* gameController */);
	    	this.map = undefined;
	    	// if (typeof this.callback === "function") {
	    	// 	this.callback();
	    	// 	this.callback = undefined;
	    	// }
	    	$("#loading").remove();
	    }

	    this.updateProgress = function (progress) {
	    	totalLoaded = progress;
	    	var w = (totalLoaded / totalToLoad * 100) + "%";
	    	this.loadingPill.css({left: w});
	    	this.loadingBar.css({width: w});
	    }
	}
	return new LoadingManager();
})();
