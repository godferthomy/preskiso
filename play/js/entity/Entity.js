PRESKISO.Entity = (function () {
	function Entity(object) {
		this.id = Entity.getNextEntityId();

		this.object = object;
		this.hasBody = false;

		this.isHittable = false;
		this.isDimmable = false;
		this.piercingFactor = 0;

		this.hittableObjects = undefined;

		this.impactFactor = 1;

		this.lastHitEntity = undefined;

		this.globalMaterial = undefined;
		this.materialPool = undefined;

		this.opacity = 1;
		this.minimumOpacity = 0.6;

		this.animations = [];
		this.animationsByName = {};
	}

	Entity.prototype.registerAnimations = function (skinnedMesh, animations) {
		for (var i = 0; i < animations.length; i++) {
			animations[i].initialized = false;
			var animation = new THREE.Animation( skinnedMesh, animations[i]);
			this.animations.push(animation);
			this.animationsByName[animation.data.name] = animation;
		}
	}

	Entity.prototype.getAnimationByName = function (name) {
		return this.animationsByName[name];
	}

	Entity.prototype.playAnimation = function (name) {
		var animation = this.getAnimationByName(name);
	}

	Entity.prototype.getOpacity = function () {
		return this.opacity;
	}

	Entity.prototype.setOpacity = function (opacity) {
		if (opacity < 1 && this.opacity == 1) {
			if (this.object.material == this.globalMaterial) {
				this.object.material = this.getIndividualMaterial();
			}
			PRESKISO.Util.setMaterialField(this.object.material, "transparent", true);
		} else if (opacity >= 1 && this.opacity < 1) {
			if (this.globalMaterial != undefined) {
				this.releaseIndividualMaterial();
				this.object.material = this.globalMaterial;
			}
			PRESKISO.Util.setMaterialField(this.object.material, "transparent", false);
		}
		PRESKISO.Util.setMaterialField(this.object.material, "opacity", opacity);
		this.opacity = opacity;
	}



	Entity.prototype.getIndividualMaterial = function () {
		if (this.material_pool != undefined && this.material_pool.length > 0) {
			var material = this.material_pool.shift();
			PRESKISO.Util.setMaterialField(material, "opacity", 1);
			PRESKISO.Util.setMaterialField(material, "transparent", false);
			return material;
		}
		return PRESKISO.Util.cloneMaterial(this.globalMaterial);
	}

	Entity.prototype.releaseIndividualMaterial = function () {
		if (this.material_pool != undefined) {
			this.material_pool.push(this.object.material);
		}
	}

	Entity.prototype.setDimmed = function (value) {
		this.isDimmed = !!value;
	}

	Entity.prototype.manageDimming = function (delta) {
		var currentOpacity = this.opacity, targetOpacity;
		if (this.isDimmed && currentOpacity > this.minimumOpacity) {
			targetOpacity = Math.max(this.minimumOpacity, currentOpacity - delta * 6);
		} else if (!this.isDimmed && currentOpacity < 1) {
			targetOpacity = Math.min(1, currentOpacity + delta * 6);
		}
		if (targetOpacity) {
			this.setOpacity(targetOpacity); 
		}
	}

	Entity.prototype.isAlive = function () {
		return this.health > 0;
	}

	Entity.prototype.isDead = function () {
		return this.health <= 0;
	}

	Entity.prototype.clear = function () {
		this.scene = undefined;
	}

	Entity.prototype.getObject = function () {
		if (this.object === undefined) {
			this.createObject();
		}
		return this.object;
	}

	Entity.prototype.getBody = function () {
		if (this.hasBody && this.body === undefined) {
			this.createBody();
		}
		return this.body;
	}

	Entity.prototype.getHittableObjects = function () {
		if (this.hittableObjects === undefined) {
			this.hittableObjects = PRESKISO.Util.getHittableObjects(this.getObject());
		}
		return this.hittableObjects;
	}

	Entity.prototype.createObject = function () {
		window.console.error("createObject() is not defined!");
	}

	Entity.prototype.createBody = function () {
		window.console.error("createBody() is not defined!");
	}

	Entity.prototype.objectAdded = function () {
		// if (this.hasBody) {
		// 	this.body.velocity.set(0, 0, 0);
		// 	this.body.angularVelocity.set(0, 0, 0);
		// }
	}

	Entity.prototype.objectRemoved = function () {
	}

	Entity.prototype.tick = function (delta, time) {
		// if (this.hasBody) {
		// 	this.body.position.copy(this.object.position);
  //           this.body.quaternion.copy(this.object.quaternion);
		// }
	}

	Entity.prototype.prepareForPhysics = function () {
		if (!this.hasBody) {
			return;
		}
		if (this.object.__dirtyPosition) {
			var p = this.object.position;
			this.body.position.set(p.x, p.y, p.z);
		}
		if (this.object.__dirtyRotation) {
			var r = this.object.quaternion;
			this.body.quaternion.set(r.x, r.y, r.z, r.w);
		}
	}

	Entity.prototype.applyImpulse = function (impulse, worldPosition, objectPosition) {
		this.object.applyImpulse(impulse, objectPosition);
		// if (!this.hasBody) {
		// 	return;
		// }
		// if (impulse instanceof THREE.Vector3) {
		// 	impulse = new CANNON.Vec3(impulse.x, impulse.y, impulse.z);
		// }
		// if (position == undefined) {
		// 	position = this.body.position;
		// } else if (position instanceof THREE.Vector3) {
		// 	position = new CANNON.Vec3(position.x, position.y, position.z);
		// }
		// this.body.applyImpulse(impulse, position);
	}

	Entity.prototype.getHit = function (power, objectPosition, sourceEntity) {
		if (this.health === undefined || this.health <= 0) {
			return;
		}
		this.lastHitEntity = sourceEntity;
	}

	Entity.prototype.getImpactSound = function (power) {
		return undefined; //"impacts/wood_impact";
	}

	Entity.prototype.die = function () {
		PRESKISO.GameController.entityDied(this);
	}

	Entity.nextEntityId = 0;
	Entity.getNextEntityId = function () {
		return Entity.nextEntityId++;
	}

	return Entity;
})();
