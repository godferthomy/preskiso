( function () {
	var texture = THREE.ImageUtils.loadTexture( "images/concrete.jpg" );
	var mat = new THREE.MeshPhongMaterial({ map: texture });
    // wall_material.impactSound = "impacts/concrete_impact";

	PRESKISO.WindowEntity = function (gameController, dimensions) {
		PRESKISO.Entity.call(this, gameController);

		this.isDimmable = true;
		this.isHittable = true;

		this.dimensions = dimensions;
	}

	PRESKISO.Util.inherit(PRESKISO.WindowEntity, PRESKISO.Entity);

	// PRESKISO.Entity.prototype.clear = function () {
	// 	this.scene = undefined;
	// }

	PRESKISO.WindowEntity.prototype.tick = function (delta) {
	}

	PRESKISO.WindowEntity.prototype.createObject = function () {
		// var texture = THREE.ImageUtils.loadTexture( "images/concrete.jpg" );
		var wall_material = Physijs.createMaterial(
	            mat,
	            0.5, // high friction
	            .9 // low restitution
	        );
		// wall_material.map.wrapS = wall_material.map.wrapT = THREE.RepeatWrapping;
		// wall_material.map.repeat.set( Math.ceil(Math.max(this.dimensions.x, this.dimensions.z) / 4), 1 );
  //       wall_material.impactSound = "impacts/concrete_impact";
        wall_material.impactSound = "impacts/concrete_impact";
		var geometry = new THREE.BoxGeometry(this.dimensions.x, this.dimensions.y, this.dimensions.z);

    	var wall = new Physijs.BoxMesh(
	        geometry,
	        wall_material,
	        0
	    );
	    wall.isHittable = true;
	    wall.receiveShadow = wall.castShadow = true;

	    this.object = wall;
	}

	PRESKISO.WindowEntity.prototype.objectAdded = function (scene) {
	}
})();
