PRESKISO.FlagEntity = (function () {
	function FlagEntity(gameMode, team, basePosition) {
		PRESKISO.CarriableEntity.call(this);

		this.gameMode = gameMode;
		this.team = team;

		this.basePosition = basePosition;

		this.isInBase = true;
		this.autoPickup = true;
		this.radius = 2;
		this.height = 4;
		this.name = "Flag";
	}
	PRESKISO.Util.inherit(FlagEntity, PRESKISO.CarriableEntity);

	// FlagEntity.prototype.tick = function (delta, time) {
	// }

	FlagEntity.prototype.createObject = function () {
		var material = Physijs.createMaterial(
            new THREE.MeshLambertMaterial({color: 0x333333}),
            0.5, // high friction
            .2 // low restitution
        );
		this.object = new Physijs.CylinderMesh(
			new THREE.CylinderGeometry(0.2, 0.2, this.height),
			material,
			100
		);

		var flag = new THREE.Mesh(
			new THREE.PlaneGeometry(2, 1.2, 2, 2),
			new THREE.MeshLambertMaterial({color: this.team.color.clone().lerp(new THREE.Color(0, 0, 0), 0.3), side: THREE.DoubleSide})
		);
		flag.position.y = (this.height - 1.2) / 2;
		flag.position.x = 1;
		flag.castShadow = flag.receiveShadow = true;
		this.object.add(flag);
		this.object.position.copy(this.basePosition);
		this.object.position.y += this.height / 2;
		this.object.castShadow = this.object.receiveShadow = true;

		PRESKISO.Util.setCollisionGroup(this.object, "MOBILE");
	}

	FlagEntity.prototype.objectAdded = function (scene) {
		this.object.setAngularFactor(new THREE.Vector3(0, 0, 0));
		this.object.setLinearFactor(new THREE.Vector3(0, 0, 0));
	}

	FlagEntity.prototype.setCarrier = function (carrier) {
		this.parent.setCarrier.call(this, carrier);
		if (carrier === undefined) {
			this.object.rotation.set(0, 0, Math.PI / 10);
			this.object.__dirtyRotation = this.object.__dirtyPosition = true;
			this.object.setAngularFactor(new THREE.Vector3(0, 0, 0));
			this.object.setLinearFactor(new THREE.Vector3(1, 1, 1));
			this.object.setDamping(0.5, 0);
		} else {
			this.isInBase = false;
		}
	}

	FlagEntity.prototype.interact = function (player) {
		if (this.team == player.team) {
			if (!this.isInBase) {
				this.reset();
				return true;
			}
			return false;
		} else {
			return this.parent.interact.call(this, player);
		}
	}

	FlagEntity.prototype.reset = function () {
		this.object.setLinearFactor(new THREE.Vector3(0, 0, 0));
		this.object.position.copy(this.basePosition);
		this.object.position.y += this.height / 2;
		this.object.rotation.set(0, 0, 0);
		this.isInBase = true;
		this.object.__dirtyRotation = this.object.__dirtyPosition = true;
	}
	return FlagEntity;
})();
