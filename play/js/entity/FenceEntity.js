PRESKISO.FenceEntity = ( function () {
	var texture = THREE.ImageUtils.loadTexture( "images/woodenCrate.jpg" );
	texture.anisotropy = 4;
	var mat = new THREE.MeshPhongMaterial({ map: texture});
	var wall_material = Physijs.createMaterial(
            mat,
            0.5, // high friction
            1 // low restitution
        );
	wall_material.map.wrapS = wall_material.map.wrapT = THREE.RepeatWrapping;
    wall_material.impactSound = "impacts/wood_impact";

    var material_pool = [];

	function FenceEntity(dimensions, position) {
		PRESKISO.Entity.call(this);

		this.position = position;

		this.isDimmable = true;
		this.isHittable = true;

		this.hasBody = true;

		this.dimensions = dimensions;

		this.globalMaterial = wall_material;
		this.material_pool = material_pool;
		this.piercingFactor = 0.6;
	}

	PRESKISO.Util.inherit(FenceEntity, PRESKISO.Entity);

	FenceEntity.prototype.tick = function (delta) {
	}

	FenceEntity.prototype.createGeometry = function () {
		var stepSpacing = this.dimensions.z * 3;
		var geometry = new THREE.BoxGeometry(this.dimensions.x, this.dimensions.z * .9, this.dimensions.z * .9);
		geometry.applyMatrix( new THREE.Matrix4().makeTranslation(0, this.dimensions.y / 3, 0));
		if (this.dimensions.x >= stepSpacing) {
			var steps = Math.ceil(this.dimensions.x / stepSpacing), stepSize = this.dimensions.x / steps;
			for (var i = 0; i < steps; i++) {
				var postGeometry = new THREE.BoxGeometry(this.dimensions.z, this.dimensions.y, this.dimensions.z);
				postGeometry.applyMatrix( new THREE.Matrix4().makeTranslation((i + 0.5) * stepSize - this.dimensions.x / 2, 0, 0));
				geometry.merge(postGeometry);
			}
		}
		return geometry;
	}

	FenceEntity.prototype.createObject = function () {
  		var geometry;
  		var displayedGeometry;
  		if (this.dimensions.x >= this.dimensions.z) {
			geometry = new THREE.BoxGeometry(this.dimensions.x, this.dimensions.y, this.dimensions.z);
			displayedGeometry = this.createGeometry();
		} else {
			var temp = this.dimensions.x;
			this.dimensions.x = this.dimensions.z;
			this.dimensions.z = temp;
			geometry = new THREE.BoxGeometry(this.dimensions.x, this.dimensions.y, this.dimensions.z);
			geometry.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI / 2));
			displayedGeometry = this.createGeometry();
			displayedGeometry.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI / 2));
		}

		// var uvs = geometry.faceVertexUvs[0];
		// var size = this.dimensions;
		// var xFaces = [4, 5, 6, 7, 8, 9, 10, 11];
		// var yFaces = [8, 9, 10, 11];
		// var xCoeff = Math.max(1, Math.round(size.x / PRESKISO.GameController.mapTo3dRatio * 2) / 2);
		// var yCoeff = size.y / PRESKISO.GameController.mapTo3dRatio;
		// for (var i = 0; i < xFaces.length; i++) {
		// 	for (var j = 0; j < 3; j++) {
		// 		uvs[xFaces[i]][j].x *= xCoeff;
		// 	}
		// }
		// for (var i = 0; i < yFaces.length; i++) {
		// 	for (var j = 0; j < 3; j++) {
		// 		uvs[yFaces[i]][j].y *= yCoeff;
		// 	}
		// }
		// geometry.uvsNeedUpdate = true;

    	var wall = new Physijs.BoxMesh(
	        geometry,
	        wall_material,
	        0
	    );
	    wall.geometry = displayedGeometry;
	    wall.isHittable = true;
	    wall.receiveShadow = wall.castShadow = true;

	    this.object = wall;

	    this.object.position.copy(this.position);

		PRESKISO.Util.setCollisionGroup(this.object, "STATIC");
	}

	FenceEntity.prototype.objectAdded = function (scene) {
	}

	return FenceEntity;
})();
