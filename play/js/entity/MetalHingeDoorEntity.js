PRESKISO.MetalHingeDoorEntity = ( function () {
	var texture = THREE.ImageUtils.loadTexture( "images/door_metal.jpg" );
    var material = Physijs.createMaterial(
        new THREE.MeshPhongMaterial({ map: texture }),
        0.2, // high friction
        .6 // low restitution
    );
    material.impactSound = "impacts/metal_impact";

	function MetalHingeDoorEntity(dimensions, position, closedRotation, openedRotation) {
		PRESKISO.HingeDoorEntity.call(this, dimensions, position, closedRotation, openedRotation);

		this.maxHealth = this.health = 3000;
	}

	PRESKISO.Util.inherit(MetalHingeDoorEntity, PRESKISO.HingeDoorEntity);

	MetalHingeDoorEntity.prototype.setHealth = function (health) {
		MetalHingeDoorEntity.prototype.parent.setHealth.call(this, health);
		if (health > 0) {
			this.object.material.emissive.setRGB(1, 0, 0).multiplyScalar(Math.pow(1 - this.health / this.maxHealth, 2));
			this.object.material.ambient.setRGB(0.5, 0, 0).multiplyScalar(Math.pow(1 - this.health / this.maxHealth, 2));
		}

	}

	MetalHingeDoorEntity.prototype.createObject = function () {
		var mat = PRESKISO.Util.cloneMaterial(material);
	    var geometry = new THREE.BoxGeometry(this.dimensions.x, this.dimensions.y, this.dimensions.z);
	    var door = new Physijs.BoxMesh(
	        geometry,
	        mat,
	        this.dimensions.x * this.dimensions.y * this.dimensions.z * 20
	    );

	    for (var i = -1; i <= 1; i+= 2) {
			var cylinderGeometry = new THREE.CylinderGeometry(this.dimensions.z * 1.2, this.dimensions.z * 1.2, this.dimensions.y / 5, 8);
			cylinderGeometry.applyMatrix( new THREE.Matrix4().makeTranslation(this.dimensions.x / 2, this.dimensions.y / 3 * i, 0) );
			geometry.merge(cylinderGeometry);
		}

		var handleRadius = 0.15, handleWidth = 0.5, handleDepth = 0.3;
		var handleGeometry = new THREE.CylinderGeometry(handleRadius, handleRadius, handleDepth, 8);
        handleGeometry.applyMatrix( new THREE.Matrix4().makeRotationX(Math.PI / 2) );
		var handleGeometry2 = new THREE.BoxGeometry(handleWidth, handleRadius * 2, 0.1);
		handleGeometry2.applyMatrix( new THREE.Matrix4().makeTranslation((handleWidth + handleRadius) / 2, 0, (0.1 - handleDepth) / 2 ) );
		handleGeometry.merge(handleGeometry2);
		handleGeometry.applyMatrix( new THREE.Matrix4().makeTranslation(-this.dimensions.x / 2 + .4, 0, -(this.dimensions.z + handleDepth) / 2) );
		geometry.merge(handleGeometry);
		handleGeometry.applyMatrix( new THREE.Matrix4().makeRotationX(Math.PI) );
		geometry.merge(handleGeometry);	

	    door.isHittable = true;
	    door.receiveShadow = door.castShadow = true;
		door.position.x -= this.dimensions.x / 2;
		this.object = door;
		this.object.position.copy(this.hingePosition);
		this.object.quaternion.setFromAxisAngle(new THREE.Vector3(0, 1, 0), this.closedRotation);

		this.isMoving = true;

		PRESKISO.Util.setCollisionGroup(this.object, "MOBILE");
	}

	MetalHingeDoorEntity.prototype.die = function () {
		MetalHingeDoorEntity.prototype.parent.die.call(this);

		this.object.material = material;

		this.impactFactor = 10;
		this.object.setAngularFactor(new THREE.Vector3( 1, .3, 1 ));

		// cheat to make the door "blow" harder
		var factor = new THREE.Vector3(1, 1, 3);
		this.object.localToWorld(factor);
		factor.sub(this.object.position);
		factor.x = Math.abs(factor.x);
		factor.y = Math.abs(factor.y);
		factor.z = Math.abs(factor.z);

		this.object.setLinearFactor(factor);

		this.object.applyCentralImpulse(new THREE.Vector3(0, 50, 0));

		(function (that) {
			that.object.addEventListener( 'collision', function( other_object, relative_velocity, relative_rotation, contact_normal ) {
				if (that.impactFactor != 1) {
					that.object.setAngularFactor(new THREE.Vector3( 1, 1, 1 ));
					that.object.setLinearFactor(new THREE.Vector3( 1, 1, 1 ));
					that.impactFactor = 1;
				}
				var velocity = relative_velocity.lengthSq();
				if (velocity > 40) {
					PRESKISO.AudioManager.playSound(that.object.position, "metal_fall", velocity / 10, {randomizePlaybackRateBy: 0.15});
				}
			});
		})(this);
	}

	return MetalHingeDoorEntity;
})();
