PRESKISO.FlagHolderEntity = (function () {

	function FlagHolderEntity(gameMode, team, position) {
		PRESKISO.Entity.call(this);

		this.gameMode = gameMode;
		this.position = position;
		this.team = team;
		this.isCarried = false;
		this.radius = 3;
		this.height = 0.3;
	}
	PRESKISO.Util.inherit(FlagHolderEntity, PRESKISO.Entity);

	FlagHolderEntity.prototype.tick = function (delta, time) {
	}

	FlagHolderEntity.prototype.createObject = function () {
		var geometry = PRESKISO.Loader.getResource("flagholder").geometry;
		var scale = this.radius / 2;
		geometry.applyMatrix( new THREE.Matrix4().makeScale(scale, scale, scale));
		// var geometry = new THREE.CylinderGeometry(this.radius, this.radius, this.height, 16);
		this.object = new THREE.Mesh(
			geometry,
			new THREE.MeshLambertMaterial({color: this.team.color})
		);
		this.object.position.copy(this.position);
		// this.object.position.y += this.height / 2;
		this.object.castShadow = this.object.receiveShadow = true;
	}

	FlagHolderEntity.prototype.isSteppedOnBy = function (player) {
		if (Math.abs(player.object.position.y - this.object.position.y) < 2.32 + this.height * 2) {
			var pos1 = new THREE.Vector2(player.object.position.x, player.object.position.z);
			var pos2 = new THREE.Vector2(this.object.position.x, this.object.position.z);
			var flatDistance = pos1.distanceTo(pos2);
			if (flatDistance <= 1.05 + this.radius / 2) {
				return true;
			}
		}
		return false;
	}
	return FlagHolderEntity;
})();
