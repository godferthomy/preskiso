PRESKISO.PlayerEntity = (function () {
	var armPosition = new THREE.Vector3(0, 0.02825, 0.28451);
	var handPositionOnArm = new THREE.Vector3(0.13696, 0.13031, 1.38822);
	var handPositionOnTwoArms = new THREE.Vector3(0.23167, 0.08708, 1.06883);
	var texture = THREE.ImageUtils.loadTexture( "images/muzzle-flash.jpg" );
	var muzzleFlashMaterial = new THREE.MeshBasicMaterial({ map: texture, side: THREE.DoubleSide, transparent: true, blending: THREE.AdditiveBlending, depthWrite: false, depthTest: true });
	var grenadeMaterial = new THREE.MeshPhongMaterial({color: 0x003300});


	var grenadePrepareArmAngleOffset = -Math.PI / 5;
	var grenadeThrowArmAngleOffset = Math.PI / 7;

	function PlayerEntity(playerId, team, options) {
		PRESKISO.Entity.call(this);

		this.weaponMaterial = new THREE.MeshLambertMaterial({color: 0x333333, side: THREE.DoubleSide});
		this.isPill = true;
		color = team.color;
		this.playerId = playerId;
		this.team = team;
		this.name = options.name;
		this.color = options.color || new THREE.Color(1, 1, 1);
		this.model = options.model || "classic";
		this.object = undefined;
		this.weaponObject = undefined;
		this.arm = undefined;
		this.aim = undefined;
		this.aimLaser = undefined;
		this.aimDot = undefined;
		this.healthBar = undefined;
		this.movementFromKeys = new THREE.Vector3();
		this.rotationFromKeys = new THREE.Vector3();
		this.target = new THREE.Vector3();
		this.nextShot = 0;
		this.isShooting = false;
		this.aimPrecision = 1;
		this.health = 0;
		this.piercingFactor = 0.2;
		// this.weapons = PRESKISO.Constants.Weapons;
		// this.currentWeaponIndex = 0;
		// this.weaponAmmos = [];
		this.nextRayCheck = 0;
		this.nextWeaponBlockedCheck = 0;
		this.nextInteractiveObjectCheck = 0;
		this.aimXAngle = 0;
		this.weapons = [];
		this.weaponsById = {};
		for (var i = 0; i < 4; i++) {
			this.weapons.push([]);
		}
		for (var i = 0; i < PRESKISO.Constants.Weapons.length; i++) {
			var weaponData = PRESKISO.Constants.Weapons[i];
			var weapon = {
				id: weaponData.id,
				weaponData: weaponData
			};
			this.weapons[weaponData.type].push(weapon);
			this.weaponsById[weaponData.id] = weapon;
		}
		this.interactiveObjectInRange = undefined;
		this.getImpactSound = function () {return "impacts/body_impact";}
		this.lastRush = 0;
		this.movementRelativeToRotation = false;
		this.rotationDuplicateJoystick = true;

		// this.meleePower = 20;
		// this.meleeImpact = 30;
		this.meleePower = 10;
		this.meleeImpact = 500;
		this.meleeDuration = 0.2;

		this.isWeaponBlocked = false;

		this.flag = undefined;

		this.kills = 0;
		this.deaths = 0;
		this.score = 0;

		this.hasBody = true;
		this.isHittable = true;

		this.muzzleFlashAnimationStep = undefined;
		this.muzzleFlashSize;

		this.isLeftHanded = true;//Math.random() > 0.5;
	}
	   
	PRESKISO.Util.inherit(PlayerEntity, PRESKISO.Entity);

	PlayerEntity.prototype.setTeam = function (team) {
		this.team = team;
	}

	PlayerEntity.prototype.setMovementFromKeys = function (movementFromKeys) {
		this.movementFromKeys.copy(movementFromKeys);
	};

	PlayerEntity.prototype.setRotationFromKeys = function (rotationFromKeys) {
		this.rotationFromKeys.copy(rotationFromKeys);
	};

	PlayerEntity.prototype.setTarget = function (target) {
		this.target.copy(target);
	}

	PlayerEntity.prototype.getCurrentWeapon = function () {
		if (this.isCarryingEntity()) {
			return undefined;
		}
		return this.weapon;
	}

	PlayerEntity.prototype.addAmmo = function (id, amount) {
		var weapon = this.weaponsById[id];
		var wasEmpty = (weapon.magazineAmmo + weapon.ammo) == 0;
		weapon.ammo += amount;
		var sound = "ammo_pickup";
		if (wasEmpty) {
			if (id == this.weapon.id) {
				// current weapon, reload
				this.startReloading();
			} else {
				// weapon not owned yet (== empty)
				weapon.magazineAmmo = Math.min(weapon.ammo, weapon.weaponData.magazineAmmo);
				weapon.ammo -= weapon.magazineAmmo;
			}
			sound = "weapon_pickup";
		}
		PRESKISO.AudioManager.playSound(this.object.position, sound, 25);
	}

	PlayerEntity.prototype.heal = function (amount) {
		var newHealth = Math.min(100, this.health + amount);
		this.setHealth(newHealth);
		PRESKISO.AudioManager.playSound(this.object.position, "health_pickup", 20);
	}

	PlayerEntity.prototype.equipWeapon = function (weaponType, equipFirst) {
		if (this.isCarryingEntity()) {
			return;
		}
		var currentWeapon = this.getCurrentWeapon();
		var newWeapon;
		if (currentWeapon === undefined || equipFirst) {
			newWeapon = this.weapons[weaponType][this.weapons[weaponType].length - 1];
		} else {
			var index;
			if (weaponType != currentWeapon.weaponData.type) {
				index = this.weapons[weaponType].length - 1;
			} else {
				index = PRESKISO.Util.arrayObjectIndexOf(this.weapons[weaponType], "id", currentWeapon.id) - 1;
				if (index < 0) {
					index = this.weapons[weaponType].length - 1;
				}
			}
			var startIndex = index;
			do {
				newWeapon = this.weapons[weaponType][index];
				index--;
				if (index < 0) {
					index = this.weapons[weaponType].length - 1;
				}
			} while (index != startIndex && (newWeapon.ammo + newWeapon.magazineAmmo) == 0);
		}
		if (currentWeapon === undefined || (newWeapon.id != currentWeapon.id && (newWeapon.ammo + newWeapon.magazineAmmo) > 0)) {
			this.cancelReloading();
			this.weapon = newWeapon;
			this.stopShooting();
		    this.updateWeaponModel();
		    if (this.weapon.magazineAmmo == 0) {
		    	this.startReloading();
		    } else {
		    	this.nextShot = PRESKISO.GameController.getCurrentTime() + 0.1;
		    }
			this.armWithRecoil.rotation.set(Math.PI / 3.5, 0, 0);
			this.updateWeaponModel();

			this.muzzleFlashSize = this.weapon.weaponData.muzzleFlashSize || 1;
		}
	}

	PlayerEntity.prototype.startMeleeAttacking = function () {
		if (this.isDead() || this.isMeleeAttacking() || this.isPreparingGrenade()) {
			return;
		}
		this.cancelReloading();
		// this.weaponObject.remove(this.aim);
		this.aim.visible = false;
		this.meleeStartTime = PRESKISO.GameController.getCurrentTime();
		PRESKISO.AudioManager.playSound(this.object.position, "melee_attack", 40);
		this.applyMeleeAttack();
	}

	PlayerEntity.prototype.applyMeleeAttack = function () {
		this.arm.rotation.x = this.aimXAngle;
		this.arm.updateMatrixWorld();

		var origin = handPositionOnArm.clone();
		origin.z -= 1;
		var direction = new THREE.Vector3(0, 0, 1);
		this.arm.localToWorld(origin);
		direction.transformDirection(this.arm.matrixWorld).normalize();
		var distance = 2;
		var weapon = this.getCurrentWeapon();
		if (weapon !== undefined) {
			distance = Math.max(distance, weapon.weaponData.aimOffset.z);
		}
		var raycaster = new THREE.Raycaster(origin, direction, 0, distance);
		var intersect = PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getHittableObjects(), true );
		if (intersect !== undefined) {
			var targetEntity = PRESKISO.GameController.getEntityFromObject(intersect.object);
			if (targetEntity != this) {
				var impactPosition = intersect.point.clone();
			    intersect.object.worldToLocal(impactPosition);
			    var impactVector = direction.clone();
			    impactVector.y += 0.5;
			    impactVector.multiplyScalar(this.meleeImpact);
				PRESKISO.GameController.hitEntity(intersect.object, this, this.meleePower, intersect.point, impactPosition, impactVector);
				PRESKISO.AudioManager.playSound(this.object.position, "melee_hit", this.meleeImpact);
			}
		}	
	}

	PlayerEntity.prototype.finishMeleeAttacking = function () {
		if (!this.isMeleeAttacking()) {
			return;
		}
		// this.weaponObject.add(this.aim);
		this.aim.visible = true;
		this.meleeStartTime = undefined;
		this.nextMeleeTime = PRESKISO.GameController.getCurrentTime();
	}

	PlayerEntity.prototype.cancelMeleeAttacking = function () {
		if (!this.isMeleeAttacking()) {
			return;
		}
		// this.weaponObject.add(this.aim);
		this.aim.visible = true;
		this.meleeStartTime = undefined;
		this.nextMeleeTime = PRESKISO.GameController.getCurrentTime();
	}

	PlayerEntity.prototype.isMeleeAttacking = function () {
		return this.meleeStartTime !== undefined;
	}

	PlayerEntity.prototype.startReloading = function () {
		if (this.isDead() || this.isReloading() || this.isMeleeAttacking() || this.isCarryingEntity() || this.isPreparingGrenade() || this.weapon.ammo == 0 || this.weapon.magazineAmmo == this.weapon.weaponData.magazineAmmo) {
			return;
		}
		this.armWithRecoil.rotation.set(0, 0, 0);
		this.reloadEndTime = PRESKISO.GameController.getCurrentTime() + this.weapon.weaponData.reloadDuration;
		// this.weaponObject.remove(this.aim);
		this.aim.visible = false;
		this.reloadSound = PRESKISO.AudioManager.playSound(this.object.position, this.weapon.weaponData.reloadSound || "beretta_reload", 15);
	}

	PlayerEntity.prototype.finishReloading = function () {
		if (!this.isReloading()) {
			return;
		}
		this.arm.rotation.set(0, 0, 0);
		this.reloadEndTime = undefined;
		var reloadAmount = Math.min(this.weapon.weaponData.magazineAmmo - this.weapon.magazineAmmo, this.weapon.ammo);
		this.weapon.magazineAmmo = this.weapon.magazineAmmo + reloadAmount;
		this.weapon.ammo -= reloadAmount;
		this.nextShot = PRESKISO.GameController.getCurrentTime() + 0.1;
		// this.weaponObject.add(this.aim);
		this.aim.visible = true;
	}

	PlayerEntity.prototype.cancelReloading = function () {
		if (!this.isReloading()) {
			return;
		}
		this.reloadEndTime = undefined;
		// this.weaponObject.add(this.aim);
		this.aim.visible = true;
		PRESKISO.AudioManager.cancelSound(this.reloadSound);
	}

	PlayerEntity.prototype.isReloading = function () {
		return (this.reloadEndTime !== undefined);
	}

	PlayerEntity.prototype.isCarryingEntity = function () {
		return this.carriedEntity !== undefined;
	}

	PlayerEntity.prototype.getCarriedEntity = function () {
		return this.carriedEntity;
	}

	PlayerEntity.prototype.takeCarriableEntity = function (carriableEntity) {
		if (this.isCarryingEntity()) {
			return false;
		}
		if (carriableEntity instanceof PRESKISO.FlagEntity) {
			var flag = carriableEntity;
			if (flag.isInBase) {
				PRESKISO.AudioManager.playSound(this.object.position, "flag_taken", 15);
			}
		}
		this.carriedEntity = carriableEntity;

		var matrixWorldInverse = new THREE.Matrix4();
		matrixWorldInverse.getInverse( this.arm.matrixWorld );
		carriableEntity.getObject().applyMatrix( matrixWorldInverse );

		this.updateWeaponModel();

		this.armWithRecoil.rotation.set(0, 0, 0);
		this.armWithRecoil.__dirtyRotation = true;
		this.isWeaponBlocked = false;
		this.cancelMeleeAttacking();
		this.cancelReloading();
		this.stopShooting();
		return true;
	}

	PlayerEntity.prototype.dropCarriedEntity = function (underneath) {
		if (!this.isCarryingEntity()) {
			return;
		}
		this.cancelMeleeAttacking();

		var position = this.carriedEntity.getObject().position.clone();
		this.carriedEntity.getObject().applyMatrix( this.arm.matrixWorld );
		if (!underneath) {
			position.z = 1.5;
		}
		var throwVector = new THREE.Vector3(0, 0, 1);
		throwVector.transformDirection(this.arm.matrixWorld);
		this.arm.localToWorld(position);
		this.carriedEntity.drop(position);
		if (this.arm.rotation.x > Math.PI / 3) {
			var power = (this.arm.rotation.x - Math.PI / 3) * 300;
			vector.setLength(power);
			this.carriedEntity.getObject().applyCentralImpulse(throwVector);
		}
		this.carriedEntity = undefined;
		this.weaponObject = undefined;
		this.updateWeaponModel();
	}

	PlayerEntity.prototype.checkInteractiveObjectInFront = (function () {
		var raycaster = new THREE.Raycaster(new THREE.Vector3(), new THREE.Vector3(), 0, 1);
		var direction = raycaster.ray.direction;//new THREE.Vector3();
		var origin = raycaster.ray.origin;//new THREE.Vector3();
		return function () {
			var currentTime = PRESKISO.GameController.getCurrentTime();
			if (currentTime >= this.nextInteractiveObjectCheck) {
				this.interactiveObjectInRange = undefined;
				this.nextInteractiveObjectCheck = currentTime + 0.06;
			 //    this.arm.rotation.x = this.aimXAngle;
				// this.arm.updateMatrixWorld();
				direction.set(0, 0, 1);
			    origin.copy(this.object.position);
			    origin.y -= 1;
			   	var distance = 5;
			    direction.transformDirection(this.object.matrixWorld);
			    // raycaster.set(origin, direction);
			    raycaster.far = distance;
			    var intersect = PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getInteractiveObjects(), true );
			    if (intersect !== undefined) {
			    	var entity = PRESKISO.GameController.getEntityFromObject(intersect.object);
			    	// window.console.log(entity);
			    	if (entity.isInteractive) {
			    		this.interactiveObjectInRange = entity;
			    	}
			    }
			}
		};
	})();

	PlayerEntity.prototype.checkWeaponBlocked = function () {
		if (this.isDead() || this.isReloading() || this.isMeleeAttacking() || this.isCarryingEntity()) {
			return;
		}
		var currentTime = PRESKISO.GameController.getCurrentTime();
		if (currentTime >= this.nextWeaponBlockedCheck) {
			this.interactiveObjectInWeaponRange = undefined;
			this.isWeaponBlocked = false;
			var weapon = this.getCurrentWeapon();
			this.nextWeaponBlockedCheck = currentTime + 0.05;
			// var wasWeaponBlocked = this.isWeaponBlocked;
		    this.isWeaponBlocked = false;
		    this.arm.rotation.x = this.aimXAngle;
			this.arm.updateMatrixWorld();
			var muzzlePosition = new THREE.Vector3().addVectors(this.weaponObject.position, weapon.weaponData.aimOffset);
			var origin = muzzlePosition.clone();
		    origin.z = 0;
		   	var distance = muzzlePosition.z;
			var direction = new THREE.Vector3(0, 0, 1).add(origin);

		    this.arm.localToWorld(origin);
		   	this.arm.localToWorld(direction);
		   	direction.sub(origin).normalize();
		   	// distance += 1;
		    var raycaster = new THREE.Raycaster(origin, direction, 0, Math.max(distance, 5));
		    var intersect = PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getHittableObjects(), true, [this.hittableCapsule.id] );
		    if (intersect !== undefined) {
		    	this.firstIntersectInFrontOfWeapon = intersect;
		    	var entity = PRESKISO.GameController.getEntityFromObject(intersect.object);
		    	if (entity.isInteractive) {
		    		this.interactiveObjectInWeaponRange = entity;
		    	}
			    if (intersect.distance < distance) {
			    	this.isWeaponBlocked = true;
			    }
		    }
		}
	}

	PlayerEntity.prototype.updateAimLaser = function () {
		var shouldAppear = (!this.isReloading() && this.isMeleeAttacking && !this.isCarryingEntity() && !this.isWeaponBlocked);
		if (shouldAppear) {
			var weapon = this.getCurrentWeapon();
			if (this.aimLaser.parent != this.aim) {
				this.aim.add(this.aimLaser);
			}
			var currentTime = PRESKISO.GameController.getCurrentTime();
			if (currentTime >= this.nextRayCheck) {
				this.nextRayCheck = currentTime;// + 0.01;
				var origin = new THREE.Vector3(0, 0, 0);
			    var direction = THREE.Z_AXIS.clone().transformDirection(this.aim.matrixWorld);
			    this.aim.localToWorld(origin);
			   	var distance = weapon.weaponData.range;
			    var raycaster = new THREE.Raycaster(origin, direction, 0, distance);
			    var intersect = PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getHittableObjects(), true );
			    if (intersect !== undefined) {
			    	if (!this.aimDot.visible) {
				    	this.aimLaser.material.opacity = .15;
				    	this.aimDot.visible = true;
				    }
			    	distance = intersect.distance;
			    	this.aimDot.position.z = distance;
			    } else {
			    	if (this.aimDot.visible) {
				    	this.aimLaser.material.opacity = .05;
				    	this.aimDot.visible = false;
				    }
			    }
				this.aimLaser.geometry.vertices[1].z = distance;
				this.aimLaser.geometry.verticesNeedUpdate = true;
			}

		} else if (this.aimLaser.parent == this.aim) {
			this.aim.remove(this.aimLaser);
		}
	}

	PlayerEntity.prototype.updateWeaponModel = function () {
		this.oneArm.visible = true;
		this.twoArms.visible = false;
		if (this.weaponObject !== undefined) {
			this.arm.remove(this.weaponObject);
		}
		if (this.isCarryingEntity()) {
			this.weaponObject = this.carriedEntity.getObject();
			if (this.carriedEntity instanceof PRESKISO.FlagEntity) {
				this.weaponObject.rotation.set(0, Math.PI / 3, 0);
				this.weaponObject.position.copy(handPositionOnArm);
			} else if (this.carriedEntity instanceof PRESKISO.PigEntity) {
				this.weaponObject.position.copy(this.arm.position).multiplyScalar(-1);
				this.weaponObject.position.z += 1.5;
				this.weaponObject.rotation.set(0, -Math.PI / 2, 0);
			} else {
				this.weaponObject.position.copy(handPositionOnArm);
				// this.weaponObject.rotation.set(0, 0, 0);
			}
		} else if (this.isPreparingGrenade()) {
			this.weaponObject = this.grenadeObject;
		} else {
			var weapon = this.getCurrentWeapon();
			var resource = PRESKISO.Loader.getResource(weapon.weaponData.model);
	    	// var mat = new THREE.MeshFaceMaterial(resource.materials);
	    	// var mat = resource.materials[0].clone();
	    	var mat = this.weaponMaterial;
	    	this.weaponObject = new THREE.Mesh(
	        	resource.geometry,
		        mat
		    );
	    	this.weaponObject.receiveShadow = this.weaponObject.castShadow = true;
		    this.aim.position.copy(weapon.weaponData.aimOffset);
		    this.weaponObject.add(this.aim);

			if (weapon.weaponData.twoHanded) {
				this.oneArm.visible = false;
				this.twoArms.visible = true;
				this.weaponObject.position.copy(handPositionOnTwoArms);
			} else {
				this.weaponObject.position.copy(handPositionOnArm);
			}
		}
		// window.console.log(this.weaponObject);
	    this.arm.add(this.weaponObject);
	}

	PlayerEntity.prototype.die = function () {
		PlayerEntity.prototype.parent.die.call(this);
		this.throwGrenade(true);
		this.dropCarriedEntity(true);
		this.deathTime = PRESKISO.GameController.getCurrentTime();
		for (var i = 0; i < this.materials.length; i++) {
			this.materials[i].transparent = true;
		}
		for (var i = 0; i < this.weapons.length; i++) {
			for (var j = 0; j < this.weapons[i].length; j++) {
				var weapon = this.weapons[i][j];
				var ammo = Math.ceil((weapon.ammo + weapon.magazineAmmo) / 2);
				if (ammo > weapon.weaponData.magazineAmmo) {
					var position = new THREE.Vector3(0, 0, 0.5 + Math.random()).applyAxisAngle(THREE.Y_AXIS, Math.random() * Math.PI * 2).setY(0).add(this.object.position);
					var ammo = new PRESKISO.AmmoPowerupEntity(position, {permanent: false, weaponId: weapon.id, amount: ammo, lifespan: 30});
					PRESKISO.GameController.addEntity(ammo);
				}
				weapon.magazineAmmo = 0;
				weapon.ammo = 0;
			}
		}
		this.weaponObject.remove(this.aim);



		// this.body.fixedRotation = false;
		// this.body.updateMassProperties();
		this.object.setAngularFactor(new THREE.Vector3(1, 1, 1));
		this.impactFactor = 5;
		this.deaths++;
		PRESKISO.AudioManager.playSound(this.object.position, "wilhelm", 20);
	}

	PlayerEntity.prototype.toggleMovementMode = function () {
		this.movementRelativeToRotation = !this.movementRelativeToRotation;
	}

	PlayerEntity.prototype.toggleRotationMode = function () {
		this.rotationDuplicateJoystick = !this.rotationDuplicateJoystick;
	}

	PlayerEntity.prototype.tick = function (delta, time) {
		PlayerEntity.prototype.parent.tick.call(this, delta, time);
		if (this.muzzleFlashAnimationStep != undefined) {
			this.muzzleFlashAnimationStep += delta * 12;
			if (this.muzzleFlashAnimationStep > 1) {
				this.muzzleFlashAnimationStep = undefined;
				this.muzzleFlash.visible = false;
			} else {
				var scale = (1 - this.muzzleFlashAnimationStep) * this.muzzleFlashSize;
				this.muzzleFlash.scale.set(scale, scale, scale);
			}
		}
		if (this.health <= 0) {
			var timeSinceDeath = time - this.deathTime;
			if (timeSinceDeath > 3) {
				PRESKISO.GameController.playerDiedAndDisappeared(this);
				// PRESKISO.GameController.respawnPlayer(this.playerId);
			} else if (timeSinceDeath > 2) {
				var alpha = 3 - timeSinceDeath;
				for (var i = 0; i < this.materials.length; i++) {
					this.materials[i].opacity = alpha;
				}
			}
			return;
		}
		// var pos = PRESKISO.AI.PathFinder.AStar.get3dToMapPosition(this.object.position, true);
		// window.console.log(this.object.position, pos);
		this._isStanding = undefined;

		if (this.isReloading()) {
			if (this.reloadEndTime <= time) {
				this.finishReloading();
			}
		} else if (this.isShooting) {
			this.shoot();
		} else if (this.isMeleeAttacking) {
			var timeElapsed = time - this.meleeStartTime;
			if (timeElapsed > this.meleeDuration) {
				this.finishMeleeAttacking();
			}
		}
		var weapon = this.getCurrentWeapon();

		this.cameraMotionTarget.position.copy(this.object.getLinearVelocity()).multiplyScalar(0.5).add(this.object.position);

		// MOVEMENT
	    if (this.movementFromKeys.lengthSq() > 0 && this.isStanding()) {
			var dir = this.movementFromKeys.clone();
			if (this.movementRelativeToRotation)
				dir.applyMatrix4(new THREE.Matrix4().extractRotation(this.object.matrix));
	        var speedSq = this.object.getLinearVelocity().lengthSq();
	        var maxSpeedSq = 100;
	        if (speedSq < maxSpeedSq) {

					dir.transformDirection(cameraFlat.matrixWorld).setY(0).multiplyScalar(-1).setLength(this.movementFromKeys.length());

	            if (this.isRushing) {
	            	dir.multiplyScalar(15);
	            	this.isRushing = false;
	            	this.lastRush = clock.getElapsedTime();
	            }
	            dir.multiplyScalar(1000);
	            // this.body.applyImpulse(new CANNON.Vec3(dir.x, dir.y, dir.z), this.body.position);
	            this.object.applyCentralForce(dir);
	        }
	    }

	    // ROTATION
		this.object.__dirtyRotation = true;
	    var rot = this.rotationFromKeys;
	    this.aimXAngle = rot.x;
		// this.arm.rotation.x = this.aimXAngle;
		// this.arm.updateMatrixWorld();
		if (this.rotationDuplicateJoystick) {
			if (this.target.lengthSq() > 0.2) {
				// var actualTarget = this.target.clone();
				// camera.localToWorld(actualTarget);
				// actualTarget.sub(camera.position).setY(0).multiplyScalar(-1).normalize().add(this.object.position);
				var horizontalTarget = this.target.clone().setY(this.object.position.y);
				this.object.lookAt(horizontalTarget);
				// this.object.__dirtyRotation = true;
			}
		} else {
		    if (rot.y != 0) {
		    	this.object.rotateOnAxis(THREE.Y_AXIS, Math.pow(rot.y, 5) * delta * 5 * ((!this.isSniping) ? 1 : 0.2) );
		    }
		}
		// this.arm.rotation.x = Math.pow(rot.x, 5) * Math.PI / 3;

		// if (this.nextRayCheck <= time) {
		// 	// this.firstIntersectInFront = undefined;
		// 	// var interactiveObjectInRange = undefined;
		// 	// var origin = new THREE.Vector3(0, 0, 0);
		//  //    this.object.localToWorld(origin);
		//  //    var direction = THREE.Z_AXIS.clone();
		//  //   	this.object.localToWorld(direction);
		//  //   	direction.sub(origin);
		//  //    var raycaster = new THREE.Raycaster(origin, direction, 0, 5);
		//  //    var intersect = PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getHittableObjects(), true );
		//  //    if (intersect !== undefined) {
		//  //    	this.firstIntersectInFront = intersect;
		//  //    	var entity = PRESKISO.GameController.getEntityFromObject(intersect.object);
		//  //    	if (entity.isInteractive) {
		//  //    		interactiveObjectInRange = entity;
		//  //    	}
		//  //    }

		// 	// this.nextRayCheck = time + 0.03;
		// 	// if (!this.isReloading() && !this.isMeleeAttacking && !this.isCarryingFlag) {
				


		// 	//     if (!this.isMeleeAttacking && !this.isWeaponBlocked) {
		// 	//     	if (wasWeaponBlocked) {
		// 	//     		this.weaponObject.add(this.aim);
		// 	//     	}
		// 	// 		// UPDATE AIM LASER
		// 	// 	} else if (!wasWeaponBlocked) {
		// 	// 		this.weaponObject.remove(this.aim);
		// 	// 	}
		// 	// }
		// 	// this.interactiveObjectInRange = interactiveObjectInRange;
		// }

		this.checkInteractiveObjectInFront();
		this.checkWeaponBlocked();
		this.updateAimLaser();

		if (this.isReloading()) {
			this.arm.rotation.x = Math.PI / 2.4;
		} else if (this.isMeleeAttacking()) {
			var frame = (time - this.meleeStartTime) / this.meleeDuration;
			this.arm.rotation.x = -frame * 0.4 * Math.PI / 2;
		} else if (this.isPreparingGrenade()) {
	    	this.arm.rotation.x = this.aimXAngle;
	    	this.armWithRecoil.rotation.x = grenadePrepareArmAngleOffset;
		} else {
			this.arm.rotation.x = this.aimXAngle;
			if (!this.isCarryingEntity() && weapon.weaponData.recoil !== undefined) {
		    	var coeff = Math.min(1, 1 - delta * 6);
		    	var obj = this.armWithRecoil;
		    	obj.rotation.x *= coeff;
		    	obj.rotation.y *= coeff;
		    	obj.rotation.z *= coeff;
		    }
		}
		if (this.isWeaponBlocked) {
	    	this.arm.rotation.x = Math.PI / 2.2;
		}
	    if (this.isInteracting) {
	    	this.interact();
	    }
	};

	PlayerEntity.prototype.jump = function () {
		if (!this.isStanding()) {
			return;
		}
        // this.body.applyImpulse(new CANNON.Vec3(0, this.object.mass * 6, 0), this.body.position);
		this.object.applyCentralImpulse(new THREE.Vector3(0, 20 * this.object.mass, 0));
	}

	PlayerEntity.prototype.isStanding = function () {
		if (this._isStanding === undefined) {
			var touches = this.object._physijs.touches;
			for (var i = 0; i < touches.length; i++) {
				var obj = scene._objects[touches[i]];
				var ent = PRESKISO.GameController.getEntityFromObject(obj);
				if (ent instanceof PRESKISO.GroundEntity) {
					this._isStanding = true;
					break;
				}
			}
			if (!this._isStanding) {
				if (Math.abs(this.object.getLinearVelocity().y) > 2) {
					this._isStanding = false;
				} else {
					var origin = this.object.position.clone();
					origin.y -= cylinderHeight - 0.4;
					var raycaster = new THREE.Raycaster(origin, new THREE.Vector3(0, -1, 0), 0, 0.6);
					var intersect = PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getHittableObjects(), true, [this.hittableCapsule.id] );
					if (intersect !== undefined) {
						this._isStanding = true;
					}
				}
			}
		}
		return this._isStanding;
	};

	PlayerEntity.prototype.rush = function () {
		if (clock.getElapsedTime() - this.lastRush > 1) {
			this.isRushing = true;
		}
	}

	PlayerEntity.prototype.interact = function () {
		var object = this.interactiveObjectInWeaponRange || this.interactiveObjectInRange;
		if (object !== undefined) {
			object.interact(this);
		}
		this.isInteracting = false;
	}

	PlayerEntity.prototype.isPreparingGrenade = function () {
		return this.prepareGrenadeTime != undefined;
	}

	PlayerEntity.prototype.prepareGrenade = function () {
		if (this.isDead()) {
			return;
		}
		this.prepareGrenadeTime = PRESKISO.GameController.getCurrentTime();
		this.cancelReloading();
		this.updateWeaponModel();
	}

	PlayerEntity.prototype.throwGrenade = function (justDrop) {
		if (!this.isPreparingGrenade()) {
			return;
		}
		var prepareDuration = Math.min(1, PRESKISO.GameController.getCurrentTime() - this.prepareGrenadeTime);
		var origin = this.grenadeObject.position.clone();
		var direction = new THREE.Vector3();
		if (!justDrop) {
			direction.set(0, 0, 1);
			direction.applyAxisAngle(THREE.X_AXIS, grenadeThrowArmAngleOffset);
	   		direction.transformDirection(this.arm.matrixWorld).normalize().multiplyScalar(70 * prepareDuration);
	   	}
	    this.arm.localToWorld(origin);
	 // var origin = new THREE.Vector3(0.8, 0.5, 1.5);
		// if (!this.isLeftHanded) {
		// 	origin.x *= -1;
		// }
		// origin.applyAxisAngle(THREE.X_AXIS, this.aimXAngle);
		// var direction = new THREE.Vector3(0, 0.1, 1);
		// direction.applyAxisAngle(THREE.X_AXIS, this.aimXAngle);
	 //    this.object.localToWorld(origin);
	 //   	direction.transformDirection(this.object.matrixWorld).normalize().multiplyScalar(100 * prepareDuration);
	   	// origin.y += 0.3;
	   	// origin.z += 0.5;
	   	direction.add(this.object.getLinearVelocity());
	   	var grenade = new PRESKISO.GrenadeEntity(this, origin, direction, 100);
	   	PRESKISO.GameController.addEntity(grenade);
		PRESKISO.AudioManager.playSound(this.object.position, "grenade_throw", 13 * (1 + prepareDuration), {randomizePlaybackRateBy: 0.1});

		this.prepareGrenadeTime = undefined;
		this.updateWeaponModel();
	}

	PlayerEntity.prototype.startShooting = function () {
		if (this.isDead() || ((this.isWeaponBlocked) && !this.getCurrentWeapon().weaponData.auto) || this.isMeleeAttacking() || this.isPreparingGrenade()) {
			return;
		}
		if (this.isReloading()) {
			if (this.weapon.magazineAmmo > 0) {
				this.cancelReloading();
			}
			return;
		}
		this.isShooting = true;
	}

	PlayerEntity.prototype.stopShooting = function () {
		this.isShooting = false;
	}

	PlayerEntity.prototype.shoot = function () {
		if (this.isReloading() || this.isMeleeAttacking() || this.isWeaponBlocked) {
			// cannot shoot in that state
			return false;
		}
		if (this.isCarryingEntity()) {
			// won't shoot but will drop the item carried
			this.dropCarriedEntity();
	    	this.stopShooting();
			return false;
		}
		if (this.nextShot > PRESKISO.GameController.getCurrentTime()) {
			// not ready yet
			return false;
		}

		var weapon = this.getCurrentWeapon();
	    var weaponData = weapon.weaponData;

		if (weapon.magazineAmmo <= 0) {
			this.stopShooting();
			PRESKISO.AudioManager.playSound(this.object.position, "empty_weapon", 25, {randomizePlaybackRateBy: 0.1});
			this.startReloading();
			return false;
		}

		weapon.magazineAmmo--;
		bulletCount = weaponData.bullets || 1;

		if (!weaponData.auto) {
			this.stopShooting();
		}
	    this.nextShot = PRESKISO.GameController.getCurrentTime() + weapon.weaponData.rateOfFire;

		var origin = new THREE.Vector3(0, 0, 0);
	    this.aim.localToWorld(origin);
	    var direction = THREE.Z_AXIS.clone();
	   	// this.aim.localToWorld(direction);
	   	// direction.sub(origin);
	   	// direction.normalize();
		direction.transformDirection(this.aim.matrixWorld);

	   	this.armWithRecoil.rotateOnAxis(THREE.X_AXIS, (Math.random() - 0.5) * weaponData.recoil / 4);
	   	this.armWithRecoil.rotateOnAxis(THREE.Y_AXIS, (Math.random() - 0.5) * weaponData.recoil / 2);
	   	// var yRotation = Math.random() * 0.5;
	   	// this.arm.rotateOnAxis(new THREE.Vector3(-yRotation, (1 - yRotation) * (Math.random()>0.5?1:-1), 0), (1 + Math.random()) * weaponData.recoil / 4);


	   	var offset = 1 - weaponData.accuracy;
	   	for (var i = 0; i < bulletCount; i++) {
	   		var bulletDirection = direction.clone();
		    bulletDirection.add({x: (Math.random() - .5) * offset, y: (Math.random() - .5) * offset, z: (Math.random() - .5) * offset});
			var bullet = new PRESKISO.BulletEntity(this, origin, bulletDirection, {weaponData: weaponData});
			PRESKISO.GameController.addEntity(bullet);
			// bullet.getObject().translateOnAxis(THREE.Z_AXIS, Math.random());
		}
		var worldPosition = new THREE.Vector3().setFromMatrixPosition( this.aim.matrixWorld );
		PRESKISO.AudioManager.playSound(worldPosition, weaponData.sound, 10, {randomizePlaybackRateBy: 0.1});

		if (weapon.magazineAmmo == 0) {
			this.startReloading();
		}
		this.muzzleFlashAnimationStep = 0;
		this.muzzleFlash.visible = true;
		this.muzzleFlash.rotation.z = Math.random() * Math.PI * 2;
		return true;
	}

	PlayerEntity.prototype.getHit = function (power, objectPosition, sourceEntity) {
		PlayerEntity.prototype.parent.getHit.call(this, power, objectPosition, sourceEntity);
		if (objectPosition.y > 1.3) {
			power *= 2;
			if (power > this.health) {
	        	// audioManager.playSound(null, "headshot", 5);
			}
		}
		var health = Math.max(0, this.health -power);
		this.setHealth(health);
		return this.health;
	}

	PlayerEntity.prototype.setHealth = function (health) {
		if (health <= 0 && this.health <= 0) {
			return;
		}
		this.health = health;
		if (this.health <= 0) {
			this.die();
		}
	}

	var cylinderHeight = 2.60, radius = 1.06, segments = 8;

	PlayerEntity.prototype.createBody = function () {
		var cylinderShape = new CANNON.Cylinder(radius, radius, cylinderHeight, segments);
		this.body = new CANNON.Body({ mass: 10 });
        this.body.addShape(cylinderShape);
        for (var i = -1; i <= 1; i+= 2) {
        	var sphereShape = new CANNON.Sphere(radius);
        	this.body.addShape(sphereShape, new CANNON.Vec3(0, cylinderHeight / 2 * i, 0));
		}
	}

	PlayerEntity.prototype.createObject = function () {
		var muzzleFlashSize = 3.5;
		this.grenadeObject = new THREE.Mesh(PRESKISO.Loader.getResource("weapons/grenade").geometry, grenadeMaterial);
		this.grenadeObject.position.copy(handPositionOnArm);

		var muzzleFlashGeometry = new THREE.PlaneGeometry(muzzleFlashSize, muzzleFlashSize);
		muzzleFlashGeometry.applyMatrix( new THREE.Matrix4().makeRotationX(Math.PI / 2));
		muzzleFlashGeometry.merge(new THREE.PlaneGeometry(muzzleFlashSize, muzzleFlashSize));
		muzzleFlashGeometry.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI / 2));
		muzzleFlashGeometry.applyMatrix( new THREE.Matrix4().makeTranslation(0, 0, muzzleFlashSize / 1.8));
		this.muzzleFlash = new THREE.Mesh(
			muzzleFlashGeometry,
			muzzleFlashMaterial);
		this.muzzleFlash.visible = false;


	    var hittableCapsuleGeometry = new THREE.CylinderGeometry(radius, radius, cylinderHeight, segments);
	    for (var i = -1; i <= 1; i+= 2) {
			var geometry = new THREE.SphereGeometry(radius, segments, segments / 2);
			geometry.applyMatrix( new THREE.Matrix4().makeTranslation(0, cylinderHeight / 2 * i, 0) );
			hittableCapsuleGeometry.merge(geometry);
		}
		this.hittableCapsule = new THREE.Mesh(
			hittableCapsuleGeometry,
			new THREE.MeshBasicMaterial({color: new THREE.Color(1, 0, 1)})
			);
		this.hittableCapsule.visible = false;
		this.hittableCapsule.isHittable = true;


		this.materials = [];
		var armSideFactor = (this.isLeftHanded) ? 1 : -1;
	    var mass = 10;
	    var resourceName = "pill-" + this.model;
	    var resource = PRESKISO.Loader.getResource(resourceName);
	    var materials = [];
	    for (var i = 0; i < resource.materials.length; i++) {
	    	var material = resource.materials[i].clone();
	    	materials.push(material);
	    	this.materials.push(material);
	    }
	    var mat = new THREE.MeshFaceMaterial(materials);
	    this.object = new Physijs.CapsuleMesh(
	        hittableCapsuleGeometry,
	        Physijs.createMaterial(
	            mat,
	            1, // high friction
	            0.05 // low restitution
	        ),
	        10
	    );
	    this.object.isHittable = true;
	    this.object.material.materials[this.object.material.materials.length - 1].color.copy(this.color);
	    this.object.geometry = resource.geometry;
	    // this.object.add(this.hittableCapsule);
	    this.object.receiveShadow = this.object.castShadow = true;

	    this.armWithRecoil = new THREE.Object3D();
		this.arm = new THREE.Object3D();
	    this.arm.position.copy(armPosition);
	    this.armWithRecoil.add(this.arm);
	    this.object.add(this.armWithRecoil);

		this.aim = new THREE.Object3D();

		this.aim.add(this.muzzleFlash);

	    var material = new THREE.LineBasicMaterial({
	        color: 0xFF0000, //this.color,
	        transparent: true,
	        opacity: 0.1,
	        depthWrite: false,
	    });

	    var geometry = new THREE.Geometry();
	    geometry.vertices.push(
	        new THREE.Vector3( 0, 0, 0 ),
	        new THREE.Vector3( 0, 0, 10 )
	    );
	    this.aimLaser = new THREE.Line( geometry, material );
	    this.aimLaser.transparent = true;
	    this.aim.add(this.aimLaser);

	    this.aimDot = new THREE.Mesh(
	        new THREE.SphereGeometry(.1),
	        new THREE.MeshBasicMaterial({ color: this.color })
	    );

	    var geometry = PRESKISO.Loader.getResource("pill-arm").geometry;
	    var material = new THREE.MeshPhongMaterial({color: this.color});
	    this.materials.push(material);
	    this.oneArm = new THREE.Mesh(geometry, material);
	    this.oneArm.receiveShadow = this.oneArm.castShadow = true;
	    this.arm.add(this.oneArm);

	    var geometry = PRESKISO.Loader.getResource("pill-arm2").geometry;
	    var material = new THREE.MeshPhongMaterial({color: this.color});
	    this.materials.push(material);
	    this.twoArms = new THREE.Mesh(geometry, material);
	    this.twoArms.receiveShadow = this.twoArms.castShadow = true;
	    this.arm.add(this.twoArms);



		// if (this.playerId == 0) {
			// 	this.arm.add(flashLight);
			// 	this.arm.add(flashLightTarget);
		// }


	    this.cameraAimTarget = new THREE.Object3D();
	    this.cameraAimTarget.position.set(0, 0, 15);
	    this.object.add(this.cameraAimTarget);

	    this.cameraMotionTarget = new THREE.Object3D();
	    
		PRESKISO.Util.setCollisionGroup(this.object, "PILL");
	}

	PlayerEntity.prototype.objectAdded = function () {
		PlayerEntity.prototype.parent.objectAdded.call(this);

		this.object.setAngularFactor(new THREE.Vector3(0, 0, 0));
		// this.object.setLinearFactor(new THREE.Vector3(0.2, 0.2, 0.2));
		this.impactFactor = 1;

		this.setHealth(100);
		for (var i = 0; i < this.materials.length; i++) {
			this.materials[i].opacity = 1;
			this.materials[i].transparent = false;
		}
		this.object.position.copy(this.team.getSpawningPoint());
		this.object.position.y = 2.5;
		this.object.quaternion.setFromAxisAngle(new THREE.Vector3(0, 1, 0), Math.random() * Math.PI * 2);
		this.object.__dirtyPosition = true;
		this.object.__dirtyRotation = true;

		scene.add(this.cameraMotionTarget);
	    PRESKISO.GameController.getCameraController().addTrackedObject(this.cameraAimTarget);
	    PRESKISO.GameController.getCameraController().addTrackedObject(this.cameraMotionTarget);

		for (var i = 0; i < this.weapons.length; i++) {
			for (var j = 0; j < this.weapons[i].length; j++) {
				var weapon = this.weapons[i][j];
				var weaponData = this.weapons[i][j].weaponData;
				weapon.magazineAmmo = Math.min(weaponData.initialAmmo, weaponData.magazineAmmo);
				weapon.ammo = weaponData.initialAmmo - weapon.magazineAmmo;
			}
		}
		this.equipWeapon(0, true);
	}

	PlayerEntity.prototype.objectRemoved = function () {
	    PRESKISO.GameController.getCameraController().removeTrackedObject(this.cameraAimTarget);
		scene.add(this.cameraMotionTarget);
	    PRESKISO.GameController.getCameraController().removeTrackedObject(this.cameraMotionTarget);
	}

	return PlayerEntity;
})();
