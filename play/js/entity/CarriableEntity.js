PRESKISO.CarriableEntity = (function () {
	function CarriableEntity() {
		PRESKISO.InteractiveEntity.call(this);
		this.isCarried = false;
		this.carrier = undefined;
		this.autoPickup = false;
		this.radius = 0;
		this.height = 0;
		this.name = "Unknown";
	}

	PRESKISO.Util.inherit(CarriableEntity, PRESKISO.InteractiveEntity);

	CarriableEntity.prototype.tick = function (delta, time) {
		if (this.isCarried || !this.autoPickup) {
			return;
		}
		var players = PRESKISO.GameController.getPlayers();
		for (var i = 0; i < players.length; i++) {
			var player = players[i];
			if (player.isAlive() && this.isInRange(player)) {
				if (this.interact(player)) {
					break;
				}
			}
		}
	}

	CarriableEntity.prototype.isInRange = function (player) {
		if (Math.abs(player.object.position.y - this.object.position.y) < 2.32 + this.height / 2) {
			var pos1 = new THREE.Vector2(player.object.position.x, player.object.position.z);
			var pos2 = new THREE.Vector2(this.object.position.x, this.object.position.z);
			var flatDistance = pos1.distanceTo(pos2);
			if (flatDistance <= 1.05 + this.radius / 2) {
				return true;
			}
		}
		return false;
	}

	CarriableEntity.prototype.setCarrier = function (carrier) {
		this.isCarried = carrier !== undefined;
		if (!this.isCarried) {
			scene.add(this.object);
		} else {
		}
		this.carrier = carrier;
	}

	CarriableEntity.prototype.drop = function (position) {
		this.object.position.copy(position);
		this.setCarrier(undefined);
	}

	CarriableEntity.prototype.interact = function (player) {
		if (this.isCarried) {
			return false;
		}
		var accepted = player.takeCarriableEntity(this);
		if (accepted) {
			this.setCarrier(player);
		}
		return accepted;
	}

	return CarriableEntity;
})();
