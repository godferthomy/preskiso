PRESKISO.PowerupEntity = (function () {
	function PowerupEntity(position, options) {
		PRESKISO.Entity.call(this, gameController);

		options = options || {};

		this.lastCheckingTime = -10;
		this.nextSpawn = undefined;
		if (position !== undefined) {
			this.position = position.clone();
		}
		if (options.lifespan) {
			this.deathTime = PRESKISO.GameController.getCurrentTime() + options.lifespan;
		}
		this.permanent = !!options.permanent;
	}

	PRESKISO.Util.inherit(PowerupEntity, PRESKISO.Entity);

	PowerupEntity.prototype.tick = function (delta, time) {
		if (this.deathTime !== undefined) {
			if (time >= this.deathTime) {
				PRESKISO.GameController.removeEntity(this);
				return;
			}
		}
		if (this.nextSpawn !== undefined) {
			if (time >= this.nextSpawn) {
				this.respawn();
			}
			return;
		}
		var player;
		if (time - this.lastCheckingTime > 0.1) {
			this.lastCheckingTime = time;
			var players = PRESKISO.GameController.getPlayers();
			player, minDistance = 10;
			for (var i = 0; i < players.length; i++) {
				if (players[i].isAlive()) {
					var distance = players[i].getObject().position.distanceToSquared(this.object.position)
					if (distance < minDistance) {
						player = players[i];
						minDistance = distance;
					}
				}
			}
			if (player !== undefined) {
				if (this.giveBonus(player)) {
					if (this.permanent) {
						this.object.visible = false;
						this.nextSpawn = time + 5;
					} else {
						PRESKISO.GameController.removeEntity(this);
					}
				}
			}
		}
	}

	PowerupEntity.prototype.giveBonus = function (player) {
		// IMPLEMENT THIS METHOD TO GIVE A BONUS
		// return true if the bonus is comsumed
		return false;
	};

	PowerupEntity.prototype.respawn = function () {
		this.nextSpawn = undefined;
		this.object.visible = true;
	}

	return PowerupEntity;
})();
