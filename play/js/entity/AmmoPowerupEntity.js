PRESKISO.AmmoPowerupEntity = (function () {
	var mat = new THREE.MeshLambertMaterial({color: 0x333333, side: THREE.DoubleSide});

	function AmmoPowerupEntity(position, options) {
		PRESKISO.PowerupEntity.call(this, position, options);

		options = options || {};

		this.lastCheckingTime = -10;
		this.weaponId = options.weaponId;
		if (this.weaponId === undefined) {
			this.weaponId = Math.floor(Math.random() * PRESKISO.Constants.Weapons.length);
		}
		this.ammoAmount = options.amount;
		if (this.ammoAmount === undefined) {
			this.ammoAmount = Math.round(PRESKISO.Constants.Weapons[this.weaponId].powerupAmmo * (Math.random() * 0.7 + 1));
		}
	}

	PRESKISO.Util.inherit(AmmoPowerupEntity, PRESKISO.PowerupEntity);

	AmmoPowerupEntity.prototype.giveBonus = function (player) {
		player.addAmmo(this.weaponId, this.ammoAmount);
		return true;
	};

	AmmoPowerupEntity.prototype.tick = function (delta, time) {
		AmmoPowerupEntity.prototype.parent.tick.call(this, delta, time);
		if (this.nextSpawn !== undefined) {
			return;
		}
		this.object.rotateOnAxis(THREE.Y_AXIS, delta * 1.5);
	};

	AmmoPowerupEntity.prototype.createObject = function () {
		var weapon = PRESKISO.Constants.Weapons[this.weaponId];
		var resource = PRESKISO.Loader.getResource(weapon.model);
		this.object = new THREE.Mesh(
	    	resource.geometry,
	        mat
	    );
		this.object.receiveShadow = this.object.castShadow = true;
		this.object.rotateOnAxis(THREE.Y_AXIS, Math.random() * Math.PI * 2);
	};

	AmmoPowerupEntity.prototype.respawn = function () {
		AmmoPowerupEntity.prototype.parent.respawn.call(this);
		var newWeaponId = Math.floor(Math.random() * PRESKISO.Constants.Weapons.length);
		if (newWeaponId != this.weaponId) {
			this.weaponId = newWeaponId;
			this.ammoAmount = Math.round(PRESKISO.Constants.Weapons[this.weaponId].powerupAmmo * (Math.random() * 0.5 + 1));
			PRESKISO.GameController.removeEntity(this);
			PRESKISO.GameController.addEntity(this);
		}
	}

	AmmoPowerupEntity.prototype.objectAdded = function () {
		this.object.position.copy(this.position);
	}

	AmmoPowerupEntity.prototype.objectRemoved = function () {
		this.object = undefined;
	}

	return AmmoPowerupEntity;
})();
