PRESKISO.PlayerEntity = (function () {

	function PlayerEntity(playerId, team, name, color) {
		PRESKISO.Entity.call(this);

		this.isPill = true;
		color = team.color;
		this.playerId = playerId;
		this.team = team;
		this.name = name;
		this.color = color || new THREE.Color(1, 1, 1);
		this.object = undefined;
		this.weaponObject = undefined;
		this.arm = undefined;
		this.aim = undefined;
		this.aimLaser = undefined;
		this.aimDot = undefined;
		this.healthBar = undefined;
		this.movementFromKeys = new THREE.Vector3();
		this.rotationFromKeys = new THREE.Vector3();
		this.target = new THREE.Vector3();
		this.nextShot = 0;
		this.isShooting = false;
		this.aimPrecision = 1;
		this.health = 0;
		this.piercingFactor = 0.2;
		// this.weapons = PRESKISO.Constants.Weapons;
		// this.currentWeaponIndex = 0;
		// this.weaponAmmos = [];
		this.nextRayCheck = 0;
		this.nextWeaponBlockedCheck = 0;
		this.nextInteractiveObjectCheck = 0;
		this.aimXAngle = 0;
		this.weapons = [];
		this.weaponsById = {};
		for (var i = 0; i < 4; i++) {
			this.weapons.push([]);
		}
		for (var i = 0; i < PRESKISO.Constants.Weapons.length; i++) {
			var weaponData = PRESKISO.Constants.Weapons[i];
			var weapon = {
				id: weaponData.id,
				weaponData: weaponData
			};
			this.weapons[weaponData.type].push(weapon);
			this.weaponsById[weaponData.id] = weapon;
		}
		this.interactiveObjectInRange = undefined;
		this.getImpactSound = function () {return "impacts/body_impact";}
		this.lastRush = 0;
		this.movementRelativeToRotation = false;
		this.rotationDuplicateJoystick = true;

		// this.meleePower = 20;
		// this.meleeImpact = 30;
		this.meleePower = 10;
		this.meleeImpact = 500;
		this.meleeDuration = 0.2;

		this.isWeaponBlocked = false;

		this.flag = undefined;

		this.kills = 0;
		this.deaths = 0;
		this.score = 0;

		this.isHittable = true;

		this.isLeftHanded = true;//Math.random() > 0.5;
	}
	   
	PRESKISO.Util.inherit(PlayerEntity, PRESKISO.Entity);

	PlayerEntity.prototype.setTeam = function (team) {
		this.team = team;
	}

	PlayerEntity.prototype.setMovementFromKeys = function (movementFromKeys) {
		this.movementFromKeys.copy(movementFromKeys);
	};

	PlayerEntity.prototype.setRotationFromKeys = function (rotationFromKeys) {
		this.rotationFromKeys.copy(rotationFromKeys);
	};

	PlayerEntity.prototype.setTarget = function (target) {
		this.target.copy(target);
	}

	PlayerEntity.prototype.getCurrentWeapon = function () {
		if (this.isCarryingEntity()) {
			return undefined;
		}
		return this.weapon;
	}

	PlayerEntity.prototype.addAmmo = function (id, amount) {
		var weapon = this.weaponsById[id];
		var wasEmpty = (weapon.magazineAmmo + weapon.ammo) == 0;
		weapon.ammo += amount;
		var sound = "ammo_pickup";
		if (wasEmpty) {
			if (id == this.weapon.id) {
				// current weapon, reload
				this.startReloading();
			} else {
				// weapon not owned yet (== empty)
				weapon.magazineAmmo = Math.min(weapon.ammo, weapon.weaponData.magazineAmmo);
				weapon.ammo -= weapon.magazineAmmo;
			}
			sound = "weapon_pickup";
		}
		PRESKISO.AudioManager.playSound(this.object.position, sound, 25);
	}

	PlayerEntity.prototype.heal = function (amount) {
		var newHealth = Math.min(100, this.health + amount);
		this.setHealth(newHealth);
		PRESKISO.AudioManager.playSound(this.object.position, "health_pickup", 20);
	}

	PlayerEntity.prototype.equipWeapon = function (weaponType) {
		if (this.isCarryingEntity()) {
			return;
		}
		var currentWeapon = this.getCurrentWeapon();
		var newWeapon;
		if (currentWeapon === undefined) {
			newWeapon = this.weapons[weaponType][this.weapons[weaponType].length - 1];
		} else {
			var index;
			if (weaponType != currentWeapon.weaponData.type) {
				index = this.weapons[weaponType].length - 1;
			} else {
				index = PRESKISO.Util.arrayObjectIndexOf(this.weapons[weaponType], "id", currentWeapon.id) - 1;
				if (index < 0) {
					index = this.weapons[weaponType].length - 1;
				}
			}
			var startIndex = index;
			do {
				newWeapon = this.weapons[weaponType][index];
				index--;
				if (index < 0) {
					index = this.weapons[weaponType].length - 1;
				}
			} while (index != startIndex && (newWeapon.ammo + newWeapon.magazineAmmo) == 0);
		}
		if (currentWeapon === undefined || (newWeapon.id != currentWeapon.id && (newWeapon.ammo + newWeapon.magazineAmmo) > 0)) {
			this.cancelReloading();
			this.weapon = newWeapon;
			this.stopShooting();
		    this.updateWeaponModel();
		    if (this.weapon.magazineAmmo == 0) {
		    	this.startReloading();
		    } else {
		    	this.nextShot = PRESKISO.GameController.getCurrentTime() + 0.1;
		    }
		}
	}

	PlayerEntity.prototype.startMeleeAttacking = function () {
		if (this.isDead() || this.isMeleeAttacking()) {
			return;
		}
		this.cancelReloading();
		this.weaponObject.remove(this.aim);
		this.meleeStartTime = PRESKISO.GameController.getCurrentTime();
		PRESKISO.AudioManager.playSound(this.object.position, "melee_attack", 40);
		this.applyMeleeAttack();
	}

	PlayerEntity.prototype.applyMeleeAttack = function () {
		this.arm.rotation.x = this.aimXAngle;
		this.arm.updateMatrixWorld();

		var origin = new THREE.Vector3(0, 0, 0.2);
		var direction = new THREE.Vector3(0.4, 0.1, 1);
		if (this.isLeftHanded) {
			direction.x *= -1;
		}
		direction.add(origin);
		this.arm.localToWorld(origin);
		this.arm.localToWorld(direction);
		direction.transformDirection(this.arm.matrixWorld).normalize();
		var distance = 2;
		var weapon = this.getCurrentWeapon();
		if (weapon !== undefined) {
			distance = Math.max(distance, weapon.weaponData.aimOffset.z);
		}
		var raycaster = new THREE.Raycaster(origin, direction, 0, distance);
		var intersect = PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getHittableObjects(), true );
		if (intersect !== undefined) {
			var targetEntity = PRESKISO.GameController.getEntityFromObject(intersect.object);
			if (targetEntity != this) {
				var impactPosition = intersect.point.clone();
			    intersect.object.worldToLocal(impactPosition);
			    var impactVector = direction.clone().multiplyScalar(this.meleeImpact);
				PRESKISO.GameController.hitEntity(intersect.object, this, this.meleePower, intersect.point, impactPosition, impactVector);
				PRESKISO.AudioManager.playSound(this.object.position, "melee_hit", this.meleeImpact);
			}
		}	
	}

	PlayerEntity.prototype.finishMeleeAttacking = function () {
		if (!this.isMeleeAttacking()) {
			return;
		}
		this.weaponObject.add(this.aim);
		this.meleeStartTime = undefined;
		this.nextMeleeTime = PRESKISO.GameController.getCurrentTime();
	}

	PlayerEntity.prototype.cancelMeleeAttacking = function () {
		if (!this.isMeleeAttacking()) {
			return;
		}
		this.weaponObject.add(this.aim);
		this.meleeStartTime = undefined;
		this.nextMeleeTime = PRESKISO.GameController.getCurrentTime();
	}

	PlayerEntity.prototype.isMeleeAttacking = function () {
		return this.meleeStartTime !== undefined;
	}

	PlayerEntity.prototype.startReloading = function () {
		if (this.isDead() || this.isReloading() || this.isMeleeAttacking() || this.isCarryingEntity() || this.weapon.ammo == 0 || this.weapon.magazineAmmo == this.weapon.weaponData.magazineAmmo) {
			return;
		}
		this.reloadEndTime = PRESKISO.GameController.getCurrentTime() + this.weapon.weaponData.reloadDuration;
		this.weaponObject.remove(this.aim);
		this.reloadSound = PRESKISO.AudioManager.playSound(this.object.position, this.weapon.weaponData.reloadSound || "beretta_reload", 15);
	}

	PlayerEntity.prototype.finishReloading = function () {
		if (!this.isReloading()) {
			return;
		}
		this.arm.rotation.set(0, 0, 0);
		this.reloadEndTime = undefined;
		var reloadAmount = Math.min(this.weapon.weaponData.magazineAmmo - this.weapon.magazineAmmo, this.weapon.ammo);
		this.weapon.magazineAmmo = this.weapon.magazineAmmo + reloadAmount;
		this.weapon.ammo -= reloadAmount;
		this.nextShot = PRESKISO.GameController.getCurrentTime() + 0.1;
		this.weaponObject.add(this.aim);
	}

	PlayerEntity.prototype.cancelReloading = function () {
		if (!this.isReloading()) {
			return;
		}
		this.reloadEndTime = undefined;
		this.weaponObject.add(this.aim);
		PRESKISO.AudioManager.cancelSound(this.reloadSound);
	}

	PlayerEntity.prototype.isReloading = function () {
		return (this.reloadEndTime !== undefined);
	}

	PlayerEntity.prototype.isCarryingEntity = function () {
		return this.carriedEntity !== undefined;
	}

	PlayerEntity.prototype.getCarriedEntity = function () {
		return this.carriedEntity;
	}

	PlayerEntity.prototype.takeCarriableEntity = function (carriableEntity) {
		if (this.isCarryingEntity()) {
			return false;
		}
		if (carriableEntity instanceof PRESKISO.FlagEntity) {
			var flag = carriableEntity;
			if (flag.isInBase) {
				PRESKISO.AudioManager.playSound(this.object.position, "flag_taken", 15);
			}
		}
		this.carriedEntity = carriableEntity;
		this.updateWeaponModel();

		this.isWeaponBlocked = false;
		this.cancelMeleeAttacking();
		this.cancelReloading();
		this.stopShooting();
		return true;
	}

	PlayerEntity.prototype.dropCarriedEntity = function (underneath) {
		if (!this.isCarryingEntity()) {
			return;
		}
		this.cancelMeleeAttacking();

		var position = this.carriedEntity.getObject().position.clone();
		if (!underneath) {
			position.z = 3;
		}
		var throwVector = new THREE.Vector3(0, 0, 1);
		throwVector.transformDirection(this.arm.matrixWorld);
		this.arm.localToWorld(position);
		this.carriedEntity.drop(position);
		if (this.arm.rotation.x > Math.PI / 3) {
			var power = (this.arm.rotation.x - Math.PI / 3) * 300;
			vector.setLength(power);
			this.carriedEntity.getObject().applyCentralImpulse(throwVector);
		}
		this.carriedEntity = undefined;
		this.weaponObject = undefined;
		this.updateWeaponModel();
	}

	PlayerEntity.prototype.checkInteractiveObjectInFront = (function () {
		var raycaster = new THREE.Raycaster(new THREE.Vector3(), new THREE.Vector3(), 0, 1);
		var direction = raycaster.ray.direction;//new THREE.Vector3();
		var origin = raycaster.ray.origin;//new THREE.Vector3();
		return function () {
			var currentTime = PRESKISO.GameController.getCurrentTime();
			if (currentTime >= this.nextInteractiveObjectCheck) {
				this.interactiveObjectInRange = undefined;
				this.nextInteractiveObjectCheck = currentTime + 0.05;
			    this.arm.rotation.x = this.aimXAngle;
				this.arm.updateMatrixWorld();
				direction.set(0, 0, 1);
			    origin.copy(this.object.position);
			   	var distance = 5;
			    direction.transformDirection(this.aim.matrixWorld);
			    // raycaster.set(origin, direction);
			    raycaster.far = distance;
			    var intersect = PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getHittableObjects(), true );
			    if (intersect !== undefined) {
			    	var entity = PRESKISO.GameController.getEntityFromObject(intersect.object);
			    	if (entity.isInteractive) {
			    		this.interactiveObjectInRange = entity;
			    	}
			    }
			}
		};
	})();

	PlayerEntity.prototype.checkWeaponBlocked = function () {
		if (this.isDead() || this.isReloading() || this.isMeleeAttacking() || this.isCarryingEntity()) {
			return;
		}
		var currentTime = PRESKISO.GameController.getCurrentTime();
		if (currentTime >= this.nextWeaponBlockedCheck) {
			this.interactiveObjectInWeaponRange = undefined;
			this.isWeaponBlocked = false;
			var weapon = this.getCurrentWeapon();
			this.nextWeaponBlockedCheck = currentTime + 0.05;
			// var wasWeaponBlocked = this.isWeaponBlocked;
		    this.isWeaponBlocked = false;
		    this.arm.rotation.x = this.aimXAngle;
			this.arm.updateMatrixWorld();
			var direction = this.aim.position.clone();
		    var origin = new THREE.Vector3(direction.x,
		    	direction.y,
		    	direction.z - this.weaponObject.position.z - weapon.weaponData.aimOffset.z - this.arm.position.z + 0.5
		    );
		   	var distance = direction.distanceTo(origin);

		    this.arm.localToWorld(origin);
		   	this.arm.localToWorld(direction);
		   	direction.sub(origin);
		   	direction.normalize();
		   	distance += 0.8;
		    var raycaster = new THREE.Raycaster(origin, direction, 0, Math.max(distance, 5));
		    var intersect = PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getHittableObjects(), true, [this.hittableCapsule.id] );
		    if (intersect !== undefined) {
		    	this.firstIntersectInFrontOfWeapon = intersect;
		    	var entity = PRESKISO.GameController.getEntityFromObject(intersect.object);
		    	if (entity.isInteractive) {
		    		this.interactiveObjectInWeaponRange = entity;
		    	}
			    if (intersect.distance < distance) {
			    	this.isWeaponBlocked = true;
			    }
		    }
		}
	}

	PlayerEntity.prototype.updateAimLaser = function () {
		var shouldAppear = (!this.isReloading() && this.isMeleeAttacking && !this.isCarryingEntity() && !this.isWeaponBlocked);
		if (shouldAppear) {
			var weapon = this.getCurrentWeapon();
			if (this.aimLaser.parent != this.aim) {
				this.aim.add(this.aimLaser);
			}
			var currentTime = PRESKISO.GameController.getCurrentTime();
			if (currentTime >= this.nextRayCheck) {
				this.nextRayCheck = currentTime + 0.03;
				var origin = new THREE.Vector3(0, 0, 0);
			    var direction = THREE.Z_AXIS.clone().transformDirection(this.aim.matrixWorld);
			    this.aim.localToWorld(origin);
			   	var distance = weapon.weaponData.range;
			    var raycaster = new THREE.Raycaster(origin, direction, 0, distance);
			    var intersect = PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getHittableObjects(), true );
			    if (intersect !== undefined) {
			    	if (!this.aimDot.visible) {
				    	this.aimLaser.material.opacity = .5;
				    	// this.aimDot.visible = true;
				    }
			    	distance = intersect.distance;
			    	this.aimDot.position.z = distance;
			    } else {
			    	if (this.aimDot.visible) {
				    	this.aimLaser.material.opacity = .2;
				    	this.aimDot.visible = false;
				    }
			    }
				this.aimLaser.geometry.vertices[1].z = distance;
				this.aimLaser.geometry.verticesNeedUpdate = true;
			}

		} else if (this.aimLaser.parent == this.aim) {
			this.aim.remove(this.aimLaser);
		}
	}

	PlayerEntity.prototype.updateWeaponModel = function () {
		if (this.weaponObject !== undefined) {
			this.arm.remove(this.weaponObject);
		}
		if (this.isCarryingEntity()) {
			this.weaponObject = this.carriedEntity.getObject();
			if (this.carriedEntity instanceof PRESKISO.FlagEntity) {
				if (this.isLeftHanded) {
					this.weaponObject.rotation.set(0, Math.PI / 2.5, 0);
				} else {
					this.weaponObject.rotation.set(0, - Math.PI - Math.PI / 2.5, 0);
				}
				this.weaponObject.position.set(0, 1, 0.7);
			} if (this.carriedEntity instanceof PRESKISO.PigEntity) {
				this.weaponObject.position.copy(this.arm.position).multiplyScalar(-1);
				this.weaponObject.position.z += 1.5;
				this.weaponObject.rotation.set(0, -Math.PI / 2, 0);
			} else {
				this.weaponObject.position.set(0, 0, 0);
				this.weaponObject.rotation.set(0, 0, 0);
			}
		} else {
			var weapon = this.getCurrentWeapon();
			var resource = PRESKISO.Loader.getResource(weapon.weaponData.model);
	    	// var mat = new THREE.MeshFaceMaterial(resource.materials);
	    	// var mat = resource.materials[0].clone();
	    	var mat = new THREE.MeshLambertMaterial({color: 0x333333, side: THREE.DoubleSide});
	    	this.weaponObject = new THREE.Mesh(
	        	resource.geometry,
		        mat
		    );
	    	this.weaponObject.receiveShadow = this.weaponObject.castShadow = true;
		    this.aim.position.copy(weapon.weaponData.aimOffset);
		    this.weaponObject.add(this.aim);
			this.weaponObject.position.set(0, 0, 0.7);
		}
		// window.console.log(this.weaponObject);
	    this.arm.add(this.weaponObject);
	}

	PlayerEntity.prototype.die = function () {
		this.parent.die.call(this);
		this.dropCarriedEntity(true);
		this.deathTime = PRESKISO.GameController.getCurrentTime();
		for (var i = 0; i < this.materials.length; i++) {
			this.materials[i].transparent = true;
		}
		for (var i = 0; i < this.weapons.length; i++) {
			for (var j = 0; j < this.weapons[i].length; j++) {
				var weapon = this.weapons[i][j];
				var ammo = Math.ceil((weapon.ammo + weapon.magazineAmmo) / 2);
				if (ammo > weapon.weaponData.magazineAmmo) {
					var position = new THREE.Vector3(0, 0, 0.5 + Math.random()).applyAxisAngle(THREE.Y_AXIS, Math.random() * Math.PI * 2).setY(0).add(this.object.position);
					var ammo = new PRESKISO.AmmoPowerupEntity(position, {permanent: false, weaponId: weapon.id, amount: ammo, lifespan: 30});
					PRESKISO.GameController.addEntity(ammo);
				}
				weapon.magazineAmmo = 0;
				weapon.ammo = 0;
			}
		}
		this.weaponObject.remove(this.aim);
		this.object.setAngularFactor(new THREE.Vector3(1, 1, 1));
		this.impactFactor = 5;
		this.deaths++;
		PRESKISO.AudioManager.playSound(this.object.position, "wilhelm", 20);
	}

	PlayerEntity.prototype.toggleMovementMode = function () {
		this.movementRelativeToRotation = !this.movementRelativeToRotation;
	}

	PlayerEntity.prototype.toggleRotationMode = function () {
		this.rotationDuplicateJoystick = !this.rotationDuplicateJoystick;
	}

	PlayerEntity.prototype.tick = function (delta, time) {
		if (this.health <= 0) {
			var timeSinceDeath = time - this.deathTime;
			if (timeSinceDeath > 3) {
				PRESKISO.GameController.playerDiedAndDisappeared(this);
				// PRESKISO.GameController.respawnPlayer(this.playerId);
			} else if (timeSinceDeath > 2) {
				var alpha = 3 - timeSinceDeath;
				for (var i = 0; i < this.materials.length; i++) {
					this.materials[i].opacity = alpha;
				}
			}
			return;
		}
		// var pos = PRESKISO.AI.PathFinder.AStar.get3dToMapPosition(this.object.position, true);
		// window.console.log(this.object.position, pos);

		if (this.isReloading()) {
			if (this.reloadEndTime <= time) {
				this.finishReloading();
			}
		} else if (this.isShooting) {
			this.shoot();
		} else if (this.isMeleeAttacking) {
			var timeElapsed = time - this.meleeStartTime;
			if (timeElapsed > this.meleeDuration) {
				this.finishMeleeAttacking();
			}
		}
		var weapon = this.getCurrentWeapon();

		this.cameraMotionTarget.position.copy(this.object.getLinearVelocity()).multiplyScalar(0.5).add(this.object.position);

		// MOVEMENT
	    if (this.movementFromKeys.lengthSq() > 0 && this.isStanding()) {
	    	var dir = this.movementFromKeys.clone();
	        var velocity = this.object.getLinearVelocity();
	        var maxSpeed = 100;
	        var speed = velocity.x * velocity.x + velocity.z * velocity.z;
	        if (speed < maxSpeed) {

					dir.transformDirection(cameraFlat.matrixWorld).setY(0).multiplyScalar(-1).setLength(this.movementFromKeys.length());

	            if (this.isRushing) {
	            	dir.multiplyScalar(15);
	            	this.isRushing = false;
	            	this.lastRush = clock.getElapsedTime();
	            }
	            dir.multiplyScalar(1000);
	            this.object.applyCentralForce(dir);
	        }
	    }

	    // ROTATION
		this.object.__dirtyRotation = true;
	    var rot = this.rotationFromKeys;
	    this.aimXAngle = Math.pow(rot.x, 5) * Math.PI / 3;
		// this.arm.rotation.x = this.aimXAngle;
		// this.arm.updateMatrixWorld();
		if (this.rotationDuplicateJoystick) {
			if (this.target.lengthSq() > 0.7) {
				// var actualTarget = this.target.clone();
				// camera.localToWorld(actualTarget);
				// actualTarget.sub(camera.position).setY(0).multiplyScalar(-1).normalize().add(this.object.position);

				var target = this.target.clone();
				target.transformDirection(cameraFlat.matrixWorld).setY(0).multiplyScalar(-1).setLength(this.target.length());
				var actualTarget = new THREE.Vector3().addVectors(target, this.object.position);
				
				// var actualTarget = new THREE.Vector3().addVectors(this.target, this.object.position);
				actualTarget.y = this.object.position.y;
				this.object.lookAt(actualTarget);
				// this.object.__dirtyRotation = true;
			}
		} else {
		    if (rot.y != 0) {
		    	this.object.rotateOnAxis(THREE.Y_AXIS, Math.pow(rot.y, 5) * delta * 5 * ((!this.isSniping) ? 1 : 0.2) );
		    }
		}
		// this.arm.rotation.x = Math.pow(rot.x, 5) * Math.PI / 3;

		// if (this.nextRayCheck <= time) {
		// 	// this.firstIntersectInFront = undefined;
		// 	// var interactiveObjectInRange = undefined;
		// 	// var origin = new THREE.Vector3(0, 0, 0);
		//  //    this.object.localToWorld(origin);
		//  //    var direction = THREE.Z_AXIS.clone();
		//  //   	this.object.localToWorld(direction);
		//  //   	direction.sub(origin);
		//  //    var raycaster = new THREE.Raycaster(origin, direction, 0, 5);
		//  //    var intersect = PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getHittableObjects(), true );
		//  //    if (intersect !== undefined) {
		//  //    	this.firstIntersectInFront = intersect;
		//  //    	var entity = PRESKISO.GameController.getEntityFromObject(intersect.object);
		//  //    	if (entity.isInteractive) {
		//  //    		interactiveObjectInRange = entity;
		//  //    	}
		//  //    }

		// 	// this.nextRayCheck = time + 0.03;
		// 	// if (!this.isReloading() && !this.isMeleeAttacking && !this.isCarryingFlag) {
				


		// 	//     if (!this.isMeleeAttacking && !this.isWeaponBlocked) {
		// 	//     	if (wasWeaponBlocked) {
		// 	//     		this.weaponObject.add(this.aim);
		// 	//     	}
		// 	// 		// UPDATE AIM LASER
		// 	// 	} else if (!wasWeaponBlocked) {
		// 	// 		this.weaponObject.remove(this.aim);
		// 	// 	}
		// 	// }
		// 	// this.interactiveObjectInRange = interactiveObjectInRange;
		// }

		this.checkInteractiveObjectInFront();
		this.checkWeaponBlocked();
		this.updateAimLaser();

		if (this.isReloading()) {
			this.arm.rotation.x = -Math.PI / 2;
		} else if (this.isMeleeAttacking()) {
			var frame = (time - this.meleeStartTime) / this.meleeDuration;
			this.arm.rotation.x = -frame * 0.4 * Math.PI / 2;
		} else if (this.isWeaponBlocked) {
	    	this.arm.rotation.x = Math.PI / 2.2;
		} else {
			this.arm.rotation.x = this.aimXAngle;
			if (!this.isCarryingEntity() && weapon.weaponData.recoil !== undefined) {
		    	var coeff = Math.min(1, 1 - delta * 6);
		    	var obj = this.arm;
		    	obj.rotation.x *= coeff;
		    	obj.rotation.y *= coeff;
		    	obj.rotation.z *= coeff;
		    }
		}
	    if (this.isInteracting) {
	    	this.interact();
	    }
	};

	PlayerEntity.prototype.jump = function () {
		if (!this.isStanding()) {
			return;
		}
		this.object.applyCentralImpulse(new THREE.Vector3(0, 20 * this.object.mass, 0));
	}

	PlayerEntity.prototype.isStanding = function () {
		// return true;
		if (Math.abs(this.object.getLinearVelocity().y) > 2) {
			return false;
		}
		var origin = this.object.position.clone();
		origin.y -= 2.2;
		var raycaster = new THREE.Raycaster(origin, new THREE.Vector3(0, -1, 0), 0, 0.6);
		var intersect = PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getHittableObjects(), true );
		if (intersect !== undefined && intersect.object != this.hittableCapsule) {
			return true;
		}
		return false;
	};

	PlayerEntity.prototype.rush = function () {
		if (clock.getElapsedTime() - this.lastRush > 1) {
			this.isRushing = true;
		}
	}

	PlayerEntity.prototype.interact = function () {
		var object = this.interactiveObjectInWeaponRange || this.interactiveObjectInRange;
		if (object !== undefined) {
			object.interact(this);
		}
		this.isInteracting = false;
	}

	PlayerEntity.prototype.prepareGrenade = function () {
		this.prepareGrenadeTime = PRESKISO.GameController.getCurrentTime();
	}

	PlayerEntity.prototype.throwGrenade = function () {
		if (this.health <= 0) {
			return;
		}
		var prepareDuration = Math.min(1, PRESKISO.GameController.getCurrentTime() - this.prepareGrenadeTime);
		var origin = new THREE.Vector3(0.8, 0.5, 1.5);
		if (!this.isLeftHanded) {
			origin.x *= -1;
		}
		origin.applyAxisAngle(THREE.X_AXIS, this.aimXAngle);
		var direction = new THREE.Vector3(0, 0.1, 1);
		direction.applyAxisAngle(THREE.X_AXIS, this.aimXAngle);
	    this.object.localToWorld(origin);
	   	direction.transformDirection(this.object.matrixWorld).normalize().multiplyScalar(100 * prepareDuration);
	   	// origin.y += 0.3;
	   	// origin.z += 0.5;
	   	direction.add(this.object.getLinearVelocity());
	   	var grenade = new PRESKISO.GrenadeEntity(this, origin, direction, 100);
	   	PRESKISO.GameController.addEntity(grenade);
		PRESKISO.AudioManager.playSound(this.object.position, "grenade_throw", 13 * (1 + prepareDuration), {randomizePlaybackRateBy: 0.1});
	}

	PlayerEntity.prototype.startShooting = function () {
		if (this.isDead() || this.isReloading() || this.isMeleeAttacking() || this.isWeaponBlocked()) {
			return;
		}
		this.isShooting = true;
	}

	PlayerEntity.prototype.stopShooting = function () {
		if (this.isShooting === false) {
			return;
		}
		this.isShooting = false;
	}

	PlayerEntity.prototype.shoot = function () {
		if (this.isReloading() || this.isMeleeAttacking() || this.isWeaponBlocked) {
			// cannot shoot in that state
			return false;
		}
		if (this.isCarryingEntity()) {
			// won't shoot but will drop the item carried
			this.dropCarriedEntity();
	    	this.stopShooting();
			return false;
		}
		if (this.nextShot > PRESKISO.GameController.getCurrentTime()) {
			// not ready yet
			return false;
		}

		var weapon = this.getCurrentWeapon();
	    var weaponData = weapon.weaponData;

		if (weapon.magazineAmmo <= 0) {
			this.stopShooting();
			PRESKISO.AudioManager.playSound(this.object.position, "empty_weapon", 25, {randomizePlaybackRateBy: 0.1});
			return false;
		}

		weapon.magazineAmmo--;
		bulletCount = weaponData.bullets || 1;

		if (!weaponData.auto) {
			this.stopShooting();
		}
	    this.nextShot = PRESKISO.GameController.getCurrentTime() + weapon.weaponData.rateOfFire;

		var origin = new THREE.Vector3(0, 0, 0);
	    this.aim.localToWorld(origin);
	    var direction = THREE.Z_AXIS.clone();
	   	// this.aim.localToWorld(direction);
	   	// direction.sub(origin);
	   	// direction.normalize();
		direction.transformDirection(this.aim.matrixWorld);

	   	var yRotation = Math.random() * 0.5;
	   	this.arm.rotateOnAxis(new THREE.Vector3(-yRotation, (1 - yRotation) * (Math.random()>0.5?1:-1), 0), (1 + Math.random()) * weaponData.recoil / 4);


	   	var bulletColor = this.color.clone();
	   	bulletColor.lerp(new THREE.Color(1, 1, 1), .8);
	   	var offset = 1 - weaponData.accuracy;
	   	for (var i = 0; i < bulletCount; i++) {
	   		var bulletDirection = direction.clone();
		    bulletDirection.add({x: (Math.random() - .5) * offset, y: (Math.random() - .5) * offset, z: (Math.random() - .5) * offset});
			var bullet = new PRESKISO.BulletEntity(this, origin, bulletDirection, weaponData.power / bulletCount, weaponData.speed, weaponData.range, weaponData.isExplosive, bulletColor, weaponData.ammoModel, weaponData.impactSize, weaponData.isPiercing);
			PRESKISO.GameController.addEntity(bullet);
		}
		var worldPosition = new THREE.Vector3().setFromMatrixPosition( this.aim.matrixWorld );
		PRESKISO.AudioManager.playSound(worldPosition, weaponData.sound, 10, {randomizePlaybackRateBy: 0.1});

		if (weapon.magazineAmmo == 0) {
			this.startReloading();
		}
		return true;
	}

	PlayerEntity.prototype.getHit = function (power, objectPosition, sourceEntity) {
		this.parent.getHit.call(this, power, objectPosition, sourceEntity);
		if (objectPosition.y > 1.3) {
			power *= 2;
			if (power > this.health) {
	        	// audioManager.playSound(null, "headshot", 5);
			}
		}
		var health = Math.max(0, this.health -power);
		this.setHealth(health);
		return this.health;
	}

	PlayerEntity.prototype.setHealth = function (health) {
		if (health <= 0 && this.health <= 0) {
			return;
		}
		this.health = health;
		if (this.health <= 0) {
			this.die();
		}
	}

	PlayerEntity.prototype.createObject = function () {
	    var cylinderHeight = 2.60, radius = 1.06;
	    var hittableCapsuleGeometry = new THREE.CylinderGeometry(radius, radius, cylinderHeight, 8);
	    for (var i = -1; i <= 1; i+= 2) {
			var geometry = new THREE.SphereGeometry(radius, 8, 4);
			geometry.applyMatrix( new THREE.Matrix4().makeTranslation(0, cylinderHeight / 2 * i, 0) );
			hittableCapsuleGeometry.merge(geometry);
			// THREE.GeometryUtils.merge(hittableCapsuleGeometry, geometry);
		}
		this.hittableCapsule = new THREE.Mesh(
			hittableCapsuleGeometry,
			new THREE.MeshBasicMaterial({color: new THREE.Color(1, 0, 1)})
			);
		this.hittableCapsule.visible = false;
		this.hittableCapsule.isHittable = true;


		this.materials = [];
		var armSideFactor = (this.isLeftHanded) ? 1 : -1;
	    var mass = 10;
	    var resourceName = "pill-papy"//(["pill-classic", "pill-cool", "pill-girl"])[Math.floor(Math.random() * 3)];
	    var resource = PRESKISO.Loader.getResource(resourceName);
	    var materials = [];
	    for (var i = 0; i < resource.materials.length; i++) {
	    	var material = resource.materials[i].clone();
	    	materials.push(material);
	    	this.materials.push(material);
	    }
	    var mat = new THREE.MeshFaceMaterial(materials);
	    this.object = new Physijs.CapsuleMesh(
	        hittableCapsuleGeometry,
	        Physijs.createMaterial(
	            mat,
	            1, // high friction
	            0.1 // low restitution
	        ),
	        10
	    );
	    this.object.material.materials[this.object.material.materials.length - 1].color.copy(this.color);
	    this.object.geometry = resource.geometry;
	    this.object.add(this.hittableCapsule);
	    // this.object.isHittable = true;
	    this.object.receiveShadow = this.object.castShadow = true;

		this.arm = new THREE.Object3D();
	    this.arm.position.set(.7 * armSideFactor, .2, 0);
	    this.object.add(this.arm);

		this.aim = new THREE.Object3D();
	    // this.aim.position.set(1, .2, .2);
	    // this.arm.add(this.aim);

	    var material = new THREE.LineBasicMaterial({
	        color: this.color,
	        transparent: true,
	        opacity: 0.1
	    });

	    var geometry = new THREE.Geometry();
	    geometry.vertices.push(
	        new THREE.Vector3( 0, 0, 0 ),
	        new THREE.Vector3( 0, 0, 10 )
	    );
	    this.aimLaser = new THREE.Line( geometry, material );
	    this.aimLaser.transparent = true;
	    this.aim.add(this.aimLaser);

	    this.aimDot = new THREE.Mesh(
	        new THREE.SphereGeometry(.1),
	        new THREE.MeshBasicMaterial({ color: this.color })
	    );
	    this.aim.add(this.aimDot);

	    // this.healthBar = new THREE.Mesh(
	    // 	new THREE. PlaneGeometry(2, .3, 1, 1),
	    // 	new THREE.MeshBasicMaterial({color: 0x00FF00})
	    // );
	    // scene.add(this.healthBar);

	    var shirt_material = new THREE.MeshLambertMaterial({color: new THREE.Color(1, 1, 1)});
	    var arm = new THREE.Mesh(
	    	new THREE.CylinderGeometry(.2, .25, .5, 8),
	    	shirt_material
	    );
	    this.materials.push(shirt_material);
		arm.rotation.x = Math.PI / 2;
		arm.position.z = 0.35;
		arm.position.x = 0;
		arm.rotateOnAxis(THREE.Z_AXIS, armSideFactor * Math.PI / 24);
		// arm.rotateOnAxis(THREE.Y_AXIS, -Math.PI / 5);
	    arm.castShadow = arm.receiveShadow = true;
	    // arm.isHittable = true;
		this.arm.add(arm);

		var hand_material = new THREE.MeshLambertMaterial({color: this.color});
		this.materials.push(hand_material);
		var hand = new THREE.Mesh(
	    	new THREE.SphereGeometry(.22, 8, 4),
	    	hand_material
	    );
	    hand.position.y = 0.35;
	    hand.castShadow = hand.receiveShadow = true;
	    arm.add(hand);

	    var shoulder = new THREE.Mesh(
	    	new THREE.SphereGeometry(.27, 8, 4),
	    	shirt_material
	    );
	    shoulder.position.y = -0.35;
	    shoulder.castShadow = shoulder.receiveShadow = true;
	    arm.add(shoulder);

	    // this.equipWeapon(0, 0);

	 //    var camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 0.1, 1000 );
	 //    camera.rotateOnAxis(THREE.Y_AXIS, Math.PI);
		// camera.position.z = -0.2;
		// camera.position.x = -0.7;
		// camera.position.y = 1;
		// // camera.rotateOnAxis(THREE.Z_AXIS, Math.PI / 12);
	 //    this.arm.add(camera);

	 //    this.camera = camera;
		if (this.playerId == 0) {
			// 	this.arm.add(flashLight);
			// 	this.arm.add(flashLightTarget);
		}

	    this.cameraAimTarget = new THREE.Object3D();
	    this.cameraAimTarget.position.set(0, 0, 15);
	    this.object.add(this.cameraAimTarget);

	    this.cameraMotionTarget = new THREE.Object3D();
	}

	PlayerEntity.prototype.objectAdded = function () {
		this.object.setAngularFactor(new THREE.Vector3(0, 0, 0));
		this.impactFactor = 1;

		this.setHealth(100);
		for (var i = 0; i < this.materials.length; i++) {
			this.materials[i].opacity = 1;
			this.materials[i].transparent = false;
		}
		this.object.position.copy(this.team.getSpawningPoint());
		this.object.position.y = 2.5;
		this.object.rotation.set(0, y = Math.random() * Math.PI * 2, 0);
		this.object.__dirtyPosition = true;
		this.object.__dirtyRotation = true;

		scene.add(this.cameraMotionTarget);
	    PRESKISO.GameController.getCameraController().addTrackedObject(this.cameraAimTarget);
	    PRESKISO.GameController.getCameraController().addTrackedObject(this.cameraMotionTarget);

		for (var i = 0; i < this.weapons.length; i++) {
			for (var j = 0; j < this.weapons[i].length; j++) {
				var weapon = this.weapons[i][j];
				var weaponData = this.weapons[i][j].weaponData;
				weapon.magazineAmmo = Math.min(weaponData.initialAmmo, weaponData.magazineAmmo);
				weapon.ammo = weaponData.initialAmmo - weapon.magazineAmmo;
			}
		}
		this.equipWeapon(0);
	}

	PlayerEntity.prototype.objectRemoved = function () {
	    PRESKISO.GameController.getCameraController().removeTrackedObject(this.cameraAimTarget);
		scene.add(this.cameraMotionTarget);
	    PRESKISO.GameController.getCameraController().removeTrackedObject(this.cameraMotionTarget);
	}

	return PlayerEntity;
})();
