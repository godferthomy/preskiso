PRESKISO.BarrelEntity = ( function () {
	var texture = THREE.ImageUtils.loadTexture( "images/door_metal.jpg" );
	var material = Physijs.createMaterial(
        new THREE.MeshLambertMaterial({ color: 0x996666, map: texture }),
        0.5, // high friction
        .2 // low restitution
    );
	var blown_material = Physijs.createMaterial(
        new THREE.MeshLambertMaterial({ color: 0x331111, map: texture }),
        0.8, // high friction
        .1 // low restitution
    );
    material.impactSound = "impacts/metal_impact";

	function BarrelEntity(radius, height, position) {
		PRESKISO.CarriableEntity.call(this);

		this.isHittable = true;


		this.position = position.clone();

		this.radius = radius;
		this.height = height;

		this.lastHitPosition = new THREE.Vector3();

		this.health = 150;
	}
	PRESKISO.Util.inherit(BarrelEntity, PRESKISO.CarriableEntity);

	BarrelEntity.prototype.setHealth = function (health) {
		this.health = health;
		if (this.health <= 0) {
			this.die();
		}
	}

	BarrelEntity.prototype.die = function () {
		if (this.carrier !== undefined) {
			this.carrier.dropCarriedEntity(this);
		}
		this.impactFactor = 3;
		// this.isInteractive = false;
		// var pos = new THREE.Vector3().setFromMatrixPosition( this.object.matrixWorld )
		this.object.material = blown_material;
		var pos = this.lastHitPosition.clone();
		this.object.localToWorld(pos);
		var explosion = new PRESKISO.ExplosionEntity(this.lastHitEntity, pos, 10, 50);
        PRESKISO.GameController.addEntity(explosion);
        // PRESKISO.GameController.removeEntity(this);
		PRESKISO.AudioManager.playSound(pos, "grenade_explosion", 20, {randomizePlaybackRateBy: 0.1});
	}

	BarrelEntity.prototype.getHit = function (power, objectPosition, sourceEntity) {
		BarrelEntity.prototype.parent.getHit.call(this, power, objectPosition, sourceEntity);
		this.lastHitPosition.copy(objectPosition);
		if (this.health > 0) {
			this.setHealth(this.health - power);
		}
	}

	BarrelEntity.prototype.setCarrier = function (carrier) {
		BarrelEntity.prototype.parent.setCarrier.call(this, carrier);
		if (carrier === undefined) {
			this.object.setDamping(0.2, 0.9);
		}
	}

	BarrelEntity.prototype.createObject = function () {
		this.object = new Physijs.CylinderMesh(
			new THREE.CylinderGeometry(this.radius, this.radius, this.height, 12),
			material,
			Math.PI * Math.pow(this.radius, 2) * this.height * 2
		);
		this.object.isHittable = true;
		this.object.castShadow = this.object.receiveShadow = true;
		this.object.position.copy(this.position);

		PRESKISO.Util.setCollisionGroup(this.object, "MOBILE");
	}

	BarrelEntity.prototype.objectAdded = function () {
		this.object.setDamping(0.5, 0.8);
	}

	return BarrelEntity;
})();
