PRESKISO.GroundEntity = ( function () {
	var texture = THREE.ImageUtils.loadTexture( "images/grass.jpg" );
	// var texture = THREE.ImageUtils.loadTexture( "images/parking.jpg" );
	var ground_material = Physijs.createMaterial(
		new THREE.MeshPhongMaterial({ map: texture }),
		.5,
		1
	);
    ground_material.map.wrapS = ground_material.map.wrapT = THREE.RepeatWrapping;
    ground_material.map.repeat.set( 150, 150 );

	ground_material.bumpMap = THREE.ImageUtils.loadTexture( "images/grass-bump.jpg" );
	ground_material.bumpMap.wrapS = ground_material.bumpMap.wrapT = THREE.RepeatWrapping;
    ground_material.bumpMap.repeat.set( 150, 150 );
	ground_material.bumpScale = 0.05;
    ground_material.impactSound = "impacts/concrete_impact";

	function GroundEntity() {
		PRESKISO.Entity.call(this);

		this.hasBody = true;

		// this.isDimmable = true;
		this.isHittable = true;
	}

	PRESKISO.Util.inherit(GroundEntity, PRESKISO.Entity);

	// PRESKISO.Entity.prototype.clear = function () {
	// 	this.scene = undefined;
	// }

	GroundEntity.prototype.tick = function (delta) {
	}

	GroundEntity.prototype.createObject = function () {
		ground = new Physijs.BoxMesh(
	        new THREE.BoxGeometry(500, 5, 500),
	        ground_material,
	        0
	    );
	    ground.position.y = -2.52;
	    ground.receiveShadow = true;

	    ground.receiveShadow = true;
	    ground.isHittable = true;


	    this.object = ground;

	    PRESKISO.Util.setCollisionGroup(this.object, "STATIC");
	}

	GroundEntity.prototype.createBody = function () {
		var groundShape = new CANNON.Plane();
        var groundBody = new CANNON.Body({ mass: 0 });
        groundBody.addShape(groundShape);
        groundBody.quaternion.setFromAxisAngle(new CANNON.Vec3(1,0,0),-Math.PI/2);
        groundBody.position.set(0,0,0);

        this.body = groundBody;
	}

	GroundEntity.prototype.objectAdded = function (scene) {
	}

	return GroundEntity;
})();
