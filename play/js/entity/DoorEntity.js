PRESKISO.DoorEntity = ( function () {

	function DoorEntity(dimensions, position) {
		PRESKISO.InteractiveEntity.call(this);

		this.movingDuration = 0.5;
		this.healingRate = 30;
		this.openingSound = "door_start_opening";
		this.closedSound = "door_finish_closing";

		this.isDimmable = true;
		this.isHittable = true;


		this.position = position.clone();

		this.dimensions = dimensions;

		this.open = false;
		this.isMoving = false;
		this.movingTime = 1;
	}

	PRESKISO.Util.inherit(DoorEntity, PRESKISO.InteractiveEntity);

	DoorEntity.prototype.tick = function (delta, time) {
		DoorEntity.prototype.parent.tick.call(this, delta, time);
		if (this.health > 0 && this.healingRate > 0) {
			this.setHealth(Math.min(this.health + delta * this.healingRate, this.maxHealth));
		}
		if (this.isMoving) {
			this.movingTime += delta / this.movingDuration;
			if (this.movingTime > 1) {
				this.movingTime = 1;
				PRESKISO.GameController.updateMapValue(this.position, (this.open) ? PRESKISO.WallGenerator.Types.DOOR_OPEN : PRESKISO.WallGenerator.Types.DOOR_CLOSED);
				this.isMoving = false;
				if (!this.open && this.closedSound != undefined) {
					PRESKISO.AudioManager.playSound(this.object.position, this.closedSound, 10, {randomizePlaybackRateBy: 0.15});
				}
			}
			this.updateFromMovingState();
		}
	}

	DoorEntity.prototype.updateFromMovingState = function () {
		window.console.err("DoorEntity.updateFromMovingState(): Not implemented");
	}

	DoorEntity.prototype.objectAdded = function (scene) {
		PRESKISO.GameController.updateMapValue(this.position, PRESKISO.WallGenerator.Types.DOOR_CLOSED);
		this.object.setAngularFactor(new THREE.Vector3( 0, 0, 0 ));
		this.object.setLinearFactor(new THREE.Vector3( 0, 0, 0 ));
	}

	DoorEntity.prototype.interact = function (sourceEntity) {
		this.isMoving = true;
		this.movingTime = 1 - this.movingTime;
		this.open = !this.open;
		if (this.open && this.openingSound != undefined) {
			PRESKISO.AudioManager.playSound(this.object.position, this.openingSound, 10, {randomizePlaybackRateBy: 0.1});
		}
	}

	DoorEntity.prototype.setHealth = function (health) {
		if (this.health == undefined) {
			return;
		}
		this.health = health;
		if (this.health <= 0) {
			this.die();
		}
	}

	DoorEntity.prototype.die = function () {
		PRESKISO.GameController.updateMapValue(this.position, PRESKISO.WallGenerator.Types.DOOR_OPEN);
		this.isMoving = false;
		this.isInteractive = false;
		this.object.setAngularFactor(new THREE.Vector3( 1, 1, 1 ));
		this.object.setLinearFactor(new THREE.Vector3( 1, 1, 1 ));
	}

	DoorEntity.prototype.getHit = function (power, objectPosition) {
		if (this.health <= 0) {
			return;
		}
		this.setHealth(this.health - power);
		
	}

	return DoorEntity;
})();
