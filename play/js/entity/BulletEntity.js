PRESKISO.BulletEntity = ( function () {
    var texture = THREE.ImageUtils.loadTexture( "images/impact_decal.png" );
    var decal_geometry = new THREE.PlaneGeometry(1, 1, 1, 1);
    var decal_material = new THREE.MeshLambertMaterial({map: texture, transparent: true, blending: THREE.NormalBlending})
    function BulletEntity(player, origin, direction, parameters) {
        PRESKISO.Entity.call(this);
        parameters = parameters || {};

        if (parameters.weaponData) {
            var p = parameters, wD = parameters.weaponData;
            p.power = p.power || wD.power / (wD.bullets || 1);
            p.speed = p.speed || wD.speed;
            p.range = p.range || wD.range;
            p.isExplosive = p.isExplosive || wD.isExplosive;
            p.ammoModel = p.ammoModel || wD.ammoModel;
            p.impactSize = p.impactSize || wD.impactSize;
            p.piercingFactor = p.piercingFactor || wD.piercingFactor;
            p.shotLength = p.shotLength || wD.shotLength;
        }

        this.player = player;

        this.origin = origin;
        this.direction = direction;
        this.power = parameters.power;
        this.speed = parameters.speed;
        this.range = parameters.range;
        this.color = parameters.color || player.color.clone().lerp(new THREE.Color(1, 1, 1), .8);
        this.shotLength = parameters.shotLength || this.speed / 80;
        this.isExplosive = !!parameters.isExplosive;
        this.ammoModel = parameters.ammoModel;
        this.impactSize = parameters.impactSize || 1;
        this.piercingFactor = !!parameters.piercingFactor;

        this.distanceLeft = this.range;//BulletEntity.MAX_RANGE;
        this.distanceDone = 0;
        this.raycaster = new THREE.Raycaster(origin, direction, 0, 0.1);
    }
    PRESKISO.Util.inherit(BulletEntity, PRESKISO.Entity);


    BulletEntity.MAX_RANGE = 150;


    BulletEntity.prototype.tick = function (delta, time) {
        if (this.distanceLeft <= 0) {
            PRESKISO.GameController.removeEntity(this);
        }
        var distance = Math.min(delta * this.speed, this.distanceLeft);
        this.raycaster.far = distance;
        // window.console.log(distance, PRESKISO.GameController.getHittableObjects());
        var intersect = PRESKISO.Util.intersectFirstHittableObject(this.raycaster, PRESKISO.GameController.getHittableObjects(), true );
        if (intersect !== undefined) {
            PRESKISO.GameController.removeEntity(this);
            var targetEntity = PRESKISO.GameController.getEntityFromObject(intersect.object);
            if (!this.isExplosive) {
                
                var faceIndex = intersect.faceIndex;
                var normal = intersect.object.geometry.faces[faceIndex].normal;

                this.distanceDone += intersect.distance;
                var impactDamage = this.power * (.5 + .5 * (1 - (this.distanceDone / this.range)));

                var impactVector = this.direction.clone();
                impactVector.multiplyScalar(impactDamage);
                var impactPosition = intersect.point.clone();
                intersect.object.worldToLocal(impactPosition);

                PRESKISO.GameController.hitEntity(intersect.object, this, impactDamage, intersect.point, impactPosition, impactVector);
                // intersect.object.applyImpulse(impactVector, impactPosition);

                var decal_size = .2 + .3 * this.impactSize;
                var decal_mesh = new THREE.Mesh(
                    decal_geometry, decal_material
                );
                decal_mesh.scale.set(decal_size, decal_size, decal_size);
                decal_mesh.lookAt(normal);
                decal_mesh.position.copy(impactPosition);
                decal_mesh.receiveShadow = true;
                // decal_mesh.position.multiplyScalar(1.01);
                if (intersect.object === ground) {
                    decal_mesh.position.y += .05;
                } else {
                    if (decal_mesh.position.x > 0) decal_mesh.position.x += 0.05; else decal_mesh.position.x -= 0.05;
                    if (decal_mesh.position.y > 0) decal_mesh.position.y += 0.05; else decal_mesh.position.y -= 0.05;
                    if (decal_mesh.position.z > 0) decal_mesh.position.z += 0.05; else decal_mesh.position.z -= 0.05;
                }
                intersect.object.add(decal_mesh);
                impactAdded(decal_mesh);

                if (this.piercingFactor > 0 && targetEntity.piercingFactor > 0) {
                    var factor = targetEntity.piercingFactor * this.piercingFactor;
                    var factorSq = Math.pow(factor, 2);
                    // nextSpeed = this.speed * factor;
                    var newDamage = impactDamage * factor;
                    var newRange = (this.range - this.distanceDone - intersect.distance) * factorSq;
                    var newImpactSize = this.impactSize * factor;
                    var newShotLength = this.shotLength * factor;
                    if (newDamage > 10 && newRange > 5) {
                        var position = this.raycaster.ray.direction.clone().multiplyScalar(0.2).add(intersect.point);
                        var bullet = new BulletEntity(this.player, position, this.direction, {
                            power: newDamage,
                            speed: this.speed,
                            range: newRange,
                            color: this.color,
                            shotLength: newShotLength,
                            isExplosive: this.isExplosive,
                            ammoModel: this.ammoModel,
                            impactSize: newImpactSize,
                            piercingFactor: this.piercingFactor,
                        });
                        PRESKISO.GameController.addEntity(bullet);
                    }
                }
            } else {
                var position = this.raycaster.ray.direction.clone().multiplyScalar(-0.2).add(intersect.point);
                var explosion = new PRESKISO.ExplosionEntity(this.player, position, 8, this.power);
                PRESKISO.GameController.addEntity(explosion);
                PRESKISO.AudioManager.playSound(this.object.position, "rocket_explosion", 20, {randomizePlaybackRateBy: 0.1});
            }

            return true;
        }
        // bullet.rotateOnAxis(THREE.X_AXIS, Math.PI / 20 * delta);
        // raycaster.ray.direction.set(0, 0, 1);
        // bullet.localToWorld(raycaster.ray.direction);
        // raycaster.ray.direction.sub(bullet.position);
        this.object.translateOnAxis(THREE.Z_AXIS, distance);
        this.distanceLeft -= distance;
        this.distanceDone += distance;
        if (!this.ammoModel) {
            if (!this.object.visible) {
                // this.object.visible = true;
            }
            if (this.distanceDone < this.shotLength) {
                this.object.scale.z = this.distanceDone / this.shotLength;
                // this.cylinder.position.z = -this.cylinder.scale.y * this.shotLength / 2;
            } else {
                this.object.scale.z = 1;
            }
        }
    }

    BulletEntity.prototype.createObject = function () {
        if (this.ammoModel !== undefined) {
            var resource = PRESKISO.Loader.getResource(this.ammoModel);
            var mat = resource.materials[0].clone();
            this.object = new THREE.Mesh(
                resource.geometry,
                mat
            );
            this.object.receiveShadow = this.object.castShadow = true;
        } else {
            var geometry = new THREE.CylinderGeometry( .01, .02, this.shotLength, 6 );
            geometry.applyMatrix( new THREE.Matrix4().makeRotationX(Math.PI / 2).multiply(new THREE.Matrix4().makeTranslation(0, -this.shotLength / 2, 0)) );
            var material = new THREE.MeshBasicMaterial( {color: this.color} );
            this.object = new THREE.Mesh( geometry, material );
            this.object.visible = false;
        }
        this.object.lookAt(this.direction);
        this.object.position.copy(this.origin);

        this.raycaster.ray.origin = this.object.position;
    }

    BulletEntity.prototype.objectAdded = function (scene) {
    }

    return BulletEntity;
})();
