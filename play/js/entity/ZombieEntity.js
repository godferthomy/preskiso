PRESKISO.ZombieEntity = ( function () {
	 function ZombieEntity(health, maxSpeed, acquisitionDistance) {
		PRESKISO.Entity.call(this);

		this.isPill = true;
		this.color = new THREE.Color(0.05, 0.1, 0.075);
		this.isHittable = true;
		this.lastAttack = 0;
		this.isAttacking = false;
		this.piercingFactor = 0.5;
		this.maxSpeed = maxSpeed || 40;

		this.health = health || 60;

		this.attackSpeed = 1.5 + Math.random() * 0.5;
		this.attackPower = 120 / this.attackSpeed;
		this.attackDelay = ZombieEntity.ATTACK_DELAY / this.attackSpeed;
		this.attackGruntPlayed = false;

		this.materials = [];

		this.targetPosition = new THREE.Vector3();
		this.AI = new PRESKISO.AI.SmartZombie(this, acquisitionDistance);
		// this.AI = new PRESKISO.AI.SmartZombie(this);
		// if (Math.random() > 0.5) {
		// 	this.AI = new PRESKISO.AI.SmartZombie(this);
		// } else {
		// 	this.AI = new PRESKISO.AI.StupidZombie(this);
		// }

		this.walkAnimation = undefined;
		this.attackAnimations = undefined;
		this.attackAnimation = undefined;

		this.attackAnimationGruntFrame = 20 / 25;
		this.attackAnimationHitFrame = 30 / 25;
	}

	PRESKISO.Util.inherit(ZombieEntity, PRESKISO.Entity);

	// PRESKISO.Entity.prototype.clear = function () {
	// 	this.scene = undefined;
	// }

	ZombieEntity.prototype.setTargetPosition = function (position) {
		this.targetPosition = position;
	}

	ZombieEntity.prototype.isStanding = function () {
		return true;
		// var touches = this.object._physijs.touches;
		// for (var i = 0; i < touches.length; i++) {
		// 	var obj = scene._objects[touches[i]];
		// 	var ent = PRESKISO.GameController.getEntityFromObject(obj);
		// 	if (ent instanceof PRESKISO.GroundEntity) {
		// 		return true;
		// 	}
		// }
		// return false;
	}

	ZombieEntity.prototype.tick = function (delta, time) {
		if (this.health <= 0) {
			var timeSinceDeath = time - this.deathTime;
			if (timeSinceDeath > 3) {
				PRESKISO.GameController.removeEntity(this);
			} else if (timeSinceDeath > 2) {
				var alpha = 3 - timeSinceDeath;
				for (var i = 0; i < this.materials.length; i++) {
					this.materials[i].opacity = alpha;
				}
			} else if (timeSinceDeath < 0.5) {
	    		this.arms.rotation.x = timeSinceDeath * 2 * Math.PI / 2;
			}
			return;
		}
		this.AI.tick(delta, time);
		var force = this.object.getLinearVelocity().clone();
		force.multiplyScalar(-40);
		this.object.applyCentralForce(force);
		if (this.isStanding() && !this.isAttacking && this.targetPosition.distanceToSquared(this.object.position) > 6) {
			var speedSq = this.object.getLinearVelocity().lengthSq();

			if (!this.walkAnimation.isPlaying) {
				this.walkAnimation.loop = true;
				this.walkAnimation.play();
			}
			this.walkAnimation.timeScale = Math.max(0.2, Math.sqrt(speedSq) / 4);

			if (speedSq < this.maxSpeed) {
				var targetPosition = this.targetPosition.clone();
				targetPosition.y = this.object.position.y;
				var direction = new THREE.Vector3().subVectors(this.targetPosition, this.object.position).normalize();
				var force = direction.clone().multiplyScalar(this.maxSpeed * 12);
				this.object.lookAt(targetPosition);
				this.object.applyCentralForce(force);
				this.object.__dirtyRotation = true;
			}
		} else if (this.walkAnimation.isPlaying) {
			this.walkAnimation.timeScale = 2;
			this.walkAnimation.loop = false;
		}

		if (!this.isAttacking && time - this.lastAttack > this.attackDelay) {
			this.lastAttack = time;
    		var intersect = this.firstIntersectInFront(2.4);
	    	if (intersect !== undefined) {
	    		var targetEntity = PRESKISO.GameController.getEntityFromObject(intersect.object);
	    		if (!(targetEntity instanceof ZombieEntity)) {
	    			this.startAttacking();
	    		} else {
	    			var force = new THREE.Vector3().subVectors(intersect.object.position, this.object.position).normalize();
	    			force.multiplyScalar(-550);
	    			this.object.applyCentralForce(force);
	    		}
	    	}
	    }
	    if (this.isAttacking) {
	    	if (!this.attackGruntPlayed && this.attackAnimation.currentTime >= this.attackAnimationGruntFrame) {
    			PRESKISO.AudioManager.playSound(this.object.position, "zombie_attack", 6, {randomizePlaybackRateBy: 0.2});
    			this.attackGruntPlayed = true;
    		}
	    	else if (this.attackAnimation.currentTime >= this.attackAnimationHitFrame) {
	    		this.finishAttacking();
	    	}
	    }
		this.setHealth(this.health - delta);
	}

	ZombieEntity.prototype.firstIntersectInFront = function (distance) {
		distance = distance || 2.2;
		var aimPosition = new THREE.Vector3(0, 0.5, 0.5);
		var direction = THREE.Z_AXIS.clone().add(aimPosition);
		this.object.localToWorld(direction);
		this.object.localToWorld(aimPosition);
		direction.sub(aimPosition).normalize();
		var raycaster = new THREE.Raycaster(aimPosition, direction, 0, distance);
    	return PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getHittableObjects(), true);
	}

	ZombieEntity.prototype.startAttacking = function () {
		this.isAttacking = true;
		this.attackAnimation = this.attackAnimations[Math.floor(Math.random() * this.attackAnimations.length)];
		this.attackAnimation.loop = false;
		this.attackAnimation.timeScale = this.attackSpeed;
		this.attackAnimation.reset();
		this.attackAnimation.play();
		this.attackingPhase = 0;
		this.attackGruntPlayed = false;
	}

	ZombieEntity.prototype.finishAttacking = function () {
		this.isAttacking = false;
		this.lastAttack = PRESKISO.GameController.getCurrentTime() + this.attackDelay * Math.random();
		var intersect = this.firstIntersectInFront(2.6);
    	if (intersect !== undefined) {
			var direction = THREE.Z_AXIS.clone();
			this.object.localToWorld(direction);
			direction.sub(this.object.position);
			var targetObject = intersect.object;
    		var targetEntity = PRESKISO.GameController.getEntityFromObject(targetObject);
    		var power = this.attackPower;
    		if (targetEntity instanceof PRESKISO.PlayerEntity) {
    			power /= 5;
    		}
    		if (targetEntity.health === undefined) {
    			this.lastAttack += this.attackDelay * 2;
    		}
    		if (!(targetEntity instanceof ZombieEntity)) {
		    	var impactVector = direction.clone();
	            impactVector.multiplyScalar(power);
	            var impactPosition = intersect.point.clone();
	            targetObject.worldToLocal(impactPosition);

	            PRESKISO.GameController.hitEntity(targetObject, this, power, intersect.point, impactPosition, impactVector);
	    	}
	    }
	}

	ZombieEntity.prototype.createObject = function () {
		var cylinderHeight = 1.9, radius = 1.3;//1.06;
	    var hittableCapsuleGeometry = new THREE.CylinderGeometry(radius, radius, cylinderHeight, 12);
	    for (var i = -1; i <= 1; i+= 2) {
			var geometry = new THREE.SphereGeometry(radius, 8, 4);
			geometry.applyMatrix( new THREE.Matrix4().makeTranslation(0, cylinderHeight / 2 * i, 0) );
			hittableCapsuleGeometry.merge(geometry);
			// THREE.GeometryUtils.merge(hittableCapsuleGeometry, geometry);
		}
	    this.object = new Physijs.CapsuleMesh(
	        hittableCapsuleGeometry,
	        Physijs.createMaterial(
	            new THREE.MeshBasicMaterial({color: new THREE.Color(0, 1, 1), visible: false}),
	            .8, // high friction
	            0.05 // low restitution
	        ),
	        10
	    );
	    this.object.material.impactSound = "impacts/body_impact";
	    this.object.isHittable = true;
	    this.object.position.y = 4;

		var resource = PRESKISO.Loader.getResource("pill-zombie");
		var geometry = resource.geometry;
	    var mat = new THREE.MeshFaceMaterial(resource.materials);
	    // var mat = resource.materials[0].clone();

	    var skinnedMaterials = [];
		for (var i = 0; i < resource.material.materials.length; i++) {
			var m = resource.material.materials[ i ].clone();
			m.skinning = true;
			// m.morphTargets = true;
			skinnedMaterials.push(m);
		}
		var skinnedMaterial = new THREE.MeshFaceMaterial(skinnedMaterials);

	    this.animatedMesh = new THREE.SkinnedMesh( geometry, skinnedMaterial );
	    this.animatedMesh.receiveShadow = this.animatedMesh.castShadow = true;
		this.object.add(this.animatedMesh);
		// this.animatedMesh.isHittable = true;
	    this.animatedMesh.receiveShadow = this.animatedMesh.castShadow = true;

		this.registerAnimations(this.animatedMesh, geometry.animations);
		window.console.log(geometry.animations);
		this.walkAnimation = this.getAnimationByName("ZombieWalk");
		this.attackAnimations = [this.getAnimationByName("ZombieAttack"), this.getAnimationByName("ZombieAttack2")];

	    // this.materials.push(mat);
	    // mat.materials[mat.materials.length - 1].color.copy(this.color);
	    // mat.impactSound = "impacts/body_impact";
		// this.object.scale.set(0.8, 0.8, 0.8);

	    // var headWeight = new Physijs.CapsuleMesh(
	    //     new THREE.BoxGeometry(0.01, 0.01, 0.01),
	    //     Physijs.createMaterial(
	    //         new THREE.MeshBasicMaterial(),
	    //         0, // high friction
	    //         0 // low restitution
	    //     ),
	    //     100
	    // );
	    // this.object.add(headWeight);


	    var arm_material = new THREE.MeshLambertMaterial({color: this.color});
	    this.materials.push(arm_material);

	    var arms_geometry = new THREE.Geometry();
	    var arm_geometry = new THREE.CylinderGeometry(.2, .25, .9, 8);
	    var hand_geometry = new THREE.SphereGeometry(.2, 8, 4);
	    hand_geometry.applyMatrix( new THREE.Matrix4().makeTranslation(0, 0.4, 0) );
	    arm_geometry.merge(hand_geometry);
	    arm_geometry.applyMatrix( new THREE.Matrix4().makeTranslation(0, 0.4, 0) );
	    arm_geometry.applyMatrix( new THREE.Matrix4().makeRotationX(Math.PI / 2) );
	    for (var i = -1; i <= 1; i+= 2) {
	    	arms_geometry.merge(arm_geometry, new THREE.Matrix4().makeTranslation(0.8 * i, 0, 0));
	    }
	    this.arms = new THREE.Mesh(arms_geometry, arm_material);
	    this.arms.position.set(0, .2, .2);
	    // this.object.add(this.arms);
	    // this.object.add(hittableCapsule);

		PRESKISO.Util.setCollisionGroup(this.object, "PILL");
	}

	ZombieEntity.prototype.objectAdded = function (scene) {
		this.object.setAngularFactor(new THREE.Vector3( 0, 0, 0 ));
	}

	ZombieEntity.prototype.getHit = function (power, objectPosition, sourceEntity) {
		ZombieEntity.prototype.parent.getHit.call(this, power, objectPosition, sourceEntity);
		if (this.health <= 0) {
			return;
		}
		if (objectPosition.y > 1) {
			power *= 5;
		}
		this.setHealth(this.health - power);
		return this.health;
	}

	ZombieEntity.prototype.setHealth = function (health) {
		if (health <= 0 && this.health <= 0) {
			return;
		}
		this.health = health;
		if (this.health <= 0) {
			this.die();
		}
	}

	ZombieEntity.prototype.die = function () {
		ZombieEntity.prototype.parent.die.call(this);
		this.object.setAngularFactor(new THREE.Vector3(1, 0.1, 1));
		this.object.material._physijs.friction = 1;
		this.object.material._physijs.restitution = 0;
		// this.object.setLinearFactor(new THREE.Vector3(3, 1, 3));
    	PRESKISO.AudioManager.playSound(this.object.position, "zombie_death", 20, {randomizePlaybackRateBy: 0.3});
    	this.deathTime = PRESKISO.GameController.getCurrentTime();
    	for (var i = 0; i < this.materials.length; i++) {
			this.materials[i].transparent = true;
		}
		this.impactFactor = 5;
	}

	ZombieEntity.ATTACK_DELAY = 0.7;

	return ZombieEntity;
})();
