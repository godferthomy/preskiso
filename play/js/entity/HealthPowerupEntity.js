PRESKISO.HealthPowerupEntity = (function () {
	var geometry = new THREE.BoxGeometry(0.2, 1, 0.2);
	geometry.merge(new THREE.BoxGeometry(1, 0.2, 0.2));
	var mat = new THREE.MeshLambertMaterial({color: new THREE.Color(0xFF3333)});

	function HealthPowerupEntity(position, options) {
		PRESKISO.PowerupEntity.call(this, position, options);

		options = options || {};

		this.lastCheckingTime = -10;
		this.position.y += 0.5;
		this.healAmount = Math.random() * 20 + 20;
	}

	PRESKISO.Util.inherit(HealthPowerupEntity, PRESKISO.PowerupEntity);

	HealthPowerupEntity.prototype.giveBonus = function (player) {
		if (player.health < 100) {
			player.heal(this.healAmount);
			return true;
		} else {
			return false;
		}
	};

	HealthPowerupEntity.prototype.tick = function (delta, time) {
		HealthPowerupEntity.prototype.parent.tick.call(this, delta, time);
		if (this.nextSpawn !== undefined) {
			return;
		}
		this.object.rotateOnAxis(THREE.Y_AXIS, delta * 1);
	};

	HealthPowerupEntity.prototype.createObject = function () {
		this.object = new THREE.Mesh(geometry.clone(), mat);
		this.object.receiveShadow = this.object.castShadow = true;
	};

	HealthPowerupEntity.prototype.respawn = function () {
		HealthPowerupEntity.prototype.parent.respawn.call(this);
		var newWeaponIndex = Math.floor(Math.random() * PRESKISO.Constants.Weapons.length);
		if (newWeaponIndex != this.weaponIndex) {
			this.weaponIndex = newWeaponIndex;
			this.ammoAmount = Math.round(PRESKISO.Constants.Weapons[this.weaponIndex].powerupAmmo * (Math.random() * 0.5 + 1));
			PRESKISO.GameController.removeEntity(this);
			PRESKISO.GameController.addEntity(this);
		}
	}

	HealthPowerupEntity.prototype.objectAdded = function () {
		this.object.position.copy(this.position);
	}

	HealthPowerupEntity.prototype.objectRemoved = function () {
	}

	return HealthPowerupEntity;
})();
