PRESKISO.WallEntity = ( function () {
	var texture = THREE.ImageUtils.loadTexture( "images/concrete3.jpg" );
	texture.anisotropy = 4;
	var mat = new THREE.MeshPhongMaterial({ map: texture});
	var wall_material = Physijs.createMaterial(
            mat,
            0.5, // high friction
            1 // low restitution
        );
	wall_material.map.wrapS = wall_material.map.wrapT = THREE.RepeatWrapping;

	wall_material.bumpMap = THREE.ImageUtils.loadTexture( "images/concrete3-bump.jpg" );
	wall_material.bumpMap.wrapS = wall_material.bumpMap.wrapT = THREE.RepeatWrapping;
	wall_material.bumpScale = 0.05;

    wall_material.impactSound = "impacts/concrete_impact";

    var material_pool = [];

	function WallEntity(dimensions, position) {
		PRESKISO.Entity.call(this);

		this.position = position;

		this.isDimmable = true;
		this.isHittable = true;

		this.hasBody = true;

		this.dimensions = dimensions;

		this.globalMaterial = wall_material;
		this.material_pool = material_pool;
	}

	PRESKISO.Util.inherit(WallEntity, PRESKISO.Entity);

	WallEntity.prototype.tick = function (delta) {
	}

	WallEntity.prototype.createBody = function () {
		var boxShape = new CANNON.Box(new CANNON.Vec3(this.dimensions.x, this.dimensions.y, this.dimensions.z).mult(0.5));
		this.body = new CANNON.Body({ mass: 0 });
        this.body.addShape(boxShape);
        this.body.position.set(this.position.x, this.position.y, this.position.z);
	}

	WallEntity.prototype.createObject = function () {
  		var geometry;
  		if (this.dimensions.x >= this.dimensions.z) {
			geometry = new THREE.BoxGeometry(this.dimensions.x, this.dimensions.y, this.dimensions.z);
		} else {
			var temp = this.dimensions.x;
			this.dimensions.x = this.dimensions.z;
			this.dimensions.z = temp;
			geometry = new THREE.BoxGeometry(this.dimensions.x, this.dimensions.y, this.dimensions.z);
			geometry.applyMatrix( new THREE.Matrix4().makeRotationY(-Math.PI / 2));
		}

		var uvs = geometry.faceVertexUvs[0];
		var size = this.dimensions;
		var xFaces = [4, 5, 6, 7, 8, 9, 10, 11];
		var yFaces = [8, 9, 10, 11];
		var xCoeff = Math.max(1, Math.round(size.x / PRESKISO.GameController.mapTo3dRatio * 2) / 2);
		var yCoeff = size.y / PRESKISO.GameController.mapTo3dRatio;
		for (var i = 0; i < xFaces.length; i++) {
			for (var j = 0; j < 3; j++) {
				uvs[xFaces[i]][j].x *= xCoeff;
			}
		}
		for (var i = 0; i < yFaces.length; i++) {
			for (var j = 0; j < 3; j++) {
				uvs[yFaces[i]][j].y *= yCoeff;
			}
		}
		geometry.uvsNeedUpdate = true;

    	var wall = new Physijs.BoxMesh(
	        geometry,
	        wall_material,
	        0
	    );
	    wall.isHittable = true;
	    wall.receiveShadow = wall.castShadow = true;

	    this.object = wall;

	    this.object.position.copy(this.position);
	    PRESKISO.Util.setCollisionGroup(this.object, "STATIC");
	}

	WallEntity.prototype.objectAdded = function (scene) {
	}

	return WallEntity;
})();
