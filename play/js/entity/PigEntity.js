PRESKISO.PigEntity = ( function () {
	function PigEntity(maxSpeed) {
		PRESKISO.CarriableEntity.call(this, maxSpeed);

		this.isPill = true;
		// this.color = new THREE.Color(0.05, 0.1, 0.075);
		this.isHittable = true;
		this.piercingFactor = 0.5;
		this.maxSpeedSq = maxSpeed || 60;
		this.health = /*health ||*/ 60;

		this.nextRegularSoundTime = Math.random() * 5 + PRESKISO.GameController.getCurrentTime();
		this.nextStressedSoundTime = undefined;
		this.nextPanickedSoundTime = undefined;

		this.materials = [];

		this.targetPosition = new THREE.Vector3();
		this.AI = new PRESKISO.AI.ScaredAI(this);
		// if (Math.random() > 0.5) {
		// 	this.AI = new PRESKISO.AI.SmartZombie(this);
		// } else {
		// 	this.AI = new PRESKISO.AI.StupidZombie(this);
		// }
		this.autoPickup = true;
		this.radius = 4;
		this.height = 6;
		this.name = "Pig...";

		this.animations = [];
		this.walkAnimationName = "RunningPig";
		this.animation = undefined;
	}

	PRESKISO.Util.inherit(PigEntity, PRESKISO.CarriableEntity);

	// PRESKISO.Entity.prototype.clear = function () {
	// 	this.scene = undefined;
	// }

	PigEntity.prototype.setTargetPosition = function (position) {
		this.targetPosition = position;
	}

	PigEntity.prototype.tick = function (delta, time) {
		if (this.isAlive()) {
			var actualPosition = this.object.position.clone().setFromMatrixPosition( this.object.matrixWorld );
			if (this.state == "panicked") {
				if (this.nextPanickedSoundTime == undefined || time >= this.nextPanickedSoundTime) {
					this.nextPanickedSoundTime = time + 0.5 + Math.random();
					PRESKISO.AudioManager.playSound(actualPosition, "pig_panicked", 10, {randomizePlaybackRateBy: 0.15});
				}
			} else if (this.state == "stressed") {
				if (this.nextStressedSoundTime == undefined || time >= this.nextStressedSoundTime) {
					this.nextStressedSoundTime = time + 1.5 + Math.random();
					PRESKISO.AudioManager.playSound(actualPosition, "pig_stressed", 8, {randomizePlaybackRateBy: 0.15});
				}
			} else {
				if (this.nextRegularSoundTime == undefined || time >= this.nextRegularSoundTime) {
					this.nextRegularSoundTime = time + 5 + Math.random() * 5;
					PRESKISO.AudioManager.playSound(actualPosition, "pig_normal", 6, {randomizePlaybackRateBy: 0.2});
				}
			}
		}
		if (this.carrier !== undefined) {
			return;
		}
		if (this.health <= 0) {
			var timeSinceDeath = time - this.deathTime;
			if (timeSinceDeath > 3) {
				this.isInteractive = true;
				PRESKISO.GameController.removeEntity(this);
			} else if (timeSinceDeath > 2) {
				var alpha = 3 - timeSinceDeath;
				// for (var i = 0; i < this.materials.length; i++) {
				// 	this.materials[i].opacity = alpha;
				// }
				this.setOpacity(alpha);
			}
			return;
		}
		this.AI.tick(delta, time);
		var force = this.object.getLinearVelocity().clone();
		force.multiplyScalar(-40);
		this.object.applyCentralForce(force);
		if (this.targetPosition.distanceToSquared(this.object.position) > 2) {
			var speedSq = this.object.getLinearVelocity().lengthSq();
			var runningAnimation = this.getAnimationByName(this.walkAnimationName);
			if (!runningAnimation.isPlaying) {
				runningAnimation.loop = true;
				// this.animation.reset();
				runningAnimation.play();
			}
			runningAnimation.timeScale = Math.max(0.5, Math.sqrt(speedSq) / 2);
			if (speedSq < this.maxSpeedSq) {
				var targetPosition = this.targetPosition.clone();
				targetPosition.y = this.object.position.y;
				var direction = new THREE.Vector3().subVectors(this.targetPosition, this.object.position).normalize();
				var force = direction.clone().multiplyScalar(Math.sqrt(this.maxSpeedSq) * 40);
				this.object.lookAt(targetPosition);
				this.object.applyCentralForce(force);
				this.object.__dirtyRotation = true;
			}
		} else {
			var runningAnimation = this.getAnimationByName(this.walkAnimationName);
			if (runningAnimation.isPlaying) {
				window.console.log(runningAnimation);
				runningAnimation.timeScale = 2;
				runningAnimation.loop = false;
				if (Math.abs(runningAnimation.currentTime - runningAnimation.data.length / 2) < 0.1) {
					runningAnimation.reset();
					runningAnimation.update(0);
					runningAnimation.stop();
				}
			}
		}
	}

	PigEntity.prototype.createObject = function () {
		var cylinderHeight = .8, radius = 1;
	 //    var geometry = new THREE.CylinderGeometry(radius, radius, cylinderHeight, 8);

	 //    for (var i = -1; i <= 1; i+= 2) {
		// 	var otherGeometry = new THREE.SphereGeometry(radius, 8, 4);
		// 	otherGeometry.applyMatrix( new THREE.Matrix4().makeTranslation(0, cylinderHeight / 2 * i, 0) );
		// 	geometry.merge(otherGeometry);
		// 	// THREE.GeometryUtils.merge(hittableCapsuleGeometry, geometry);
		// }
		// geometry.applyMatrix( new THREE.Matrix4().makeRotationX(Math.PI / 2) );
		var resource = PRESKISO.Loader.getResource("pig");

		var skinnedMaterials = [];
		var objectMaterials = [];
		for (var i = 0; i < resource.material.materials.length; i++) {
			var m = resource.material.materials[ i ].clone();
			m.skinning = true;
			m.morphTargets = true;
			skinnedMaterials.push(m);

			// m.wrapAround = true;

			var m = resource.material.materials[ i ].clone();
			m.visible = false;
			objectMaterials.push(m);
		}
		var skinnedMaterial = new THREE.MeshFaceMaterial(skinnedMaterials);
		var objectMaterial = new THREE.MeshFaceMaterial(objectMaterials);

		var geometry = resource.geometry;
		var material = Physijs.createMaterial(
            objectMaterial,
            0.3, // high friction
            .2 // low restitution
        );
        material.impactSound = "pig_panicked";
        // this.materials.push(material);
		this.object = new Physijs.BoxMesh(
			geometry,
			material,
			5);
		// this.object.visible = false;
		this.object.rotation.z = Math.PI / 4;
	    this.object.receiveShadow = this.object.castShadow = true;
	    this.object.position.y = 3;


	    this.animatedMesh = new THREE.SkinnedMesh( geometry, skinnedMaterial );
	    this.animatedMesh.receiveShadow = this.animatedMesh.castShadow = true;
		this.object.add(this.animatedMesh);
		this.animatedMesh.isHittable = true;

		this.registerAnimations(this.animatedMesh, geometry.animations);
		// for (var i = 0; i < geometry.animations.length; i++) {
		// 	geometry.animations[i].initialized = false;
		// 	this.animations.push(new THREE.Animation( this.animatedMesh, geometry.animations[i] ));
		// }
		window.console.log(this.animations);

		PRESKISO.Util.setCollisionGroup(this.object, "PILL");
	}

	PigEntity.prototype.objectAdded = function (scene) {
		this.object.setAngularFactor(new THREE.Vector3( 0, 0, 0 ));
	}

	PigEntity.prototype.getHit = function (power, objectPosition, sourceEntity) {
		PigEntity.prototype.parent.getHit.call(this, power, objectPosition, sourceEntity);
		if (this.health <= 0) {
			return;
		}
		if (objectPosition.y > 1) {
			power *= 5;
		}
		this.setHealth(this.health - power);
		return this.health;
	}

	PigEntity.prototype.setHealth = function (health) {
		if (health <= 0 && this.health <= 0) {
			return;
		}
		this.health = health;
		if (this.health <= 0) {
			this.die();
		}
	}

	PigEntity.prototype.die = function () {
		this.isInteractive = false;
		if (this.carrier !== undefined) {
			this.carrier.dropCarriedEntity(this);
			this.impactFactor = 0.5;
		} else {
			this.impactFactor = 5;
		}
		PigEntity.prototype.parent.die.call(this);
		this.object.setAngularFactor(new THREE.Vector3(0.2, 0.2, 0.2));
		// this.object.setLinearFactor(new THREE.Vector3(3, 1, 3));
    	PRESKISO.AudioManager.playSound(this.object.position, "pig_death", 20, {randomizePlaybackRateBy: 0.2});
    	this.deathTime = PRESKISO.GameController.getCurrentTime();
  //   	for (var i = 0; i < this.materials.length; i++) {
		// 	this.materials[i].transparent = true;
		// }
	}

	PigEntity.prototype.setCarrier = function (carrier) {
		PigEntity.prototype.parent.setCarrier.call(this, carrier);
		if (carrier === undefined) {
			this.object.rotation.set(0, 0, 0);
			this.object.__dirtyRotation = this.object.__dirtyPosition = true;
			this.object.setAngularFactor(new THREE.Vector3(0, 0, 0));
		} else {
			this.state = "panicked";
		}
	}

	return PigEntity;
})();
