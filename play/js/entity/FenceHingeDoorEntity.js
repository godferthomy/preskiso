PRESKISO.FenceHingeDoorEntity = ( function () {
	var texture = THREE.ImageUtils.loadTexture( "images/woodenCrate.jpg" );
    var material = Physijs.createMaterial(
        new THREE.MeshPhongMaterial({ map: texture }),
        0.2, // high friction
        .1 // low restitution
    );
    material.impactSound = "impacts/wood_impact";

	function FenceHingeDoorEntity(dimensions, position, closedRotation, openedRotation) {
		PRESKISO.HingeDoorEntity.call(this, dimensions, position, closedRotation, openedRotation);

		this.piercingFactor = 0.6;
	}

	PRESKISO.Util.inherit(FenceHingeDoorEntity, PRESKISO.HingeDoorEntity);

	// FenceHingeDoorEntity.prototype.setHealth = function (health) {
	// 	FenceHingeDoorEntity.prototype.parent.setHealth.call(this, health);
	// 	if (health > 0) {
	// 		this.object.material.emissive.setRGB(1, 0, 0).multiplyScalar(Math.pow(1 - this.health / 1000, 2));
	// 		this.object.material.ambient.setRGB(0.5, 0, 0).multiplyScalar(Math.pow(1 - this.health / 1000, 2));
	// 	}

	// }

	FenceHingeDoorEntity.prototype.createObject = function () {
		var mat = material;
	    var geometry = new THREE.BoxGeometry(this.dimensions.x, this.dimensions.y, this.dimensions.z);
	    var door = new Physijs.BoxMesh(
	        geometry,
	        mat,
	        this.dimensions.x * this.dimensions.y * this.dimensions.z * 10
	    );

	    door.isHittable = true;
	    door.receiveShadow = door.castShadow = true;
		door.position.x -= this.dimensions.x / 2;
		this.object = door;
		this.object.position.copy(this.hingePosition);
		this.object.quaternion.setFromAxisAngle(new THREE.Vector3(0, 1, 0), this.closedRotation);

		this.isMoving = true;

		PRESKISO.Util.setCollisionGroup(this.object, "STATIC");
	}

	// FenceHingeDoorEntity.prototype.die = function () {
	// 	FenceHingeDoorEntity.prototype.parent.die.call(this);

	// 	this.object.material = material;

	// 	this.impactFactor = 10;
	// 	this.object.setAngularFactor(new THREE.Vector3( 1, .3, 1 ));

	// 	// cheat to make the door "blow" harder
	// 	var factor = new THREE.Vector3(1, 1, 3);
	// 	this.object.localToWorld(factor);
	// 	factor.sub(this.object.position);
	// 	factor.x = Math.abs(factor.x);
	// 	factor.y = Math.abs(factor.y);
	// 	factor.z = Math.abs(factor.z);

	// 	this.object.setLinearFactor(factor);

	// 	this.object.applyCentralImpulse(new THREE.Vector3(0, 50, 0));

	// 	(function (that) {
	// 		that.object.addEventListener( 'collision', function( other_object, relative_velocity, relative_rotation, contact_normal ) {
	// 			if (that.impactFactor != 1) {
	// 				that.object.setAngularFactor(new THREE.Vector3( 1, 1, 1 ));
	// 				that.object.setLinearFactor(new THREE.Vector3( 1, 1, 1 ));
	// 				that.impactFactor = 1;
	// 			}
	// 			var velocity = relative_velocity.lengthSq();
	// 			if (velocity > 40) {
	// 				PRESKISO.AudioManager.playSound(that.object.position, "metal_fall", velocity / 10, {randomizePlaybackRateBy: 0.15});
	// 			}
	// 		});
	// 	})(this);
	// }

	return FenceHingeDoorEntity;
})();
