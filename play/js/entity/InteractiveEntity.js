PRESKISO.InteractiveEntity = (function () {

	function InteractiveEntity() {
		PRESKISO.Entity.call(this, gameController);

		this.isInteractive = true;
	}

	PRESKISO.Util.inherit(InteractiveEntity, PRESKISO.Entity);

	InteractiveEntity.prototype.interact = function (sourceEntity) {
		// window.console.log("INTERACTED WITH BY ", sourceEntity);
	}
	return InteractiveEntity;
})();
