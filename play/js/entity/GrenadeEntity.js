PRESKISO.GrenadeEntity = (function () {
	var collisionGeometry = new THREE.SphereGeometry(0.2, 8, 4);
	// var geometry = new THREE.SphereGeometry(0.2, 8, 4);
 //    var geometry2 = new THREE.BoxGeometry(0.25, 0.1, 0.1);
 //    geometry2.applyMatrix( new THREE.Matrix4().makeRotationZ(Math.PI / 4).makeTranslation(Math.sin(Math.PI / 4) * 0.2, Math.cos(Math.PI / 4) * 0.2, 0) );
	// geometry.merge(geometry2);

	var grenade_material = Physijs.createMaterial(
        new THREE.MeshPhongMaterial({color: 0x003300}),
        1, // high friction
        0.3 // low restitution
    );

    var material_pool = [];

	function GrenadeEntity(player, position, direction, power) {
		PRESKISO.Entity.call(this);

		this.player = player;

		this.health = 100;
		this.delay = 3;

		this.isHittable = true;

		this.position = position.clone();
		this.direction = direction.clone();
		this.power = power;

		this.globalMaterial = grenade_material;
		this.material_pool = material_pool;
	}

	PRESKISO.Util.inherit(GrenadeEntity, PRESKISO.Entity);

	GrenadeEntity.prototype.tick = function (delta, time) {
		if (this.health <= 0 || time >= this.explosionTime) {
			var explosion = new PRESKISO.ExplosionEntity(this.player, this.object.position, 7, this.power);
            PRESKISO.GameController.addEntity(explosion);
            PRESKISO.GameController.removeEntity(this);
			PRESKISO.AudioManager.playSound(this.object.position, "grenade_explosion", 20, {randomizePlaybackRateBy: 0.1});
		} else {
			// this.object.applyCentralImpulse(new THREE.Vector3(0, -20 * delta, 0));
			var left = this.explosionTime - time;
			if (left < 1) {
				if (this.object.material == this.globalMaterial) {
					this.object.material = this.getIndividualMaterial();
				}
				this.object.material.color.r = 1 - left;
			}
		}
	}

	GrenadeEntity.prototype.getHit = function (power, objectPosition, sourceEntity) {
		GrenadeEntity.prototype.parent.getHit.call(this, power, objectPosition, sourceEntity);
		// if (this.health <= 0) {
		// 	return;
		// }
		// this.setHealth(this.health - power);
		this.explosionTime -= power / 100;
		return this.health;
	}

	GrenadeEntity.prototype.setHealth = function (health) {
		if (health <= 0 && this.health <= 0) {
			return;
		}
		this.health = health;
		if (this.health <= 0) {
			// this.die();
		}
	}

	GrenadeEntity.prototype.createObject = function () {
    	var grenade = new Physijs.SphereMesh(
	        collisionGeometry,
	        this.globalMaterial,
	        1
	    );
	    grenade.geometry = PRESKISO.Loader.getResource("weapons/grenade").geometry;

	    grenade.isHittable = true;
	    grenade.receiveShadow = grenade.castShadow = true;

	    this.object = grenade;

		PRESKISO.Util.setCollisionGroup(this.object, "MOBILE");
	}

	GrenadeEntity.prototype.objectAdded = function (scene) {
		this.explosionTime = PRESKISO.GameController.getCurrentTime() + this.delay;
		this.object.setDamping(0, 0.98);
		this.object.position.copy(this.position);
		this.object.__dirtyPosition = true;
		this.object.applyCentralImpulse(this.direction);

		(function (that) {
			that.object.addEventListener( 'collision', function( other_object, relative_velocity, relative_rotation, contact_normal ) {
				var velocity = relative_velocity.lengthSq();
				// if (velocity > 20) {
					PRESKISO.AudioManager.playSound(that.object.position, "grenade_bounce", velocity / 50, {randomizePlaybackRateBy: 0.15});
				// }
			});
		})(this);
	}

	GrenadeEntity.prototype.objectRemoved = function () {
		if (this.object.material != this.globalMaterial) {
			this.releaseIndividualMaterial();
		}
	}

	return GrenadeEntity;
})();
