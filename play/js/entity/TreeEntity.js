PRESKISO.TreeEntity = ( function () {
	var material_pool = [];

	function TreeEntity(position) {
		PRESKISO.Entity.call(this);

		this.height = 8.8;
		this.position = position.clone();
		this.position.y += this.height / 2;

		this.isDimmable = true;
		this.isHittable = true;

		this.material_pool = material_pool;
		this.minimumOpacity = 0.3;
	}

	PRESKISO.Util.inherit(TreeEntity, PRESKISO.Entity);


	// TreeEntity.prototype.getOpacity = function () {
	// 	return this.opacity;
	// }

	// TreeEntity.prototype.setOpacity = function (opacity) {
	// 	if (opacity < 1 && this.opacity == 1) {
	// 		this.object.material = this.getIndividualMaterial();
	// 		for (var i = 0; i < this.object.material.materials.length; i++) {
	// 			this.object.material.materials[i].transparent = true;
	// 		}
	// 	} else if (opacity >= 1 && this.opacity < 1) {
	// 		this.releaseIndividualMaterial();
	// 		this.object.material = this.globalMaterial;
	// 		for (var i = 0; i < this.object.material.materials.length; i++) {
	// 			this.object.material.materials[i].transparent = false;
	// 		}
	// 	}
	// 	for (var i = 0; i < this.object.material.materials.length; i++) {
	// 		this.object.material.materials[i].opacity = opacity;
	// 	}
	// 	this.opacity = opacity;
	// }

	// TreeEntity.prototype.manageDimming = function (delta) {
	// 	var currentOpacity = this.opacity, targetOpacity;
	// 	if (this.isDimmed && currentOpacity > 0.6) {
	// 		targetOpacity = Math.max(0.6, currentOpacity - delta * 6);
	// 	} else if (!this.isDimmed && currentOpacity < 1) {
	// 		targetOpacity = Math.min(1, currentOpacity + delta * 6);
	// 	}
	// 	if (targetOpacity) {
	// 		this.setOpacity(targetOpacity); 
	// 	}
	// }

	TreeEntity.prototype.createObject = function () {
  		var resource = PRESKISO.Loader.getResource("tree");
		var geometry = new THREE.CylinderGeometry(1.25, 1.25, this.height);
		if (this.globalMaterial == undefined) {
			this.globalMaterial = Physijs.createMaterial(
	            resource.material,
	            0.5, // high friction
	            .8 // low restitution
	        );
	    //     var mats = resource.material.materials;
	    //     mats[0].bumpMap = THREE.ImageUtils.loadTexture( "models/leaf-bump.jpg" );

	    //     mats[1].bumpMap = THREE.ImageUtils.loadTexture( "models/bark2-bump.jpg" );
	    //     for (var i = 0; i < mats.length; i++) {
	    //   //   	var map = mats[i].map;
	    //   //   	map.wrapS = map.wrapT = THREE.RepeatWrapping;
   		// 		// map.repeat.set( 5, 5 );

   		// 		var bumpMap = mats[i].bumpMap;
   		// 		if (bumpMap) {
		   //      	bumpMap.wrapS = bumpMap.wrapT = THREE.RepeatWrapping;
	   	// 			bumpMap.repeat.set( 5, 5 );
					// bumpMap.bumpScale = 1;
   		// 		}
   		// 	}
	        this.globalMaterial.impactSound = "impacts/wood_impact";
	    }
		this.object = new Physijs.CylinderMesh(
			geometry,
			this.globalMaterial,
			0);
		this.object.geometry = resource.geometry;
	    this.object.receiveShadow = this.object.castShadow = true;
	    this.object.isHittable = true;
	    this.object.position.copy(this.position);
	    this.object.rotation.y = Math.random() * Math.PI * 2;

		PRESKISO.Util.setCollisionGroup(this.object, "STATIC");
	}

	TreeEntity.prototype.objectAdded = function (scene) {
	}

	return TreeEntity;
})();
