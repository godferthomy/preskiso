PRESKISO.ExplosionEntity = ( function () {
    var texture = THREE.ImageUtils.loadTexture( "images/smoketex.jpg" );
    var material = new THREE.PointCloudMaterial({
        map: texture,
        size: 8,
        opacity: 0.5,
        color: 0xEECC22,
        transparent:    true,
        depthWrite: false,
        blending: THREE.AdditiveBlending,
    });

	function ExplosionEntity(player, position, size, power) {
		PRESKISO.Entity.call(this);

		this.player = player;
		this.power = power;
		this.position = position.clone();
        this.realPosition = position.clone();
		this.size = size;
	}

	PRESKISO.Util.inherit(ExplosionEntity, PRESKISO.Entity);

	// PRESKISO.Entity.prototype.clear = function () {
	// 	this.scene = undefined;
	// }

	ExplosionEntity.prototype.explode = function () {
        var hittableObjects = PRESKISO.GameController.getHittableObjects();
        var nonPillObjects = PRESKISO.GameController.getNonPillHittableObjects();
		// var allhittableObjects = PRESKISO.GameController.getHittableObjects();
  //       var hittableObjects = [];
  //       var nonPillObjects = [], pillObjects = [];
  //       for (var i = 0; i < allhittableObjects.length; i++) {
  //           var obj = allhittableObjects[i];
  //           if (obj.id != this.object.id) {
  //           	var entity = PRESKISO.GameController.getEntityFromObject(obj);
  //           	if (entity instanceof PRESKISO.PlayerEntity || entity instanceof PRESKISO.ZombieEntity) {
  //           		pillObjects.push(obj)
  //           	} else {
  //           		nonPillObjects.push(obj)
  //           	}
  //               hittableObjects.push(obj);
  //           }
  //       }
        var explosionPosition = this.realPosition;
        var superRaycaster = new THREE.Raycaster(
            explosionPosition,
            new THREE.Vector3(),
            0,
            1);
        var boomTargetIds = [];
        var sizeSquare = Math.pow(this.size, 2);
        for (var i = 0; i < hittableObjects.length; i++) {
            var object = hittableObjects[i];
            nonPillObjects.push(object);
            var objectPosition = new THREE.Vector3().setFromMatrixPosition( object.matrixWorld );
            var distSq = objectPosition.distanceToSquared(explosionPosition);
            if (distSq < sizeSquare) {
                superRaycaster.ray.direction.subVectors(objectPosition, explosionPosition).normalize();
                superRaycaster.far = Math.sqrt(distSq) + 0.5;
                var boomIntersect = PRESKISO.Util.intersectFirstHittableObject(superRaycaster, nonPillObjects, true );
                if (boomIntersect !== undefined) {
                    if (boomTargetIds.indexOf(boomIntersect.object.id) === -1) {
                        var impactDamage = this.power * (this.size - boomIntersect.distance / 2) / this.size;
                        var impactVector = superRaycaster.ray.direction.clone();
                        impactVector.multiplyScalar(impactDamage);
                        var impactPosition = boomIntersect.point.clone();
                        boomIntersect.object.worldToLocal(impactPosition);
                        PRESKISO.GameController.hitEntity(boomIntersect.object, this, impactDamage, boomIntersect.point, impactPosition, impactVector);
                        boomTargetIds.push(boomIntersect.object.id);
                    }
                }
            }
            nonPillObjects.pop();
        }
	}

	ExplosionEntity.prototype.tick = function (delta, time) {
		var timeElapsed = time - this.timeAdded;
		if (timeElapsed > 1) {
			PRESKISO.GameController.removeEntity(this);
		} else {
			var val = Math.pow(timeElapsed * 1, 0.2);
			this.object.material.color.r = 0.9 + (val) * 0.1;
			this.object.material.color.g = 0.5 + (val) * 0.5;
			this.object.material.color.b = 0.1 + (val) * 0.9;
			// this.object.scale.set(1, 1, 1).multiplyScalar(val);
			this.object.material.opacity = (1 - val);
            var dest = new THREE.Vector3();
            for (var i = 0; i < this.object.geometry.vertices.length; i++) {
                this.object.geometry.vertices[i].copy(this.particlesDestinations[i]).multiplyScalar(val);
            }
            this.object.geometry.verticesNeedUpdate = true;
		}
	}

	ExplosionEntity.prototype.createObject = function () {
        var geometry = new THREE.Geometry();

        var allhittableObjects = PRESKISO.GameController.getHittableObjects();
        var hittableObjects = [];
        for (var i = 0; i < allhittableObjects.length; i++) {
            var obj = allhittableObjects[i];
            var entity = PRESKISO.GameController.getEntityFromObject(obj);
            if (!(entity instanceof PRESKISO.PlayerEntity || entity instanceof PRESKISO.ZombieEntity)) {
                hittableObjects.push(obj)
            }
        }
        this.particles = [];
        this.particlesDestinations = [];
        var range = this.size + 2;
        var direction = new THREE.Vector3(0, 0, 1);
        var superRaycaster = new THREE.Raycaster(
            this.position.clone(),
            direction,
            0,
            range);
        var newPosition = superRaycaster.ray.origin;
        direction = superRaycaster.ray.direction;
        var rSteps = 12;
        var sSteps = 10
        var rAngle = Math.PI / rSteps * (rSteps / 2 - 1);
        for (var r = 1; r < rSteps - 1; r++) {
            rAngle -= Math.PI / rSteps * 2;
            direction.set(0, 0, 1).applyAxisAngle(THREE.X_AXIS, rAngle);
            // window.console.log(direction);
            for (var s = 0; s < sSteps; s++) {
                direction.applyAxisAngle(THREE.Y_AXIS, Math.PI / sSteps);
                direction.normalize();
                var intersect = PRESKISO.Util.intersectFirstHittableObject(superRaycaster, hittableObjects, true );
                var distance = range;
                if (intersect !== undefined) {
                    distance = intersect.distance;
                    var maxDistance = this.size;
                    if (distance < maxDistance) {
                        // newPosition.lerp(direction.clone().multiplyScalar(-distance).add(this.position), 0.2);
                        // if (s != 0) {
                        if (distance < maxDistance / 2) {
                            distance = 0;
                        }
                        // }
                    }
                }
                if (distance > 0) {
                    var vertex = new THREE.Vector3();
                    this.particlesDestinations.push(direction.clone().multiplyScalar(distance).add({x: Math.random(), y: Math.random(), z: Math.random()}));
                    geometry.vertices.push(vertex);
                    var subParts = Math.floor(distance / 2.5);
                    for (var i = 1; i < subParts; i++) {
                        var vertex = new THREE.Vector3();
                        this.particlesDestinations.push(direction.clone().multiplyScalar(distance / subParts * i).add({x: Math.random(), y: Math.random(), z: Math.random()}));
                        geometry.vertices.push(vertex);
                    }
                }
            }
        }
        this.position.copy(newPosition);
        var mat = material.clone();
        mat.size = this.size * .8;
        this.object = new THREE.PointCloud(
            geometry,
            mat);
        this.object.sortParticles = true;
	}

	ExplosionEntity.prototype.objectAdded = function (scene) {
		this.timeAdded = PRESKISO.GameController.getCurrentTime();
		this.object.position.copy(this.position);
		this.explode();
	}

    return ExplosionEntity;
})();
