PRESKISO.HingeDoorEntity = ( function () {

	function HingeDoorEntity(dimensions, position, closedRotation, openedRotation) {
		PRESKISO.DoorEntity.call(this, dimensions, position);

		closedRotation *= (Math.random() > 0.5) ? 1 : -1;
		this.hingePosition = new THREE.Vector3(dimensions.x / 2, 0, 0).applyAxisAngle(THREE.Y_AXIS, closedRotation).add(position);

		this.closedRotation = closedRotation;
		this.openedRotation = openedRotation;
		this.openedActualRotation = this.openedRotation;
	}

	PRESKISO.Util.inherit(HingeDoorEntity, PRESKISO.DoorEntity);

	HingeDoorEntity.prototype.tick = function (delta, time) {
		if (this.isMoving && this.object._physijs.touches.length > 1) {
			var blocked = false;
			var touches = this.object._physijs.touches;
			var blockingForGreaterZ = this.open && this.openedActualRotation > 0 || !this.open && this.openedActualRotation < 0
			var pos = new THREE.Vector3();
			for (var i = 0; i < touches.length; i++) {
				var obj = scene._objects[touches[i]];
				var ent = PRESKISO.GameController.getEntityFromObject(obj);
				if (ent.isPill) {//obj._physijs.mass > 0) {
					pos.copy(obj.position);
					this.object.worldToLocal(pos);
					// window.console.log(pos.z, this.open);
					if (pos.z > 0 == blockingForGreaterZ) {
						blocked = true;
						// this.isMoving = false;
						// break;

						pos.x = pos.y = 0;
						pos.setLength(3000 * delta);
						this.object.localToWorld(pos);
						pos.sub(this.object.position);
						obj.applyCentralImpulse(pos);
					}
				}
			}
			if (blocked) {
				this.movingTime -= delta / this.movingDuration * .6;
			}
		}
		HingeDoorEntity.prototype.parent.tick.call(this, delta, time);
	}

	HingeDoorEntity.prototype.updateFromMovingState = function () {
		var rotation = this.closedRotation + this.openedActualRotation * ((this.open) ? this.movingTime : 1 - this.movingTime);
		this.object.quaternion.setFromAxisAngle(new THREE.Vector3(0, 1, 0), rotation);
		this.object.position.copy(this.hingePosition);
		var rot = rotation + Math.PI / 2;
		this.object.position.z -= Math.cos(rot) * this.dimensions.x / 2;
		this.object.position.x -= Math.sin(rot) * this.dimensions.x / 2;
		this.object.__dirtyRotation = true;
		this.object.__dirtyPosition = true;
	}

	HingeDoorEntity.prototype.interact = function (sourceEntity) {
		HingeDoorEntity.prototype.parent.interact.call(this, sourceEntity);
		if (this.movingTime == 0 && this.open) {
			// check on which side the sourceEntity is, and open the door in the opposite direction
			var position = sourceEntity.getObject().position.clone();
			this.object.worldToLocal(position);
			this.openedActualRotation = this.openedRotation;
			if (position.z > 0) {
				this.openedActualRotation *= -1;
			}
		}
	}

	HingeDoorEntity.prototype.getHit = function (power, objectPosition) {
		if (objectPosition.x > this.dimensions.x / 3 && Math.abs(objectPosition.y) > this.dimensions.y / 4) {
			power *= 10;
		}
		HingeDoorEntity.prototype.parent.getHit.call(this, power, objectPosition);
	}

	return HingeDoorEntity;
})();
