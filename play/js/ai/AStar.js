PRESKISO.AI = PRESKISO.AI || {PathFinder: {}};

PRESKISO.AI.PathFinder.AStar = (function () {

    function pathTo(node){
        var curr = node,
            path = [];
        while(curr.parent) {
            path.push(curr);
            curr = curr.parent;
        }
        return path.reverse();
    }

    function getHeap() {
        return new BinaryHeap(function(node) {
            return node.f;
        });
    }

    var astar = {
        init: function(graph) {
            for (var i = 0, len = graph.nodes.length; i < len; ++i) {
                var node = graph.nodes[i];
                node.f = 0;
                node.g = 0;
                node.h = 0;
                node.visited = false;
                node.closed = false;
                node.parent = null;
            }
        },

        /**
        * Perform an A* Search on a graph given a start and end node.
        * @param {Graph} graph
        * @param {GridNode} start
        * @param {GridNode} end
        * @param {Object} [options]
        * @param {bool} [options.closest] Specifies whether to return the
                   path to the closest node if the target is unreachable.
        * @param {Function} [options.heuristic] Heuristic function (see
        *          astar.heuristics).
        */
        search: function(graph, start, end, options) {
            astar.init(graph);

            options = options || {};
            var heuristic = options.heuristic || astar.heuristics.manhattan,
                closest = options.closest || false;

            var openHeap = getHeap(),
                closestNode = start; // set the start node to be the closest if required

            start.h = heuristic(start, end);

            openHeap.push(start);

            while(openHeap.size() > 0) {

                // Grab the lowest f(x) to process next.  Heap keeps this sorted for us.
                var currentNode = openHeap.pop();

                // End case -- result has been found, return the traced path.
                if(currentNode === end) {
                    return pathTo(currentNode);
                }

                // Normal case -- move currentNode from open to closed, process each of its neighbors.
                currentNode.closed = true;

                // Find all neighbors for the current node.
                var neighbors = graph.neighbors(currentNode);

                for (var i = 0, il = neighbors.length; i < il; ++i) {
                    var neighbor = neighbors[i];

                    if (neighbor.closed || neighbor.isWall()) {
                        // Not a valid node to process, skip to next neighbor.
                        continue;
                    }

                    // The g score is the shortest distance from start to current node.
                    // We need to check if the path we have arrived at this neighbor is the shortest one we have seen yet.
                    var gScore = currentNode.g + neighbor.getCost(currentNode),
                        beenVisited = neighbor.visited;

                    if (!beenVisited || gScore < neighbor.g) {

                        // Found an optimal (so far) path to this node.  Take score for node to see how good it is.
                        neighbor.visited = true;
                        neighbor.parent = currentNode;
                        neighbor.h = neighbor.h || heuristic(neighbor, end);
                        neighbor.g = gScore;
                        neighbor.f = neighbor.g + neighbor.h;

                        if (closest) {
                            // If the neighbour is closer than the current closestNode or if it's equally close but has
                            // a cheaper path than the current closest node then it becomes the closest node
                            if (neighbor.h < closestNode.h || (neighbor.h === closestNode.h && neighbor.g < closestNode.g)) {
                                closestNode = neighbor;
                            }
                        }

                        if (!beenVisited) {
                            // Pushing to heap will put it in proper place based on the 'f' value.
                            openHeap.push(neighbor);
                        }
                        else {
                            // Already seen the node, but since it has been rescored we need to reorder it in the heap
                            openHeap.rescoreElement(neighbor);
                        }
                    }
                }
            }

            if (closest) {
                return pathTo(closestNode);
            }

            // No result was found - empty array signifies failure to find path.
            return [];
        },
        // See list of heuristics: http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html
        heuristics: {
            manhattan: function(pos0, pos1) {
                var d1 = Math.abs(pos1.x - pos0.x);
                var d2 = Math.abs(pos1.y - pos0.y);
                return d1 + d2;
            },
            diagonal: function(pos0, pos1) {
                var D = 1;
                var D2 = Math.sqrt(2);
                var d1 = Math.abs(pos1.x - pos0.x);
                var d2 = Math.abs(pos1.y - pos0.y);
                return (D * (d1 + d2)) + ((D2 - (2 * D)) * Math.min(d1, d2));
            }
        }
    };

    /**
    * A graph memory structure
    * @param {Array} gridIn 2D array of input weights
    * @param {Object} [options]
    * @param {bool} [options.diagonal] Specifies whether diagonal moves are allowed
    */
    function Graph(gridIn, options) {
        options = options || {};
        this.nodes = [];
        this.diagonal = !!options.diagonal;
        this.grid = [];
        for (var x = 0; x < gridIn.length; x++) {
            this.grid[x] = [];

            for (var y = 0, row = gridIn[x]; y < row.length; y++) {
                var node = new GridNode(x, y, row[y]);
                this.grid[x][y] = node;
                this.nodes.push(node);
            }
        }
    }

    Graph.prototype.neighbors = function(node, map) {
        var ret = [],
            x = node.x,
            y = node.y,
            grid = this.grid;

        // West
        if(grid[x-1] && grid[x-1][y]) {
            ret.push(grid[x-1][y]);
        }

        // East
        if(grid[x+1] && grid[x+1][y]) {
            ret.push(grid[x+1][y]);
        }

        // South
        if(grid[x] && grid[x][y-1]) {
            ret.push(grid[x][y-1]);
        }

        // North
        if(grid[x] && grid[x][y+1]) {
            ret.push(grid[x][y+1]);
        }

        if (this.diagonal && node.diagonal) {
            // Southwest
            if(grid[x-1] && grid[x-1][y-1] && grid[x-1][y-1].diagonal) {
                ret.push(grid[x-1][y-1]);
            }

            // Southeast
            if(grid[x+1] && grid[x+1][y-1] && grid[x+1][y-1].diagonal) {
                ret.push(grid[x+1][y-1]);
            }

            // Northwest
            if(grid[x-1] && grid[x-1][y+1] && grid[x-1][y+1].diagonal) {
                ret.push(grid[x-1][y+1]);
            }

            // Northeast
            if(grid[x+1] && grid[x+1][y+1] && grid[x+1][y+1].diagonal) {
                ret.push(grid[x+1][y+1]);
            }
        }

        return ret;
    };

    Graph.prototype.toString = function() {
        var graphString = [],
            nodes = this.grid, // when using grid
            rowDebug, row, y, l;
        for (var x = 0, len = nodes.length; x < len; x++) {
            rowDebug = [];
            row = nodes[x];
            for (y = 0, l = row.length; y < l; y++) {
                rowDebug.push(row[y].weight);
            }
            graphString.push(rowDebug.join(" "));
        }
        return graphString.join("\n");
    };

    function GridNode(x, y, value) {
        this.x = x;
        this.y = y;
        this.setValue(value);
    }

    GridNode.prototype.setValue = function(value) {
        // this.value = value;
        this.diagonal = true;
        if (value == PRESKISO.WallGenerator.Types.DOOR) { // Door
            this.diagonal = false;
            this.weight = 10;
        } else if (value == PRESKISO.WallGenerator.Types.DOOR_OPEN) { // Door
            this.diagonal = false;
            this.weight = 1;
        } else if (value < 1000) { // Any Other Walkable 
            this.weight = 1;
        } else { // Any Obstacle
            this.weight = 0;
        }
        this.rawWeight = this.weight;
        // window.console.log("Value of (" + this.x + "," + this.y + ") set to value:"+value+", weight:" + this.weight + ", diagonal:" + this.diagonal);
    }

    GridNode.prototype.toString = function() {
        return "[" + this.x + " " + this.y + "]";
    };

    GridNode.prototype.getCost = function() {
        return this.weight;
    };

    GridNode.prototype.isWall = function() {
        return this.weight === 0;
    };

    function BinaryHeap(scoreFunction){
        this.content = [];
        this.scoreFunction = scoreFunction;
    }

    BinaryHeap.prototype = {
        push: function(element) {
            // Add the new element to the end of the array.
            this.content.push(element);

            // Allow it to sink down.
            this.sinkDown(this.content.length - 1);
        },
        pop: function() {
            // Store the first element so we can return it later.
            var result = this.content[0];
            // Get the element at the end of the array.
            var end = this.content.pop();
            // If there are any elements left, put the end element at the
            // start, and let it bubble up.
            if (this.content.length > 0) {
                this.content[0] = end;
                this.bubbleUp(0);
            }
            return result;
        },
        remove: function(node) {
            var i = this.content.indexOf(node);

            // When it is found, the process seen in 'pop' is repeated
            // to fill up the hole.
            var end = this.content.pop();

            if (i !== this.content.length - 1) {
                this.content[i] = end;

                if (this.scoreFunction(end) < this.scoreFunction(node)) {
                    this.sinkDown(i);
                }
                else {
                    this.bubbleUp(i);
                }
            }
        },
        size: function() {
            return this.content.length;
        },
        rescoreElement: function(node) {
            this.sinkDown(this.content.indexOf(node));
        },
        sinkDown: function(n) {
            // Fetch the element that has to be sunk.
            var element = this.content[n];

            // When at 0, an element can not sink any further.
            while (n > 0) {

                // Compute the parent element's index, and fetch it.
                var parentN = ((n + 1) >> 1) - 1,
                    parent = this.content[parentN];
                // Swap the elements if the parent is greater.
                if (this.scoreFunction(element) < this.scoreFunction(parent)) {
                    this.content[parentN] = element;
                    this.content[n] = parent;
                    // Update 'n' to continue at the new position.
                    n = parentN;
                }
                // Found a parent that is less, no need to sink any further.
                else {
                    break;
                }
            }
        },
        bubbleUp: function(n) {
            // Look up the target element and its score.
            var length = this.content.length,
                element = this.content[n],
                elemScore = this.scoreFunction(element);

            while(true) {
                // Compute the indices of the child elements.
                var child2N = (n + 1) << 1,
                    child1N = child2N - 1;
                // This is used to store the new position of the element, if any.
                var swap = null,
                    child1Score;
                // If the first child exists (is inside the array)...
                if (child1N < length) {
                    // Look it up and compute its score.
                    var child1 = this.content[child1N];
                    child1Score = this.scoreFunction(child1);

                    // If the score is less than our element's, we need to swap.
                    if (child1Score < elemScore){
                        swap = child1N;
                    }
                }

                // Do the same checks for the other child.
                if (child2N < length) {
                    var child2 = this.content[child2N],
                        child2Score = this.scoreFunction(child2);
                    if (child2Score < (swap === null ? elemScore : child1Score)) {
                        swap = child2N;
                    }
                }

                // If the element needs to be moved, swap it, and continue.
                if (swap !== null) {
                    this.content[n] = this.content[swap];
                    this.content[swap] = element;
                    n = swap;
                }
                // Otherwise, we are done.
                else {
                    break;
                }
            }
        }
    };
    var offsets = [
        [0, 0.5], [0, -0.5], [-0.5, 0], [0.5, 0], // non diagonals
        [0.5, 0.5], [-0.5, -0.5], [0.5, -0.5], [-0.5, 0.5]
    ];

    var AStar = {
        map: [],
        graph: undefined,
        ratio: 1,
        init: function (map, ratio) {
            this.ratio = ratio;
            // for (var x = 0; x < map.length; x++) {
            //     this.map[x] = [];
            //     for (var y = 0; y < map[x].length; y++) {
            //         var value = map[x][y];
            //         if (value == 0) { // Door
            //             value = 10;
            //         } else if (value < 200) { // Any Other Walkable 
            //             value = 1;
            //         } else { // Any Obstacle
            //             value = 0;
            //         }
            //         this.map[x][y] = value;
            //     }
            // }
            this.map = map;
            this.graph = new Graph(this.map, {diagonal: true});
        },

        updateTileValue: function (position, value) {
            if (position instanceof THREE.Vector3) {
                position = this.get3dToMapPosition(position);
            }
            var x = position.x, y = position.y;
            this.map[x][y] = value;
            this.graph.grid[x][y].setValue(value);
        },

        findPath: function (startPosition, endPosition) {
            var start = this.get3dToMapPosition(startPosition, true);
            var end = this.get3dToMapPosition(endPosition, true);
            var startNode = this.graph.grid[start.x][start.y];
            var endNode = this.graph.grid[end.x][end.y];
            var result = [];
            if (startNode && endNode) {
                result = astar.search(this.graph, startNode, endNode, {closest: true, heuristics: astar.heuristics.diagonal});
            }
            return result;
        },

        updateWithEntitiesWeights: function (entities) {
            var nodes = this.graph.nodes;
            for (var i = 0, maxI = nodes.length; i < maxI; i++) {
                nodes[i].weight = nodes[i].rawWeight;
            }
            for (var i = 0, maxI = entities.length; i < maxI; i++) {
                var position = this.get3dToMapPosition(entities[i].getObject().position, true);
                this.graph.grid[position.x][position.y].weight += 3;
            }
        },

        get3dToMapPosition: function (position, freeOnly) {
            var x = Math.max(Math.min(position.x / this.ratio, this.map.length - 1), 0);
            var y = Math.max(Math.min(position.z / this.ratio, this.map[0].length - 1), 0);
            if (freeOnly && this.graph.grid[Math.round(x)][Math.round(y)].weight == 0) {
                for (var i = 0; i < offsets.length; i++) {
                    var thisX = Math.round(x + offsets[i][0]), thisY = Math.round(y + offsets[i][1]);
                    if (this.graph.grid[thisX][thisY].weight != 0) {
                        x = thisX;
                        y = thisY;
                        break;
                    }
                }
            }/* else {
                x = Math.round(x);
                y = Math.round(y);
            }*/
            return {
                x: Math.round(x),
                y: Math.round(y)
            };
        }
    }

    return AStar;
})();
