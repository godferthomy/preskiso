PRESKISO.AI = PRESKISO.AI || {};

PRESKISO.AI.SmartZombie = (function() {
	var target = new THREE.Vector3();

	function SmartZombie(entity, acquisitionDistance) {
		this.nextThinkingTime = -1000;
		this.nextPathfindingTime = 0;
		this.entity = entity;
		this.path = [];
		this.targetPlayer = undefined;
		this.acquisitionDistance = acquisitionDistance || 1E+100;
	}

	SmartZombie.prototype.tick = function (delta, time) {
		var thisObject = this.entity.getObject();
		var player;
		if (time >= this.nextThinkingTime) {
			this.nextThinkingTime = time + 0.5;
			var players = PRESKISO.GameController.getPlayers();
			var hadTarget = this.targetPlayer !== undefined;
			this.targetPlayer = undefined;
			player, minDistance = this.acquisitionDistance;
			for (var i = 0; i < players.length; i++) {
				if (players[i].isInScene) {
					var distance = players[i].getObject().position.distanceToSquared(thisObject.position)
					if (distance < minDistance) {
						minDistance = distance;
						this.targetPlayer = players[i];
						if (!hadTarget) {
							this.nextPathfindingTime = time;
						}
					}
				}
			}
		}
		var pathFindingTarget;
		if (this.targetPlayer !== undefined && this.targetPlayer.isInScene) {
			if (thisObject.position.distanceToSquared(this.targetPlayer.getObject().position) < 30) {
				this.entity.setTargetPosition(this.targetPlayer.getObject().position);
				this.nextPathfindingTime = time + 0.01;
			} else {
				pathFindingTarget = target.copy(this.targetPlayer.getObject().position);
				pathFindingTarget = target;
			}
		}
		if (time >= this.nextPathfindingTime) {
			if (pathFindingTarget === undefined) {
				target.set(0, 0, 1).applyAxisAngle(THREE.Y_AXIS, Math.random() * Math.PI * 2).setLength(PRESKISO.GameController.mapTo3dRatio).add(thisObject.position);
				pathFindingTarget = target;
			}
			var path = PRESKISO.AI.PathFinder.AStar.findPath(thisObject.position, pathFindingTarget);
			this.path = [];
			if (path.length > 0) {
				this.nextPathfindingTime = time + path.length * 0.2;
				for (var i = 0; i < path.length; i++) {
					this.path.push(new THREE.Vector3(path[i].x * PRESKISO.GameController.mapTo3dRatio, thisObject.position.y, path[i].y * PRESKISO.GameController.mapTo3dRatio));
				}
				this.entity.setTargetPosition(this.path.shift());
			} else {
				this.nextPathfindingTime = time + 1.5;
			}
		} else if (this.path.length > 0) {
			if (thisObject.position.distanceToSquared(this.entity.targetPosition) < 5) {
				this.entity.setTargetPosition(this.path.shift());
			}
		}
	}
	return SmartZombie;
})();
