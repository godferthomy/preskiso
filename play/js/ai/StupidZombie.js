PRESKISO.AI = PRESKISO.AI || {};

PRESKISO.AI.StupidZombie = function (entity) {
	this.lastThinkingTime = 0;
	this.entity = entity;

	this.targetPlayer;

}

PRESKISO.AI.StupidZombie.prototype.tick = function (delta, time) {
	if (time - this.lastThinkingTime > 3) {
		this.lastThinkingTime = time;
		var thisObject = this.entity.getObject();
		var players = PRESKISO.GameController.getPlayers();
		var player, minDistance = 1E+100;
		for (var i = 0; i < players.length; i++) {
			var distance = players[i].getObject().position.distanceToSquared(thisObject.position)
			if (distance < minDistance) {
				player = players[i];
				minDistance = distance;
			}
		}
		this.targetPlayer = player;
	}
	if (this.targetPlayer !== undefined) {
		this.entity.setTargetPosition(this.targetPlayer.getObject().position);
	}
}
