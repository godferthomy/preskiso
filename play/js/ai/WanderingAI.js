PRESKISO.AI = PRESKISO.AI || {};

PRESKISO.AI.WanderingAI = (function () {
	function WanderingAI(entity) {
		this.nextThinkingTime = 0;
		this.nextPathfindingTime = 0;
		this.entity = entity;
		this.path = [];

	}

	WanderingAI.prototype.tick = function (delta, time) {
		var thisObject = this.entity.getObject();
		var player;
		// if (time >= this.nextThinkingTime) {
		// 	this.nextThinkingTime = time + 0.5;
		// 	var players = PRESKISO.GameController.getPlayers();
		// 	minDistance = 1E+100;
		// 	this.closestPlayer = undefined;
		// 	for (var i = 0; i < players.length; i++) {
		// 		if (players[i].isInScene) {
		// 			var distance = players[i].getObject().position.distanceToSquared(thisObject.position)
		// 			if (distance < minDistance) {
		// 				minDistance = distance;
		// 				if (distance < 120) {
		// 					this.closestPlayer = players[i];
		// 				}
		// 			}
		// 		}
		// 	}
		// }
		if (this.path.length > 0) {
			if (thisObject.position.distanceToSquared(this.entity.targetPosition) < 6) {
				var target = this.path.shift();
				this.entity.setTargetPosition(target);
			}
		} else if (time > this.nextPathfindingTime) {
			var pathFindingTarget = new THREE.Vector3(0, 0, 1).applyAxisAngle(THREE.Y_AXIS, Math.random() * Math.PI * 2).setLength(PRESKISO.GameController.mapTo3dRatio).add(thisObject.position);
			this.nextPathfindingTime = time + 2;
			var path = PRESKISO.AI.PathFinder.AStar.findPath(thisObject.position, pathFindingTarget);
			this.path = [];
			if (path.length > 0) {
				for (var i = 0; i < path.length; i++) {
					this.path.push(new THREE.Vector3(path[i].x * PRESKISO.GameController.mapTo3dRatio, thisObject.position.y, path[i].y * PRESKISO.GameController.mapTo3dRatio));
				}
				var target = this.path.shift();
				this.entity.setTargetPosition(target);
			}
		}
	}

	return WanderingAI;
})();
