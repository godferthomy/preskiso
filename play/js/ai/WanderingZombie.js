PRESKISO.AI = PRESKISO.AI || {};

PRESKISO.AI.WanderingZombie = (function () {
	function WanderingZombie(entity) {
		this.nextThinkingTime = 0;
		this.nextPathfindingTime = 0;
		this.entity = entity;
		this.path = [];

	}

	WanderingZombie.prototype.tick = function (delta, time) {
		var thisObject = this.entity.getObject();
		var player;
		if (time >= this.nextThinkingTime) {
			this.nextThinkingTime = time + 1;
			this.targetPlayer = undefined;
			var players = PRESKISO.GameController.getPlayers();
			player, minDistance = 100;
			for (var i = 0; i < players.length; i++) {
				if (players[i].isInScene) {
					var distance = players[i].getObject().position.distanceToSquared(thisObject.position)
					if (distance < minDistance) {
						minDistance = distance;
						this.targetPlayer = players[i];
					}
				}
			}
		}
		if (this.targetPlayer !== undefined) {
			window.console.log("WOUHOU?");
			this.entity.setTargetPosition(this.targetPlayer.getObject().position);
			this.nextPathfindingTime = time + 1;
		} else if (this.path.length > 0) {
			if (thisObject.position.distanceToSquared(this.entity.targetPosition) < 6) {
				var target = this.path.shift();
				this.entity.setTargetPosition(target);
			}
		} else if (time > this.nextPathfindingTime) {
			var pathFindingTarget = new THREE.Vector3(0, 0, 1).applyAxisAngle(THREE.Y_AXIS, Math.random() * Math.PI * 2).setLength(PRESKISO.GameController.mapTo3dRatio).add(thisObject.position);
			this.nextPathfindingTime = time + 2;
			var path = PRESKISO.AI.PathFinder.AStar.findPath(thisObject.position, pathFindingTarget);
			this.path = [];
			if (path.length > 0) {
				for (var i = 0; i < path.length; i++) {
					this.path.push(new THREE.Vector3(path[i].x * PRESKISO.GameController.mapTo3dRatio, thisObject.position.y, path[i].y * PRESKISO.GameController.mapTo3dRatio));
				}
				var target = this.path.shift();
				this.entity.setTargetPosition(target);
			}
		}
	}

	return WanderingZombie;
})();
