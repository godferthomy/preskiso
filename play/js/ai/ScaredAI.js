PRESKISO.AI = PRESKISO.AI || {};

PRESKISO.AI.ScaredAI = (function () {
	var target = new THREE.Vector3();

	function ScaredAI(entity) {
		this.nextThinkingTime = 0;
		this.nextPathfindingTime = 0;
		this.entity = entity;
		this.path = [];
		this.closestPlayer = undefined; 

	}

	ScaredAI.prototype.tick = function (delta, time) {
		var thisObject = this.entity.getObject();
		var player;
		if (time >= this.nextThinkingTime) {
			this.nextThinkingTime = time + 0.5;
			var players = PRESKISO.GameController.getPlayers();
			minDistance = 100;
			this.closestPlayer = undefined;
			for (var i = 0; i < players.length; i++) {
				if (players[i].isInScene) {
					var distance = players[i].getObject().position.distanceToSquared(thisObject.position)
					if (distance < minDistance) {
						minDistance = distance;
						this.closestPlayer = players[i];
					}
				}
			}
		}
		var pathFindingTarget;
		if (this.closestPlayer && this.closestPlayer.isInScene) {
			// avoid him!
			var distance = thisObject.position.distanceToSquared(this.closestPlayer.getObject().position);
			if (distance < 30) { // a player is very close..
				this.entity.state = "panicked";
				// run the other way!!
				target.copy(thisObject.position).sub(this.closestPlayer.getObject().position).setLength(PRESKISO.GameController.mapTo3dRatio).add(thisObject.position);
				this.entity.setTargetPosition(target);
			} else { // he is not far enough
				this.entity.state = "stressed";
				// find a path further away
				target.copy(thisObject.position).sub(this.closestPlayer.getObject().position).setLength(PRESKISO.GameController.mapTo3dRatio * 2).add(thisObject.position);
				pathFindingTarget = target;
			}
		} else {
			this.entity.state = undefined;
			// move aimlessly
			target.set(0, 0, 1).applyAxisAngle(THREE.Y_AXIS, Math.random() * Math.PI * 2).setLength(PRESKISO.GameController.mapTo3dRatio).add(thisObject.position);
			pathFindingTarget = target;
		}
		if (pathFindingTarget !== undefined && time > this.nextPathfindingTime) {
			this.nextPathfindingTime = time + 2;
			var path = PRESKISO.AI.PathFinder.AStar.findPath(thisObject.position, pathFindingTarget);
			this.path = [];
			if (path.length > 0) {
				for (var i = 0; i < path.length; i++) {
					this.path.push(new THREE.Vector3(path[i].x * PRESKISO.GameController.mapTo3dRatio, thisObject.position.y, path[i].y * PRESKISO.GameController.mapTo3dRatio));
				}
				this.entity.setTargetPosition(this.path.shift());
			}
		} else if (this.path.length > 0) {
			if (thisObject.position.distanceToSquared(this.entity.targetPosition) < 6) {
				this.entity.setTargetPosition(this.path.shift());
			}
		}
	}

	return ScaredAI;
})();
