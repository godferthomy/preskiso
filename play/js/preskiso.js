'use strict';
var PRESKISO = PRESKISO || {};

// max 15 collision groups
PRESKISO.CollisionGroups = {
    NONE: 0,
    STATIC: 1 << 0,
    MOBILE: 1 << 1,
    PILL: 1 << 2
};

// specifies with which other groups an object can collide with
// assuming its group is set in the add
PRESKISO.CollisionMasks = {
    NONE: 0,
    STATIC: PRESKISO.CollisionGroups.PILL | PRESKISO.CollisionGroups.MOBILE,
    MOBILE: PRESKISO.CollisionGroups.STATIC | PRESKISO.CollisionGroups.MOBILE | PRESKISO.CollisionGroups.PILL,
    PILL: PRESKISO.CollisionGroups.STATIC | PRESKISO.CollisionGroups.MOBILE | PRESKISO.CollisionGroups.PILL
};

Physijs.scripts.worker = '../common/js/vendor/physics/physijs_worker.js';
Physijs.scripts.ammo = 'ammo.js';

var FLASHLIGHT_INTENSITY = 2,
    MAX_IMPACTS = 500,
    isRunning = true;


var dimmedObjects = [], dimmableObjects = [];
var loader, preloader, loadModels, container, targets = [], zoomLevel = 1, targetZoomLevel = 2.5, gameController,
    initScene, render, controls, player, gun, bullet_material, previousTime = undefined, startTime = Math.floor(new Date().getTime() / 1000),
    dynamicObjects = [], sounds = [],
    ground, camera_mesh, yawObject, pitchObject, verticalMirror,
    flashLight, flashLightTarget, flashLightVisible = true,
    isAwake = false,
    cubes = [], lockedObject, playerToLockedObject, lockedObjectParticleSystem, lockedObjectCharge, lockedObjectCharging, thrownObject, gunParticleSystem,
    anchors = [], anchoredObject, anchorConstraint, anchorPosition, anchorDistance,
    renderer, audioManager, render_stats, physics_stats, scene, world, ground, light, flatCamera, camera, mouseX, mouseY, aimTarget = new THREE.Vector3(), aimLaser, lastShot = 0, aimDot, aimPrecision = 1, isShooting = false, impacts = [],
    car = {}, clock = new THREE.Clock();
var fire = false;
var rope_constraint;
var ropes = [];
var $selection_outliner, gamepad, gamepads = [];

var keysDown = [];

THREE.X_AXIS = new THREE.Vector3(1, 0, 0);
THREE.Y_AXIS = new THREE.Vector3(0, 1, 0);
THREE.Z_AXIS= new THREE.Vector3(0, 0, 1);


//+ Jonas Raoni Soares Silva
//@ http://jsfromhell.com/array/shuffle [v1.0]
function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

var init = function () {
    try {
        renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setSize( window.innerWidth, window.innerHeight );
        renderer.shadowMapEnabled = true;
        renderer.shadowMapSoft = true;
        renderer.setClearColor( 0xffffff, 1 );
        // renderer.autoClear = false;
    } catch (e) {
        $("#loading_message").text("Couldn't start WebGL, please make sure it can run on your browser.");
        return;
    }

    // world = new CANNON.World();
    // world.quatNormalizeSkip = 0;
    // world.quatNormalizeFast = false;

    // var solver = new CANNON.GSSolver();

    // world.defaultContactMaterial.contactEquationStiffness = 1e9;
    // world.defaultContactMaterial.contactEquationRegularizationTime = 4;

    // solver.iterations = 7;
    // solver.tolerance = 0.1;
    // var split = true;
    // if(split)
    //     world.solver = new CANNON.SplitSolver(solver);
    // else
    //     world.solver = solver;

    // world.gravity.set(0,-20,0);
    // world.broadphase = new CANNON.NaiveBroadphase();
    // world.broadphase.useBoundingBoxes = true;

    gamepad = new Gamepad();
    $selection_outliner = $("#selection_outliner");
    var menuControllers = [];
    $("#menus .menu").each(function () {
        var controller;
        switch ($(this).attr("id")) {
            // case "options_menu":
            //     controller = PRESKISO.OptionsMenuController;
            //     break;
            // case "gamemode_menu":
            //     controller = PRESKISO.GameModeMenuController;
            //     break;
            // case "gamesettings_menu":
            //     controller = PRESKISO.GameSettingsMenuController;
            //     break;
            // case "playerpick_menu":
            //     controller = PRESKISO.PlayerPickingMenuController;
            //     break;
            default:
                controller = PRESKISO.PauseMenuController;
        }
        var menuController = new controller($(this));
        menuController.init();
        menuControllers.push(menuController);
    });
    PRESKISO.MenuNavigationController.init(menuControllers);
    gamepad.init();

    var aspect = 3, width = 100, height = width / aspect;
    PRESKISO.Loader.setRoot("models/");
    PRESKISO.LoadingManager.load();
}

initScene = function() {

    container.appendChild( renderer.domElement );
    render_stats = new Stats();
    render_stats.domElement.style.position = 'absolute';
    render_stats.domElement.style.top = '0px';
    render_stats.domElement.style.zIndex = 100;
    container.appendChild( render_stats.domElement );

    physics_stats = new Stats();
    physics_stats.domElement.style.position = 'absolute';
    physics_stats.domElement.style.top = '50px';
    physics_stats.domElement.style.zIndex = 100;
    container.appendChild( physics_stats.domElement );

    scene = new Physijs.Scene();//{broadphase: "sweepprune"});
    scene.setGravity(new THREE.Vector3( 0, -98, 0 ));
    // scene.addEventListener(
    //     'update',
    //     function() {
    //         // scene.simulate( undefined, 1 );
    //         // physics_stats.update();
    //     }
    // );

    // Light
    light = new THREE.DirectionalLight( 0xFFEFEE );
    light.position.set( 100, 150, -150 );
    light.target.position.copy( scene.position );
    light.castShadow = true;
    light.shadowCameraLeft = -50;
    light.shadowCameraTop = -50;
    light.shadowCameraRight = 50;
    light.shadowCameraBottom = 50;
    light.shadowCameraNear = 10;
    light.shadowCameraFar = 1000;
    light.shadowBias = -.001
    light.shadowMapWidth = light.shadowMapHeight = 2048;
    light.shadowDarkness = .4;


    light.onlyShadow = true;
    // light.shadowCascade = true;

    // light.shadowCascadeCount = 3;

    // light.shadowCascadeNearZ = [ -1.000, 0.9, 0.975 ];
    // light.shadowCascadeFarZ  = [  0.9, 0.975, 1.000 ];
    // light.shadowCascadeWidth = [ 2048, 2048, 2048 ];
    // light.shadowCascadeHeight = [ 2048, 2048, 2048 ];
    // light.shadowCascadeBias = [ 0.00005, 0.000065, 0.000065 ];

    // light.shadowCascadeOffset.set( 0, 0, -10 );


    light.intensity = .9;//.15;
    scene.add( light );

    var light2 = new THREE.DirectionalLight( 0xFFFFFF );
    light2.position.set( 100, 150, -150 );
    light2.target.position.copy( scene.position );
    light2.intensity = .9;//.15;
    scene.add( light2 );





    // flashLightTarget = new THREE.Object3D();
    // flashLightTarget.position.set(0, -0.4, 1);
    // flashLight = new THREE.SpotLight( 0xffffff, 1, 100, Math.PI / 5, 1 );
    // flashLight.position.set(0, -0.3, 0);
    // flashLight.target = flashLightTarget;


    // flashLight.castShadow = true;
    // flashLight.shadowMapWidth = 1024;
    // flashLight.shadowMapHeight = 1024;
    // flashLight.shadowCameraNear = 1;
    // flashLight.shadowCameraFar = 100;
    // flashLight.shadowCameraFov = flashLight.angle / Math.PI * 360;

    // flashLight.shadowDarkness = 1;

    // // scene.add(flashLight);
    // flashLight.add(flashLightTarget);








    // var maxAnisotropy = renderer.getMaxAnisotropy();

    // // var texture = THREE.ImageUtils.loadTexture( "images/grass.jpg" );
    // var texture = THREE.ImageUtils.loadTexture( "images/parking.jpg" );
    // texture.anisotropy = maxAnisotropy;

    // // Materials
    // var ground_material = Physijs.createMaterial(
    //     new THREE.MeshPhongMaterial({ map: texture }),
    //     // material,
    //     0.8, // high friction
    //     .8 // low restitution
    // );
    // ground_material.map.wrapS = ground_material.map.wrapT = THREE.RepeatWrapping;

    // ground_material.map.repeat.set( 50, 50 );
    
    // // If your plane is not square as far as face count then the HeightfieldMesh
    // // takes two more arguments at the end: # of x faces and # of y faces that were passed to THREE.PlaneMaterial
    // ground = new Physijs.BoxMesh(
    //     new THREE.BoxGeometry(500, 5, 500),
    //     ground_material,
    //     0
    // );
    // ground.position.y = -2.52;
    // ground.receiveShadow = true;

    // ground.receiveShadow = true;
    // ground.isHittable = true;

    // var ground_entity = new PRESKISO.Entity(ground);
    // ground_entity.isHittable = true;
    // PRESKISO.GameController.addEntity(ground_entity);




    // gameController.init();



    var maxAnisotropy = renderer.getMaxAnisotropy();

    // var texture = THREE.ImageUtils.loadTexture( "images/woodenCrate.jpg" );
    // texture.anisotropy = maxAnisotropy;

    // // Materials
    // var cube_material = Physijs.createMaterial(
    //     new THREE.MeshLambertMaterial({ map: texture, side: THREE.DoubleSide }),
    //     // new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture( 'images/grass.png' ), side: THREE.DoubleSide }),
    //     // material,
    //     1, // high friction
    //     .1 // low restitution
    // );
    // for (var i = 0; i < 30; i++) {
    //     var size = Math.floor(Math.random() * 2) * 2 + 2;
    //     var cube_geometry = new THREE.BoxGeometry(size, size, size);
    //     var cube = new Physijs.BoxMesh(
    //         cube_geometry,
    //         cube_material.clone(),
    //         size * size * size
    //     );
    //     cube.position.set(0, 2, 15 + Math.random() * 50);
    //     cube.position.applyAxisAngle(new THREE.Vector3(0, 1, 0), Math.random() * Math.PI * 2);
    //     cube.receiveShadow = cube.castShadow = true;
    //     cube.rotation.y = Math.random() * Math.PI * 2;

    //     cube.isHittable = true;

    //     var cubeEntity = new PRESKISO.Entity(gameController, cube);
    //     cubeEntity.isHittable = true;
    //     cubeEntity.isDimmable = true;
    //     gameController.addEntity(cubeEntity);
    //     // scene.add(cube);
    //     // targets.push(cube);
    //     // dimmableObjects.push(cube);

    // }

    // PLATFORM
    // var platform_material = cube_material.clone();
    // var height = 5, width = Math.random() * 5 + 15, depth = Math.random() * 5 + 10, thickness = .2;
    // var cube = new Physijs.BoxMesh(
    //     new THREE.BoxGeometry(width, thickness, depth),
    //     platform_material.clone(),
    //     0
    // );
    // for (var i = -1; i < 2; i+=2) {
    //     for (var j = -1; j < 2; j+=2) {
    //         var post = new Physijs.BoxMesh(
    //             new THREE.BoxGeometry(thickness, height, thickness),
    //             cube_material.clone(),
    //             0
    //         );
    //         post.position.x = i * (width - thickness) / 2;
    //         post.position.z = j * (depth - thickness) / 2;
    //         post.position.y -= height / 2;
    //         post.receiveShadow = post.castShadow = true;
    //         cube.add(post);
    //         targets.push(post);
    //     }
    // }
    // cube.position.y = height;

    // STAIRS
    // var baseLength = height * 2;
    // var stairLength = Math.sqrt(height * height + baseLength * baseLength);
    // var stairAngle = Math.asin(height / stairLength);
    // var stairWidth = 5;
    // var stair = new Physijs.BoxMesh(
    //     new THREE.BoxGeometry(stairWidth, thickness, stairLength),
    //     platform_material.clone(),
    //     0
    // );
    // stair.rotation.x = -stairAngle;

    // stair.position.x = (width - stairWidth) / 2
    // stair.position.z = -(depth + baseLength) / 2;
    // stair.position.y = -height / 2;
    // stair.receiveShadow = stair.castShadow = true;
    // cube.add(stair);
    // targets.push(stair);

    // targets.push(cube);
    // scene.add(cube);


    // SMALL HOUSE
    // var wallThickness = .2, width = 16, depth = 8, height = 6, doorWidth = 2.8, doorHeight = 5;
    // var house = new Physijs.BoxMesh(
    //     new THREE.BoxGeometry(0, 0, 0),
    //     cube_material.clone(),
    //     0
    // );

    // for (var i = 0; i < 4; i++) {
    //     var texture = THREE.ImageUtils.loadTexture( "images/concrete.jpg" );
    //     var wall_material = Physijs.createMaterial(
    //         new THREE.MeshPhongMaterial({ map: texture, side: THREE.DoubleSide }),
    //         1, // high friction
    //         .1 // low restitution
    //     );
    //     wall_material.impactSound = "impacts/concrete_impact";
    //     var wallWidth, wallPosition;
    //     if (i % 2 == 0) {
    //         wallWidth = width;
    //         wallPosition = depth;
    //     } else {
    //         wallWidth = depth;
    //         wallPosition = width;
    //     }
    //     for (var j = -1; j < 2; j++) {
    //         var thisHeight = (j == 0) ? height - doorHeight : height;
    //         var thisWidth = (j == 0) ? doorWidth : (wallWidth - doorWidth) / 2;
    //         var thisY = height - thisHeight / 2;
    //         var wall = new Physijs.BoxMesh(
    //             new THREE.BoxGeometry(thisWidth, thisHeight, wallThickness),
    //             wall_material,
    //             0
    //         );
    //         wall.position.set((thisWidth + doorWidth) / 2 * j, thisY, (wallPosition - wallThickness) / 2);
    //         wall.rotation.y = Math.PI / 2 * i;
    //         // wall.rotateOnAxis(new THREE.Vector3(0, 1, 0), Math.PI / 2 * i);
    //         wall.position.applyAxisAngle(new THREE.Vector3(0, 1, 0), Math.PI / 2 * i);
    //         wall.receiveShadow = wall.castShadow = true;
    //         house.add(wall);
    //         targets.push(wall);
    //         wall.isHittable = true;

    //         wall.updateMatrixWorld();
    //         if (j == 0) {
    //             var doorPosition = new THREE.Vector3(1.4, 2.5 - wall.position.y, 0);
    //             wall.localToWorld(doorPosition);
    //             var door = new PRESKISO.DoorEntity(gameController, new THREE.Vector3(2.8, 5, 0.1), doorPosition, wall.rotation.y, Math.PI / 1.5);
    //             // gameController.addEntity(door);
    //             window.console.log(door);
    //         }
    //     }
    // }
    // var texture = THREE.ImageUtils.loadTexture( "images/roof.jpg" );
    // var roof_material = Physijs.createMaterial(
    //     new THREE.MeshPhongMaterial({ map: texture, side: THREE.DoubleSide }),
    //     1, // high friction
    //     .1 // low restitution
    // );
    // roof_material.map.wrapS = roof_material.map.wrapT = THREE.RepeatWrapping;

    // roof_material.map.repeat.set( 5, 5 );
    // var roof = new Physijs.ConvexMesh(
    //     new THREE.CylinderGeometry(0, Math.sqrt(Math.pow(width, 2) / 2), 2, 4),
    //     roof_material,
    //     0
    // );
    // roof.receiveShadow = roof.castShadow = true;
    // roof.rotateOnAxis(new THREE.Vector3(0, 1, 0), Math.PI / 4);
    // roof.scale.x = depth / width;
    // roof.position.set(0, height + 1, 0);
    // roof.isHittable = true;
    // house.add(roof);
    // targets.push(roof);

    // var houseEntity
    // scene.add(house);
    // dimmableObjects.push(house);


    // var houseEntity = new PRESKISO.Entity(gameController, house);
    // houseEntity.isHittable = true;
    // houseEntity.isDimmable = true;
    // gameController.addEntity(houseEntity);




    // for (var i = 0; i < 5; i++) {
    //     var zombie = new PRESKISO.ZombieEntity(gameController);
    //     zombie.getObject().position.set(Math.random() * 10 + 25, 4, Math.random() * 10 + 30);
    //     gameController.addEntity(zombie);
    // }

    // requestAnimationFrame( render );
    // scene.simulate();

    PRESKISO.GameController.start();
}

function addDynamicObject (dynamicObject) {
    dynamicObjects.push(dynamicObject);
}

function addSound (sound) {
    sounds.push(sound);
}

render = function() {
    // if (isRunning)
    //     requestAnimationFrame( render );

    var delta = clock.getDelta();
    var time = clock.getElapsedTime();


    // scene.simulate( delta, 1 );
    // physics_stats.update();

    PRESKISO.GameController.tick(delta, time);
    // audioManager.tick(delta, time);

    // for (var i = 0; i < dimmedObjects.length; i++) {
    //     var obj = scene.getObjectById(dimmedObjects[i], true);
    //     var opacity = obj.material.opacity + delta * 4;
    //     if (opacity >= 1) {
    //         obj.material.transparent = false;
    //         obj.material.opacity = 1;
    //         dimmedObjects.splice(i, 1);
    //     } else {
    //         obj.material.opacity = opacity;
    //     }
    // }
    // // dimmedObjects = [];

    // var raycaster = new THREE.Raycaster(camera.position, new THREE.Vector3());
    // var players = gameController.getPlayers();
    // for (var i = 0; i < players.length; i++) {
    //     var playerPosition = players[i].object.position.clone();
    //     playerPosition.sub(camera.position);
    //     raycaster.far = playerPosition.length() - 1;
    //     playerPosition.normalize();
    //     raycaster.set(camera.position, playerPosition);
    //     var intersects = raycaster.intersectObjects( dimmableObjects, true );
    //     for (var j = 0; j < intersects.length; j++) {
    //         if (dimmedObjects.indexOf(intersects[j].object.id) === -1) {
    //             dimmedObjects.push(intersects[j].object.id);
    //             intersects[j].object.material.transparent = true;
    //         }
    //         var opacity = intersects[j].object.material.opacity;
    //         opacity = Math.max(.3, opacity - delta * 7);
    //         intersects[j].object.material.opacity = opacity;
    //     }
    // }


    // var SCREEN_WIDTH = window.innerWidth, SCREEN_HEIGHT = window.innerHeight;
    // renderer.setViewport( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
    // renderer.clear();
    
    // // upper left corner
    // renderer.setViewport( 1, 0.75 * SCREEN_HEIGHT, 0.25 * SCREEN_WIDTH, 0.25 * SCREEN_HEIGHT );
    // renderer.render( scene, players[0].camera );
    
    // // upper right corner
    // renderer.setViewport( 0.75 * SCREEN_WIDTH, 0.75 * SCREEN_HEIGHT, 0.25 * SCREEN_WIDTH, 0.25 * SCREEN_HEIGHT );
    // renderer.render( scene, players[1].camera );
    
    // // lower left corner
    // renderer.setViewport( 0.25 * SCREEN_WIDTH, 1,   0.5 * SCREEN_WIDTH - 2, SCREEN_HEIGHT - 2 );
    // renderer.render( scene, camera );



    // camera.position.addVectors(gameController.getPlayers()[0].object.position, new THREE.Vector3(0, 2, -5));
    // camera.lookAt(gameController.getPlayers()[0].object.position);

    previousTime = time;
};

function impactAdded(impact) {
    if (impacts.length > MAX_IMPACTS) {
        var impactToRemove = impacts.shift();
        impactToRemove.parent.remove(impactToRemove);
    }
    impacts.push(impact);
}

var getMousePositionOnScreen = (function () {
    var projector = new THREE.Projector();
    var raycaster = new THREE.Raycaster();
    var mouse = new THREE.Vector2();

    function onMouseMove( event ) {
        var domElement = renderer.domElement;
        // calculate mouse position in normalized device coordinates
        // (-1 to +1) for both components
        mouse.x = ( event.offsetX / domElement.offsetWidth ) * 2 - 1;
        mouse.y = - ( event.offsetY / domElement.offsetHeight ) * 2 + 1;
    }
    window.addEventListener( 'mousemove', onMouseMove, false );

    function _getMousePositionOnScreen() {
        var directionVector = new THREE.Vector3(mouse.x, mouse.y, 0);
        projector.unprojectVector(directionVector, camera);
        directionVector.sub(camera.position);
        directionVector.normalize();
        raycaster.set(camera.position, directionVector);
         
        var intersect = PRESKISO.Util.intersectFirstHittableObject(raycaster, PRESKISO.GameController.getHittableObjects(), true );
        if (intersect)
            return intersect.point;
        return intersect.point;
    }
    return _getMousePositionOnScreen;
})();

var element;
window.onload = function () {

        // var D = 1, // DOOR
        //     X = 2, // WALL
        //     W = 3; // WINDOW
        // var map = [
        //     [0,0,0,0,0,0],
        //     [0,0,W,W,0,0],
        //     [0,0,W,0,0,0],
        //     [0,W,W,W,0,0],
        //     [0,0,0,W,W,0],
        //     [0,0,0,0,0,0],
        // ];
        // var pathFinder = new PRESKISO.AI.PathFinder.AStar(map);
        // pathFinder.findPath({x:0, y:0}, {x:5, y:4});
        // return;

    element = document.body;
    container = document.getElementById( 'viewport' );
    init();
    // container.addEventListener("mousemove", moveCallback, false);

    // var havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;
    // if (havePointerLock) {
    //     var pointerlockchange = function ( event ) {

    //         if ( document.pointerLockElement === element || document.mozPointerLockElement === element || document.webkitPointerLockElement === element ) {
    //             document.addEventListener("mousemove", moveCallback, false);

    //         } else {
    //             document.removeEventListener("mousemove", moveCallback);

    //         }

    //     }

    //     // Hook pointer lock state change events
    //     document.addEventListener( 'pointerlockchange', pointerlockchange, false );
    //     document.addEventListener( 'mozpointerlockchange', pointerlockchange, false );
    //     document.addEventListener( 'webkitpointerlockchange', pointerlockchange, false );

    //     document.getElementById("viewport").addEventListener( 'click', function ( event ) {
    //         document.getElementById("viewport").removeEventListener( 'click');
    //         element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;
    //         element.requestPointerLock();
    //         element.requestFullscreen = element.requestFullscreen || element.mozRequestFullscreen || element.mozRequestFullScreen || element.webkitRequestFullscreen;
    //         element.requestFullscreen();
    //     });

    //     // document.getElementById("viewport").click();
    // } else {
    //     window.console.log("CRAP");
    // }

}
