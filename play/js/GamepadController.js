PRESKISO.GamepadController = function (player, gamepadId, gamepad) {
	var THRESHOLD = 0.1, THRESHOLDINV = 1 - THRESHOLD;

	// window.console.log(gamepadId, gamepad);

	this.gamepad = gamepad;
	var previousState = undefined;


	var gamepadChanged = function (gamepadId, gamepad) {
		newState = {axes: [], buttons: []};
		for (var i = 0; i < gamepad.axes.length; i++) {
			var axeValue = 0;
			if (Math.abs(gamepad.axes[i]) > THRESHOLD) {
				axeValue = gamepad.axes[i];
				if (axeValue > 0) {
					axeValue -= THRESHOLD;
				} else {
					axeValue += THRESHOLD;
				}
				axeValue /= THRESHOLDINV;
			}
			newState.axes[i] = axeValue;
		}
		for (var i = 0; i < gamepad.buttons.length; i++) {
			newState.buttons[i] = {
				pressed: gamepad.buttons[i].pressed,
				value: gamepad.buttons[i].value
			};
		}
		if (previousState !== undefined) {
			compareStates(previousState, newState);
		}
		previousState = newState;
	}

	var compareStates = function (previousState, newState) {
		for (var i = 0; i < newState.buttons.length; i++) {
			if (newState.buttons[i].pressed && !previousState.buttons[i].pressed) {
				buttonDown(i);
			} else if (!newState.buttons[i].pressed && previousState.buttons[i].pressed) {
				buttonUp(i);
			}
		}

		updateMovementFromKeys(newState);
	}

	var buttonDown = function (buttonId) {
		switch (buttonId) {
			case 0:
				// A
				player.jump();
				break;
			case 1:
				// B
				player.prepareGrenade();
				// player.rush();
				break;
			case 2:
				// X
				player.isInteracting = true;
				break;
			case 3:
				// X
				player.startReloading();
				break;
			case 4:
				// LB
				// player.toggleRotationMode();
				// player.isSniping = true;
				player.startMeleeAttacking();
				break;
			case 5:
				// RB
				player.startShooting();
				break;
			case 7:
				// RT
				break;
			case 9:
				// START
				PRESKISO.GameController.pause(gamepadId);
				break;
			case 10:
				// LJOY
				player.toggleMovementMode();
				break;
			case 11:
				// RJOY
				player.toggleRotationMode(true);
				break;
			case 12:
				// DPAD UP
				player.equipWeapon(PRESKISO.Constants.WeaponTypes.HANDGUN);
				break;
			case 13:
				// DPAD DOWN
				player.equipWeapon(PRESKISO.Constants.WeaponTypes.HEAVY);
				break;
			case 14:
				// DPAD LEFT
				player.equipWeapon(PRESKISO.Constants.WeaponTypes.SHOTGUN);
				break;
			case 15:
				// DPAD RIGHT
				player.equipWeapon(PRESKISO.Constants.WeaponTypes.RIFLE);
				break;
		}
	}

	var buttonUp = function (buttonId) {
		switch (buttonId) {
			case 1:
				// B
				player.throwGrenade();
				break;
			case 4:
				// LB
				// player.toggleRotationMode();
				// player.isSniping = false;
				break;
			case 5:
				// RB
				player.stopShooting();
				break;
			case 7:
				// RT
				break;
		}
	}

	var updateMovementFromKeys = function (newState) {
		var movementFromKeys = new THREE.Vector3(-newState.axes[0], 0, -newState.axes[1]);
		player.setMovementFromKeys(movementFromKeys);

		var target = new THREE.Vector3(-newState.axes[2], 0, -newState.axes[3]);
		if (player.movementRelativeToRotation && target.lengthSq() == 0) {
			target = new THREE.Vector3(-newState.axes[0], 0, -newState.axes[1]);
		}

		target.transformDirection(cameraFlat.matrixWorld).setY(0).multiplyScalar(-1).normalize();
		var actualTarget = new THREE.Vector3().addVectors(target, player.object.position);
		
		actualTarget.y = player.object.position.y;

		player.setTarget(actualTarget);

		var rotationFromKeys = new THREE.Vector3(Math.pow(newState.buttons[7].value - newState.buttons[6].value, 5) * Math.PI / 3.5, -newState.axes[2], 0);
		// var rotationFromKeys = new THREE.Vector3(newState.axes[3], -newState.axes[2], 0);
		player.setRotationFromKeys(rotationFromKeys);
	}
	gamepadSupport.addGamepadObserver(gamepadId, gamepadChanged);
}
