PRESKISO.OrthographicCameraController = function (camera, offset, trackedObjects) {
	var trackedObjects = trackedObjects || [];
	// this.offset = offset || new THREE.Vector3(0, 15, -15);
	this.offset = offset || new THREE.Vector3(-30, 20, -30);
	this.target = undefined;
	this.dist = undefined;
	var cameraProxy = new THREE.Object3D();

	camera.position.copy(this.offset);
	camera.position.y += 15;
	camera.lookAt(new THREE.Vector3(0, 0, 0));
	
	cameraFlat.position.copy(this.offset).multiplyScalar(-1);
	cameraFlat.position.y = -cameraFlat.position.y + 0.05;
	cameraFlat.lookAt(new THREE.Vector3(0, 0, 0));
	cameraFlat.updateMatrixWorld();

	this.addTrackedObject = function (object) {
		trackedObjects.push(object);
	};

	this.removeTrackedObject = function (object) {
		PRESKISO.Util.arrayRemoveObjectWithAttribute(trackedObjects, "id", object.id);
	}

	function lerp (origin, dest, factor) {
		return origin * (1-factor) + dest * factor;
	}

	this.tick = function (delta, time) {
		if (trackedObjects.length == 0) {
			return;
		}
		var sceneMinX = 1000, sceneMaxX = -1000,
			sceneMinZ = 1000, sceneMaxZ = -1000;
		var minX = 1000, maxX = -1000,
			minY = 1000, maxY = -1000;
		var points = [];
		var position = new THREE.Vector3();
		for (var i = 0; i < trackedObjects.length; i++) {
			position.setFromMatrixPosition( trackedObjects[i].matrixWorld );
			sceneMinX = Math.min(sceneMinX, position.x);
			sceneMaxX = Math.max(sceneMaxX, position.x);
			sceneMinZ = Math.min(sceneMinZ, position.z);
			sceneMaxZ = Math.max(sceneMaxZ, position.z);
		}
		// cameraProxy.position.copy(camera.position);
		cameraProxy.rotation.copy(camera.rotation);

		cameraProxy.position.set((sceneMaxX + sceneMinX) / 2, 20, (sceneMaxZ + sceneMinZ) / 2).add(this.offset);
		cameraProxy.updateMatrixWorld();


		// var zVector = new THREE.Vector3(1, 1, 0);
		// camera.worldToLocal(camera);
		for (var i = 0; i < trackedObjects.length; i++) {
			position = new THREE.Vector3();
			position.setFromMatrixPosition( trackedObjects[i].matrixWorld );
			cameraProxy.worldToLocal(position);
			// position.projectOnVector(zVector);
			points.push(position);
			minX = Math.min(minX, position.x);
			maxX = Math.max(maxX, position.x);
			minY = Math.min(minY, position.y);
			maxY = Math.max(maxY, position.y);
		}

		var center = new THREE.Vector2(0, 0);
		var maxDist = 30;
		for (var i = 0; i < points.length; i++) {
			var dist = center.distanceTo(points[i]);
			if (dist > maxDist) {
				maxDist = dist;
			}
		}
		// var targetHeight = sceneMaxZ - sceneMinZ;
		// var targetWidth = sceneMaxX - sceneMinX;
		// window.console.log("WINDOW: " + minX + "," + minY + " to " + maxX + "," + maxY);
		// var targetHeight = Math.max((minY + maxY) / 2, 10);
		// var targetWidth = Math.max((minX + maxX) / 2, 10);
		targetHeight = targetWidth = maxDist * 1.1;
		var aspect = targetWidth / targetHeight;
		var ratioDiff = aspect / camera.aspect;
		if (ratioDiff < 1) {
			targetWidth /= ratioDiff;
		} else if (ratioDiff > 1) {
			targetHeight /= ratioDiff;
		}
		targetLeft = -targetWidth / 2;
		targetRight = targetWidth / 2;
		targetBottom = -targetHeight / 2;
		targetTop = targetHeight / 2;

		var lerpFactor = delta * 4;
		camera.left = lerp(camera.left, targetLeft, lerpFactor);
		camera.right = lerp(camera.right, targetRight, lerpFactor);
		camera.top = lerp(camera.top, targetTop, lerpFactor);
		camera.bottom = lerp(camera.bottom, targetBottom, lerpFactor);
		camera.position.lerp(cameraProxy.position, lerpFactor);
		camera.updateProjectionMatrix();

		// var target = new THREE.Vector3((minX+maxX) / 2, 0, (minZ+maxZ) / 2);
		// var dist = Math.max((maxX - minX) / camera.aspect, (maxZ - minZ)) / 1.5;
		// // var dist = Math.sqrt(Math.pow(maxX - minX, 2), Math.pow(maxZ - minZ, 2));
		// if (this.target !== undefined) {
		// 	var lerpFactor = delta * 6;
		// 	this.target.lerp(target, delta);
		// 	this.dist = this.dist * (1-delta) + dist * delta;
		// } else {
		// 	this.target = target;
		// 	this.dist = dist;
		// }
		// var position = new THREE.Vector3().addVectors(this.target, this.offset);
		// // var dist = Math.sqrt((maxX - minX) * (maxX - minX) + (maxZ - minZ) * (maxZ - minZ));
		// // var alpha = (180 - camera.fov) / 2;
		// // alpha = alpha / 180 * Math.PI;
		// // var a = dist * Math.sin(alpha) / Math.sin(alpha * 2);
		// // position.y += a;

		// var cameraDistance = this.dist / 2 * Math.tan((180 - camera.fov) / 2 / 180 * Math.PI);
		// position.y += cameraDistance;
		// position.y = Math.max(20, position.y);

		// // window.console.log(position.y);

		// // camera.position.lerp(position, delta * 2);
		// camera.position.copy(position);
		// // camera.position.set(-20, 20, -20);
		// camera.lookAt(this.target);
	};
}
