PRESKISO.CameraController = (function () {
	THREE.Vector3.prototype.toString = function () {
		return "(" + this.x + ", " + this.y + ", " + this.z + ")";
	}

	function BoundingSphere() {
		this.center = new THREE.Vector3();
		this.radius = 0;

		this.init = function (center, radius) {
			this.center.copy(center);
			this.radius = radius;
		}

		var diffVector = new THREE.Vector3(), v1 = new THREE.Vector3(), v2 = new THREE.Vector3();
		this.addSphere = function (otherCenter, otherRadius) {
			diffVector.subVectors(this.center, otherCenter);
			var diffLength = diffVector.length();
			if (diffLength + otherRadius < this.radius) {

			} else if (diffLength + this.radius < otherRadius) {
				this.center.copy(otherCenter);
				this.radius = otherRadius;
			} else {
				v1.copy(diffVector).setLength(this.radius);
				v2.copy(diffVector).setLength(otherRadius);//.multiplyScalar(-1);
				diffVector.add(v1).add(v2);
				// window.console.log(this.center.toString(), otherCenter.toString(), v1.toString(), v2.toString());
				diffVector.divideScalar(2);
				this.radius = diffVector.length();
				this.center.subVectors(otherCenter, v2).add(diffVector);
			}
		}
	}
	var position = new THREE.Vector3();
	var cameraPosition = new THREE.Vector3();
	function CameraController(camera, offset, trackedObjects) {
		var trackedObjects = trackedObjects || [];
		// this.offset = offset || new THREE.Vector3(0, 15, -15);
		this.offset = offset || new THREE.Vector3(3, 10, -3);
		this.target = undefined;
		this.dist = undefined;

		camera.position.copy(this.offset);
		// camera.position.y = 0;
		camera.lookAt(new THREE.Vector3(0, 0, 0));
		cameraFlat.position.copy(this.offset).multiplyScalar(-1);
		cameraFlat.position.y = -cameraFlat.position.y + 0.05;
		cameraFlat.lookAt(new THREE.Vector3(0, 0, 0));
		cameraFlat.updateMatrixWorld();

		this.boundingSphere = new BoundingSphere();
		// var sphere = new THREE.Mesh(
		// 	new THREE.SphereGeometry(1, 16, 8),
		// 	new THREE.MeshBasicMaterial({color: 0xFF0000, transparent: true, opacity: 0.5, side: THREE.BackSide})
		// 	);
		// scene.add(sphere);

		this.addTrackedObject = function (object) {
			trackedObjects.push(object);
		};

		this.removeTrackedObject = function (object) {
			PRESKISO.Util.arrayRemoveObjectWithAttribute(trackedObjects, "id", object.id);
		}

		this.tick = function (delta, time) {
			if (trackedObjects.length == 0) {
				return;
			}
			// 
			// var cameraPosition = new THREE.Vector3();
			position.setFromMatrixPosition( trackedObjects[0].matrixWorld )
			this.boundingSphere.init(position, 4);
			for (var i = 1; i < trackedObjects.length; i++) {
				position.setFromMatrixPosition( trackedObjects[i].matrixWorld )
				// cameraPosition.add(position);
				this.boundingSphere.addSphere(position, 4);
			}

			cameraPosition.copy(this.boundingSphere.center);
			var fov = camera.fov * Math.PI / 180;
			// if (camera.aspect > 1) {
			// 	fov /= camera.aspect;
			// }
			var distanceToCenter = Math.max(20, this.boundingSphere.radius) / Math.sin(fov / 2);
			var vector = new THREE.Vector3(0, 0, 1);
			camera.localToWorld(vector);
			vector.sub(camera.position);
			vector.multiplyScalar(distanceToCenter);
			cameraPosition.add(vector);

			var lerpFactor = delta * 5;
			camera.position.lerp(cameraPosition, lerpFactor);

			// this.boundingSphere.radius -= 1.1;
			// var newScale = sphere.scale.x * (1 - lerpFactor) + (this.boundingSphere.radius - 1.1) * lerpFactor;
			// sphere.scale.set(newScale, newScale, newScale);
			// sphere.position.lerp(this.boundingSphere.center, lerpFactor);
		};
	}
	return CameraController;
})();
