PRESKISO.Team = function (teamId, name, color) {
	this.id = teamId;
	this.name = name;
	this.color = color;
	this.flagHolder = undefined;
	this.flag = undefined;
	this.score = 0;
	this.spawnAreas = [];
	this.players = [];

	this.init = function () {
	}

	this.addSpawningArea = function (center, radius) {
		this.spawnAreas.push({
			center: center,
			radius: radius
		});
	}

	this.getSpawningPoint = function () {
		var spawnArea = this.spawnAreas[Math.floor(Math.random() * this.spawnAreas.length)];
		var point = new THREE.Vector3(0, 0, 2 + Math.random() * (spawnArea.radius - 2));
		point.applyAxisAngle(THREE.Y_AXIS, Math.random() * Math.PI * 2).add(spawnArea.center);
		return point;
	}

	this.tick = function (delta, time) {
		this.kills = 0;
		this.deaths = 0;
		this.score = 0;
		for (var i = 0; i < this.players.length; i++) {
			this.kills += this.players[i].kills;
			this.deaths += this.players[i].deaths;
			this.score += this.players[i].score;
		}
	}

	this.addPlayer = function (player) {
		player.setTeam(this);
		this.players.push(player);
	}

	this.removePlayer = function (player) {
		this.players.splice(this.players.indexOf(player), 1);
	}

	this.getPlayers = function () {
		return this.players;
	}
}
