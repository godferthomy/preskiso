(function () {
	PRESKISO.WallGenerator = function (gameController, sizeFactor) {

		// var height = 5.5, sizeFactor = 4, thickness = 1/*0.2*/, doorThickness = .05;

		var height = 6.5, thickness = 0.17, doorThickness = .05;
		var doorSize = 0.9, doorSpacing = (1 - doorSize) / 2;
		var elements = {};
		elements[PRESKISO.WallGenerator.Types.WALL] = {
			openStart: thickness / 2,
			openEnd: thickness / 2,
			connectedStart: 0,
			connectedEnd: 0,
		};
		elements[PRESKISO.WallGenerator.Types.FENCE] = {
			openStart: thickness * .15,
			openEnd: thickness * .15,
			connectedStart: 0,
			connectedEnd: 0,
		};
		elements[PRESKISO.WallGenerator.Types.WINDOW] = {
			connectedStart: 0,
			connectedEnd: 0,
			forceSurrounding: PRESKISO.WallGenerator.Types.WALL,
		};
		elements[PRESKISO.WallGenerator.Types.DOOR] = {
			connectedStart: doorSpacing,
			connectedEnd: doorSpacing,
			forceSurrounding: PRESKISO.WallGenerator.Types.WALL,
		};
		elements[PRESKISO.WallGenerator.Types.FENCE_DOOR] = {
			connectedStart: doorSpacing,
			connectedEnd: doorSpacing,
			forceSurrounding: PRESKISO.WallGenerator.Types.FENCE,
		};

		this.currentElement = undefined;
		this.wall = undefined;

		this.startElement = function (type, position, size, rotated, valid) {
			this.currentElement = {type: type, position: position, size: size, rotated: rotated, valid: valid};
		}

		this.resetElement = function () {
			this.currentElement = undefined;
		}

		this.addElement = function (size, avoidValidating) {
			if (!avoidValidating) {
				this.currentElement.valid = true;
			}
			this.currentElement.size += size;
		}

		this.endElement = function (connected) {
			var connectedEnd;
			if (connected) {
				connectedEnd = elements[this.currentElement.type].connectedEnd;
				this.currentElement.size -= connectedEnd;
			} else {
				if (this.currentElement.size - elements[this.currentElement.type].openStart - elements[this.currentElement.type].openEnd <= 0) {
					this.currentElement.size = 0;
				}
			}
			if (this.currentElement.size > 0) {
				if (!this.currentElement.rotated) {
					this.currentElement.position.x += this.currentElement.size / 2;
				} else {
					// var p = this.currentElement.position;
					// var x = p.x;
					// p.x = p.y;
					// p.y = x;
					this.currentElement.position.y += this.currentElement.size / 2;
				}
				this.wallConfig.push(this.currentElement);
			}
			this.currentElement = undefined;
			return connectedEnd;
		}

		this.generate = function (map, callback) {
			this.currentElement = undefined;
			this.wallConfig = [];
			var wallConfiguration = map;
			var wallConfig = [];

			var wall;
			for (var y = 0; y < wallConfiguration[0].length; y++) {
				for (var x = 0; x < wallConfiguration.length; x++) {
					var tile = wallConfiguration[x][y];
					if (PRESKISO.WallGenerator.BuiltTypes.indexOf(tile) >= 0) {
						if (this.currentElement == undefined) {
							var open = elements[tile].openStart;
							this.startElement(tile, {x: x + 0.5 - open, y: y + 0.5}, 0.5 + open, false);
						} else if (this.currentElement.type == tile) {
							this.addElement(1);
						} else {
							// if (elements[this.currentElement].forceSurrounding != undefined && elements[this.currentElement].forceSurrounding != tile) {

							// }
							if (elements[tile].forceSurrounding != undefined && this.currentElement.type != elements[tile].forceSurrounding) {
								var connectedEnd = this.endElement(true);
								this.startElement(elements[tile].forceSurrounding, {x: x - connectedEnd, y: y + 0.5}, elements[tile].connectedStart + connectedEnd, false);
							} else {
								this.addElement(elements[tile].connectedStart);
							}
							var connectedEnd = this.endElement(true);
							this.startElement(tile, {x: x - connectedEnd + elements[tile].connectedStart, y: y + 0.5}, connectedEnd + 1 - elements[tile].connectedStart, false);
						}
					} else {
						if (this.currentElement != undefined) {
							this.addElement(-0.5 + elements[this.currentElement.type].openEnd);
							this.endElement();
						}
					}
				}
			}
			
			for (var x = 0; x < wallConfiguration.length; x++) {
				for (var y = 0; y < wallConfiguration[0].length; y++) {
					var tile = wallConfiguration[x][y];
					if (PRESKISO.WallGenerator.BuiltTypes.indexOf(tile) >= 0) {
						if (this.currentElement == undefined) {
							if (elements[tile].openStart !== undefined) {
								var start = 0//elements[tile].openStart;
								this.startElement(tile, {y: y + 0.5 - start, x: x + 0.5}, 0.5 + start, true);
							}
						} else if (this.currentElement.type == tile) {
							this.addElement(1);
						} else {
							// if (elements[this.currentElement].forceSurrounding != undefined && elements[this.currentElement].forceSurrounding != tile) {

							// }
							if (elements[tile].forceSurrounding != undefined && this.currentElement.type != elements[tile].forceSurrounding) {
								var connectedEnd = this.endElement(true);
								this.startElement(elements[tile].forceSurrounding, {y: y - connectedEnd, x: x + 0.5}, elements[tile].connectedStart + connectedEnd, true);
							} else {
								this.addElement(elements[tile].connectedStart);
							}
							var connectedEnd = this.endElement(true);
							this.startElement(tile, {y: y - connectedEnd + elements[tile].connectedStart, x: x + 0.5}, connectedEnd + 1 - elements[tile].connectedStart, true);
						}
					} else {
						if (this.currentElement != undefined) {
							var end = elements[this.currentElement.type].openEnd;
							this.addElement(-0.5 + end);
							this.endElement();
						}
					}
				}
			}


			// for (var y = 0; y < wallConfiguration.length; y++) {
			// 	for (var x = 0; x < wallConfiguration[y].length; x++) {
			// 		var wallTile = wallConfiguration[y][x];
			// 		if (wallTile === X) {
			// 			if (this.wall !== undefined) {
			// 				this.addWall(1);
			// 			} else {
			// 				this.startWall("h", {x: x + 0.5 - thickness / 2, y: y + 0.5}, 0.5 + thickness / 2);
			// 			}
			// 		} else if (this.wall !== undefined) {
			// 			if (wallTile === 0) {
			// 				this.addWall(-0.5 + thickness / 2, true);
			// 				this.endWall();
			// 			} else if (wallTile === D) {
			// 				this.addWall(doorSpacing);
			// 				this.endWall();
			// 				this.wallConfig.push({type: "hDoor", position: {x: x + 0.5, y: y + 0.5}, size: doorSize});
			// 				this.startWall("h", {x: x + 1 - doorSpacing, y: y + 0.5}, doorSpacing);
			// 			} else if (wallTile === W) {
			// 				this.endWall(true);
			// 				this.wallConfig.push({type: "hWindow", position: {x: x + 0.5, y: y + 0.5}, size: 1});
			// 				this.startWall("h", {x: x + 1, y: y + 0.5}, 0);
			// 			}
			// 		}
			// 	}
			// }
			
			// for (var x = 0; x < wallConfiguration[0].length; x++) {
			// 	for (var y = 0; y < wallConfiguration.length; y++) {
			// 		var wallTile = wallConfiguration[y][x];
			// 		if (wallTile === X) {
			// 			if (this.wall !== undefined) {
			// 				this.addWall(1);
			// 			} else {
			// 				this.startWall("v", {x: x + 0.5, y: y + 0.5 + thickness / 2}, 0.5 - thickness / 2);
			// 			}
			// 		} else if (this.wall !== undefined) {
			// 			if (wallTile === 0) {
			// 				this.addWall(-0.5 - thickness / 2, true);
			// 				this.endWall();
			// 			} else if (wallTile === D) {
			// 				this.addWall(doorSpacing);
			// 				this.endWall();
			// 				this.wallConfig.push({type: "vDoor", position: {x: x + 0.5, y: y + 0.5}, size: doorSize});
			// 				this.startWall("v", {x: x + 0.5, y: y + 1 - doorSpacing}, doorSpacing);
			// 			} else if (wallTile === W) {
			// 				this.endWall(true);
			// 				this.wallConfig.push({type: "vWindow", position: {x: x + 0.5, y: y + 0.5}, size: 1});
			// 				this.startWall("v", {x: x + 0.5, y: y + 1}, 0);
			// 			}
			// 		}
			// 	}
			// }

			// window.console.log(this.wallConfig);
			// }

			thickness *= sizeFactor;
			doorThickness *= sizeFactor;

	        for (var i = 0; i < this.wallConfig.length; i++) {
	        	var config = this.wallConfig[i];
	        	config.position.x -= 0.5;
	        	config.position.y -= 0.5;
	        	if (config.type == PRESKISO.WallGenerator.Types.WALL) {
	        		var dimensions = new THREE.Vector3();
	        		var position = new THREE.Vector3(config.position.x * sizeFactor, height / 2, config.position.y * sizeFactor);
		        	if (!config.rotated) {
		        		dimensions.set(config.size * sizeFactor/* + thickness*/, height, thickness);
		        	} else {
		        		dimensions.set(thickness, height, config.size * sizeFactor/* + thickness*/);
		        	}
	    			var wallEntity = new PRESKISO.WallEntity(dimensions, position);
				    gameController.addEntity(wallEntity);
				} else if (config.type == PRESKISO.WallGenerator.Types.FENCE) {
	        		var dimensions = new THREE.Vector3();
	        		var position = new THREE.Vector3(config.position.x * sizeFactor, height / 4, config.position.y * sizeFactor);
		        	if (!config.rotated) {
		        		dimensions.set(config.size * sizeFactor/* + thickness*/, height / 2, thickness / 2);
		        	} else {
		        		dimensions.set(thickness / 2, height / 2, config.size * sizeFactor/* + thickness*/);
		        	}
	    			var wallEntity = new PRESKISO.FenceEntity(dimensions, position);
				    gameController.addEntity(wallEntity);
				} else if (config.type == PRESKISO.WallGenerator.Types.WINDOW) {
					var dimensions = new THREE.Vector3();
		        	if (!config.rotated) {
		        		dimensions.set(config.size * sizeFactor/* + thickness*/, height - 4.7, thickness);
		        	} else {
		        		dimensions.set(thickness, height - 4.7, config.size * sizeFactor/* + thickness*/);
		        	}
	    			var wallEntity = new PRESKISO.WallEntity(dimensions, new THREE.Vector3(config.position.x * sizeFactor, height - dimensions.y / 2, config.position.y * sizeFactor));
				    gameController.addEntity(wallEntity);
				    dimensions = dimensions.clone();
					dimensions.y = 1.75;
	    			var wallEntity = new PRESKISO.WallEntity(dimensions, new THREE.Vector3(config.position.x * sizeFactor, dimensions.y / 2, config.position.y * sizeFactor));
				    gameController.addEntity(wallEntity);
				} else if (config.type == PRESKISO.WallGenerator.Types.DOOR) {
					var position = new THREE.Vector3(config.position.x * sizeFactor, 2.5, config.position.y * sizeFactor);
					var doorSize = new THREE.Vector3(config.size * sizeFactor, 5, doorThickness);
					var rotation, wallDimensions;
					if (!config.rotated) {
						rotation = Math.PI;
						wallDimensions = new THREE.Vector3(config.size * sizeFactor, height - 5.05, thickness);
					} else {
						rotation = Math.PI / 2;
						wallDimensions = new THREE.Vector3(thickness, height - 5.05, config.size * sizeFactor);
					}
					var door = new PRESKISO.WoodenHingeDoorEntity(doorSize, position, rotation, Math.PI / 1.5);
					gameController.addEntity(door);

	    			var wallEntity = new PRESKISO.WallEntity(wallDimensions, new THREE.Vector3(config.position.x * sizeFactor, height - wallDimensions.y / 2, config.position.y * sizeFactor));
				    gameController.addEntity(wallEntity);
				} else if (config.type == PRESKISO.WallGenerator.Types.FENCE_DOOR) {
					var position = new THREE.Vector3(config.position.x * sizeFactor, height / 4, config.position.y * sizeFactor);
					var doorSize = new THREE.Vector3(config.size * sizeFactor, height / 2, doorThickness);
					var rotation, wallDimensions;
					if (!config.rotated) {
						rotation = Math.PI;
					} else {
						rotation = Math.PI / 2;
					}
					var door = new PRESKISO.FenceHingeDoorEntity(doorSize, position, rotation, Math.PI / 1.5);
					gameController.addEntity(door);
				}
				if (i % 10 == 0) {
					scene.simulate( 0.1, 1 );
				}
	        }
		}


		this.mapFromString = function (stringMap) {
			var map = [];
			var stringLines = stringMap.split("\n");
			for (var x = 0; x < stringLines.length; x++) {
				if (stringLines[x] == "") {
					continue;
				}
				map[x] = [];
				var elements = stringLines[x].split(" ");
				for (var y = 0; y < elements.length; y++) {
					map[x][y] = PRESKISO.WallGenerator.Types[elements[y]];
				}
			}
			return map;
		}
	}

	PRESKISO.WallGenerator.Types = {
		".": 0, " ": 0, GROUND: 0, // GROUND
		P: 100, PLAYER: 100, // Player spawn
		Z: 101, ZOMBIE: 101, // Zombie spawn
		"+": 150, HEALTH: 150, // Healing powerups
		"*": 151, AMMO: 151, // Ammo powerups
		B: 500, BARREL: 500, // Barrel decoration
		D: 800, DOOR: 800, DOOR_CLOSED: 800, // DOOR
		DOOR_OPEN: 801, // DOOR
		T: 810, FENCE_DOOR: 810, // FENCE DOOR
		X: 1000, WALL: 1000, // WALL
		W: 1001, WINDOW: 1001, // WINDOW
		H: 1002, FENCE: 1002, // FENCE
		Y: 1003, TREE: 1003, // TREE
	};

	PRESKISO.WallGenerator.BuiltTypes = [
		PRESKISO.WallGenerator.Types.DOOR,
		PRESKISO.WallGenerator.Types.WALL,
		PRESKISO.WallGenerator.Types.WINDOW,
		PRESKISO.WallGenerator.Types.FENCE,
		PRESKISO.WallGenerator.Types.FENCE_DOOR,
	];
})();
