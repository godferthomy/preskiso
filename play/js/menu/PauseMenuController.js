PRESKISO.PauseMenuController = (function () {
	function PauseMenuController($el) {
		PRESKISO.MenuController.call(this, $el);
	}
	PRESKISO.Util.inherit(PauseMenuController, PRESKISO.MenuController);

	PauseMenuController.prototype.init = function () {
		this.parent.init.call(this, [
			{ id: "pouet", content: "Croustipouet" },
			{ id: "quit", content: "Quit" },
		]);
	}

	PauseMenuController.prototype.onGamepadButtonDown = function (e) {
		switch (e.control) {
			case "START_FORWARD":
				PRESKISO.GameController.resume();
				break;
			default:
				this.parent.onGamepadButtonDown.call(this, e);
		}
	}

	PauseMenuController.prototype.activateMenuItem = function ($el) {
		$el = $el || this.$selectedMenuItem;
		switch ($el.attr("id")) {
			case "pause_menu_quit":
				// window.location.href = "..";
				$("#viewport").fadeOut(900);
				var $form = $('<form action=".." method="get"></form>');
				$(document.body).append($form);
				setTimeout(function () {
					$form.submit();
				}, 800);
				break;
			default:
				this.parent.activateMenuItem.call(this, $el);
		}
	}

	return PauseMenuController;
})();
