(function () {
	function PlayerHudController(player, playerContainer, playerScoresContainer) {
		this.player = player;
		var backgroundColor = player.color.clone().lerp(new THREE.Color(0, 0, 0), 0.8);
		this.currentValues = {};
		this.el = $('<div style="border-color: #' + player.color.getHexString() + '; background-color: #' + backgroundColor.getHexString() + '" id="player' + player.playerId + '" class="hudPanel player">'
			+ '<img class="playerIcon" src="../common/images/teteDeGelule-' + player.model + '.png" style="background-color: #' + player.color.getHexString() + '; border-color: #' + player.color.getHexString() + '" />'
			+ '<div class="playerName">' + player.name + '</div>'
			+ '<br />'
			+ '<div class="playerWeapon">'
				+ '<div class="playerWeaponName"></div>'
				+ '<img src="images/ammo_icon.png" />'
				+ '<span class="playerWeaponAmmo"></span>'
			+ '</div>'
			+ '<div class="meter playerHealth" style="border-color: #' + player.color.getHexString() + '"><span class="playerHealthBar"></span></div>'
			+ '</div>');
		playerContainer.append(this.el);

		this.scoreEl = $('<tr class="playerScore">'
            + '<td class="playerNameColumn" style="color: #' + player.team.color.getHexString() + '">' + player.name + '</td>'
            + '<td class="playerKillsColumn"></td>'
            + '<td class="playerDeathsColumn"></td>'
            + '<td class="playerScoreColumn"></td>'
            + '</tr>');
		playerScoresContainer.append(this.scoreEl);
		this.elements = {
			healthBar: this.el.find(".playerHealthBar"),
			weaponName: this.el.find(".playerWeaponName"),
			weaponAmmo: this.el.find(".playerWeaponAmmo"),
			kills: this.scoreEl.find(".playerKillsColumn"),
			deaths: this.scoreEl.find(".playerDeathsColumn"),
			score: this.scoreEl.find(".playerScoreColumn"),
		};

		this.tick = function (delta) {
			if (this.currentValues.health !== this.player.health) {
				this.elements.healthBar.css({width: this.player.health + "%"});

				var colors = [[0xFF, 0, 0], [0xCC, 0xCC, 0], [0, 0xFF, 0]];
				var health = Math.max(0, player.health / 100);
				var coeffs = [
					Math.max(0, 1 - health * 2),
					(1 - Math.abs(health - 0.5) * 2),
					Math.max(0, -1 + health * 2)
				];
				var rgb = [];
				for (var i = 0; i < 3; i++) {
					rgb[i] = 0;
					for (var j = 0; j < 3; j++) {
						rgb[i] += Math.round(colors[j][i] * coeffs[j]);
					}
				}
				var stringColor = "rgb(" + rgb.join(",") + ")";
				this.elements.healthBar.css({"width": player.health + "%", "background-color": stringColor});

				this.currentValues.health = this.player.health;
			}
			var weaponName, weaponAmmo;
			if (this.player.isCarryingEntity()) {
				weaponName = this.player.getCarriedEntity().name;
				weaponAmmo = "-";
			} else {
				var weapon = this.player.getCurrentWeapon();
				weaponName = weapon.weaponData.name;
				var magAmmo;
				if (this.player.isReloading()) {
					magAmmo = "-";
				} else {
					magAmmo = weapon.magazineAmmo;
				}
				weaponAmmo = magAmmo + "/" + weapon.ammo;
			}
			if (this.currentValues.weaponName !== weaponName) {
				this.elements.weaponName.html(weaponName);
				this.currentValues.weaponName = weaponName;
			}
			if (this.currentValues.weaponAmmo !== weaponAmmo) {
				this.elements.weaponAmmo.html(weaponAmmo);
				this.currentValues.weaponAmmo = weaponAmmo;
			}
			if (this.currentValues.kills !== this.player.kills) {
				this.elements.kills.text(this.player.kills);
				this.currentValues.kills = this.player.kills;
			}
			if (this.currentValues.deaths !== this.player.deaths) {
				this.elements.deaths.text(this.player.deaths);
				this.currentValues.deaths = this.player.deaths;
			}
			if (this.currentValues.score !== this.player.score) {
				this.elements.score.text(this.player.score);
				this.currentValues.score = this.player.score;
			}
		}
	}

	function TeamHudController(team, teamScoresContainer) {
		this.team = team;
		this.el = $('<div id="team' + team.id + '" class="team" style="color: #' + team.color.getHexString() + '">'
			+ '<div class="teamName">' + team.name + '</div>'
			+ '<div class="teamScore"></div>'
			+ '</div>'
		);
		teamScoresContainer.append(this.el);
		this.elements = {
			score: this.el.find(".teamScore")
		};
		this.currentValues = {};

		this.tick = function (delta) {
			if (this.currentValues.score !== this.team.score) {
				this.elements.score.text(this.team.score);
				this.currentValues.score = this.team.score;
			}
		}
	}

	PRESKISO.HudController = function (gameController, container) {
		var nbPlayers = 0;
		var playerHudControllers = [];
		var teamHudControllers = [];
		var playerContainer = $(container).find("#players");
		var teamScoresContainer = $(container).find("#teamScores");
		var playerScoresContainer = $(container).find("#playerScores");

		this.tick = function (delta, time) {
			var players = gameController.getPlayers();
			for (var i = nbPlayers; i < players.length; i++) {
				var player = players[i];
				createplayerStatusEls(player);
				nbPlayers++;
			}

			for (var i = 0; i < playerHudControllers.length; i++) {
				playerHudControllers[i].tick(delta);
			}
			// var teams = gameController.getTeams();
			for (var i = 0; i < teamHudControllers.length; i++) {
				teamHudControllers[i].tick(delta);
			}
		}

		this.init = function () {
			var teams = gameController.getTeams();
			for (var i = 0; i < teams.length; i++) {
				var team = teams[i];
				createTeamEls(team);
			}
		}

		var createTeamEls = function (team) {
			var teamHudController = new TeamHudController(team, teamScoresContainer);
			teamHudControllers.push(teamHudController);
		}

		var createplayerStatusEls = function (player) {
			var playerHudController = new PlayerHudController(player, playerContainer, playerScoresContainer);
			playerHudControllers.push(playerHudController);
		}
	}
})();
