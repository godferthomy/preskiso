PRESKISO.OptionsMenuController = (function () {
	function OptionsMenuController($el) {
		PRESKISO.MenuController.call(this, $el);
	}
	PRESKISO.Util.inherit(OptionsMenuController, PRESKISO.MenuController);

	OptionsMenuController.prototype.init = function () {
		this.parent.init.call(this, [
			{ id: "volume", content: 'Volume <input id="options_menu_volume_value" name="masterVolume" type="range" class="menu_item_value" min="0.05" step="0.05" max="1.0" />' },
			{ id: "back", content: "Back", options: {isBackItem: true} },
		]);

		this.$el.find("#options_menu_volume_value").change(function () {
			PRESKISO.AudioManager.setMasterVolume($(this).val());
		});
	}

	return OptionsMenuController;
})();
