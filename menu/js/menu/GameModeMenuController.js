PRESKISO.GameModeMenuController = (function () {
	function GameModeMenuController($el) {
		PRESKISO.MenuController.call(this, $el);
	}
	PRESKISO.Util.inherit(GameModeMenuController, PRESKISO.MenuController);

	GameModeMenuController.prototype.init = function () {
		this.parent.init.call(this, [
			{ id: "back", content: "Back", options: {isBackItem: true} },
		]);
		var modes = PRESKISO.Constants.GameModes;
		var $items = this.$el.find(".menu_item");
		var $lastItem = $items.eq($items.length - 1);
		for (var i = 0; i < modes.length; i++) {
			var mode = modes[i];
			var $el = this.createItem(mode.id, mode.name, {submenu: "map_menu"});
			$el.data("gamemodeIndex", i);
			$lastItem.before($el);
		}
		this.$menuItems = this.$el.find(".menu_item");
		this.assignBasicNavigation();
		this.initNavigation();
	}

	GameModeMenuController.prototype.selectMenuItem = function ($el) {
		this.parent.selectMenuItem.call(this, $el);
		gamemode = PRESKISO.Constants.GameModes[parseInt($el.data("gamemodeIndex"))];
		window.console.log(gamemode);
	}

	return GameModeMenuController;
})();
