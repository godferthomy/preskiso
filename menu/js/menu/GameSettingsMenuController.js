PRESKISO.GameSettingsMenuController = (function () {
	function GameSettingsMenuController($el) {
		PRESKISO.MenuController.call(this, $el);
		this.$title = this.$el.find("#gamesettings_menu_title");
	}
	PRESKISO.Util.inherit(GameSettingsMenuController, PRESKISO.MenuController);

	GameSettingsMenuController.prototype.init = function () {
		this.parent.init.call(this, [
			{ id: "continue", content: 'Continue', options: {submenu: "playerpick_menu"} },
			{ id: "back", content: "Back", options: {isBackItem: true} },
		]);
	}
	
	GameSettingsMenuController.prototype.show = function () {
		if (this.gamemodeId != gamemode.id) {
			this.populate();
		}
		this.parent.show.call(this);
	}

	GameSettingsMenuController.prototype.populate = function () {
		this.gamemodeId = gamemode.id;
		this.$title.text(gamemode.name);
		var $items = this.$el.find(".menu_item");
		var $lastItem = $items.eq($items.length - 1);
		var $items = $($items.splice(1, $items.length - 2));
		$items.each(function () {
			$(this).remove();
		});

		var params = gamemode.params;

		for (var i = params.length - 1; i >= 0; i--) {
			var param = params[i];
			// var $el = $('<li id="gamesettings_menu_item_' + i + '" class="menu_item">' + param.visible_name + '</li>');
			var $el = this.createItem("item_" + i, param.visible_name);
			$lastItem.before($el);
			if (param.type === "range") {
				var step = param.step || 1;
				var value = PRESKISO.SettingsManager.getGameSetting(param.name);
				if (value == undefined) {
					value = Math.round((param.max - param.min) / 2 / step) * step;
				}
				var rangeEl = $('<input type="range" name="' + param.name + '" class="menu_item_value game_setting" min="' + param.from + '" step="' + step + '" max="' + param.to + '" value="' + value + '" />');
				$el.append(rangeEl);
				var valueEl = $('<span class="menu_item_value_display">' + value + '</div>');
				$el.append(valueEl);
				rangeEl.change(function () {
					var value =  $(this).val();
					$(this).next().text(value);
					// PRESKISO.SettingsManager.setGameSetting($(this).attr("name"), value);
				})
			} else if (param.type === "boolean") {
				var value = PRESKISO.SettingsManager.getGameSetting(param.name) == "true";
				window.console.log(value);
				var checkboxEl = $('<input type="checkbox" name="' + param.name + '" class="menu_item_value game_setting" />');
				if (value) {
					checkboxEl.prop("checked", true);
				}
				$el.append(checkboxEl);
				checkboxEl.change(function () {
					// PRESKISO.SettingsManager.setGameSetting($(this).attr("name"), $(this).is(':checked'));
				})
			}
		}
		this.$menuItems = this.$el.find(".menu_item");
		this.assignBasicNavigation();
		this.initNavigation();
	}

	return GameSettingsMenuController;
})();
