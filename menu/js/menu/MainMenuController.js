PRESKISO.MainMenuController = (function () {
	function MainMenuController($el) {
		PRESKISO.MenuController.call(this, $el);
	}
	PRESKISO.Util.inherit(MainMenuController, PRESKISO.MenuController);

	MainMenuController.prototype.init = function () {
		this.parent.init.call(this, [
			{ id: "start_game", content: "Start Game", options: {submenu: "gamemode_menu"} },
			{ id: "options", content: "Settings", options: {submenu: "options_menu"} },
			{ id: "help", content: "Help" },
			{ id: "about", content: "About" },
		]);
	}

	return MainMenuController;
})();
