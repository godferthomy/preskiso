PRESKISO.PlayerPickingMenuController = (function () {
	function PlayerPickingMenuController($el) {
		PRESKISO.MenuController.call(this, $el);
		this.$playerPicker = this.$el.find("#playerpicker");
		this.$ready_message = this.$el.find("#playerpick_ready_message");
		this.$title = this.$el.find("#playerpick_menu_title");
		this.$minimumTeams = this.$el.find("#playerpick_minimum_teams");
		this.$minimumTeamsValue = this.$el.find("#playerpick_minimum_teams_value");

		this.playerEls = {};

	}
	PRESKISO.Util.inherit(PlayerPickingMenuController, PRESKISO.MenuController);

	PlayerPickingMenuController.prototype.show = function () {
		this.populate();
		this.parent.show.call(this);
	}

	PlayerPickingMenuController.prototype.populate = function () {
		this.$title.text(gamemode.name);
		minTeams = gamemode.minTeams;
		maxTeams = Math.min(map.maxTeams, gamemode.maxTeams);

		if (gamemode.minTeams > 1) {
			this.$minimumTeamsValue.text(gamemode.minTeams);
			this.$minimumTeams.show();
		} else {
			this.$minimumTeams.hide();
		}

		var nbTeams = getNbTeams();
		teams = [];
		for (var i = 0; i < nbTeams; i++) {
			teams.push({
				color: PRESKISO.Constants.PlayerColors[i],
				players: []
			});
		}
		this.$playerPicker.empty();

		players = {};

		var createPlayer = function (gamePadIndex) {
			var model = Math.floor(Math.random() * PRESKISO.Constants.PlayerModels.length);
			players[gamePadIndex] = { id: gamePadIndex, side: 0, model: model };
			var $gamepadEl = $('<img class="playerpick" id="playerPick' + gamePadIndex + '" src="common/images/teteDeGelule-' + PRESKISO.Constants.PlayerModels[model] + '.png" />');
			$gamepadEl.css({ left: (100 * (0.5) / (getNbTeams() + 1)) + "%" });
			this.playerEls[gamePadIndex] = $gamepadEl;
			this.$playerPicker.append($gamepadEl);
		}

		window.console.log(gamepads);
		createPlayer.call(this, PRESKISO.Constants.KeyboardGamepadId);
		for (var i = 0; i < gamepads.length; i++) {
			this.$playerPicker.append("<br />");
			createPlayer.call(this, gamePads[i].index);
		}
		$("#playerpick_ready_message").css({ visibility: "hidden" });
	}

	PlayerPickingMenuController.prototype.movePlayerSide = function (gamepadIndex, direction) {
		var player = players[gamepadIndex];
		var side = player.side;
		var newSide = Math.max(0, Math.min(getNbTeams(), side + direction));
		if (newSide != side) {
			if (side > 0) {
				PRESKISO.Util.arrayRemoveObjectWithAttribute(teams[side - 1].players, "id", player.id);
			}
			var color, colorString;
			if (newSide > 0) {
				var team = teams[newSide - 1];
				team.players.push(player);
				color = team.color;
				colorString = "rgb(" + [Math.round(color.r * 256), Math.round(color.g * 256), Math.round(color.b * 256)].join(",") + ")";
			} else {
				colorString = "";
			}
			player.side = newSide;
			var $gamepadEl = this.playerEls[gamepadIndex];
			$gamepadEl.stop().animate({ left: (100 * (0.5 + newSide) / (getNbTeams() + 1)) + "%" }).css({ "background-color": colorString });

			var teamsToSend = [];
			for (var i = teams.length - 1; i >= 0; i--) {
				if (teams[i].players.length > 0) {
					teamsToSend.push(teams[i]);
				}
			}
			if (teamsToSend.length >= minTeams) {
				this.$ready_message.css({ visibility: "visible" });
			} else {
				this.$ready_message.css({ visibility: "hidden" });
			}
		}
	}

	PlayerPickingMenuController.prototype.changePlayerCharacter = function (gamepadIndex, direction) {
		var player = players[gamepadIndex];
		player.model = (player.model + direction) % PRESKISO.Constants.PlayerModels.length;
		if (player.model < 0) {
			player.model += PRESKISO.Constants.PlayerModels.length;
		}
		this.playerEls[gamepadIndex].attr("src", "common/images/teteDeGelule-" + PRESKISO.Constants.PlayerModels[player.model] + ".png");
	}

	PlayerPickingMenuController.prototype.onGamepadButtonDown = function (e) {
		switch (e.control) {
			case "DPAD_UP":
				this.changePlayerCharacter(e.gamepad.index, 1);
				break;
			case "DPAD_DOWN":
				this.changePlayerCharacter(e.gamepad.index, -1);
				break;
			case "DPAD_LEFT":
				this.movePlayerSide(e.gamepad.index, -1);
				break;
			case "DPAD_RIGHT":
				this.movePlayerSide(e.gamepad.index, 1);
				break;
			case "START_FORWARD":
				startGame();
				break;
			default:
				this.parent.onGamepadButtonDown.call(this, e);
		}
	}

	PlayerPickingMenuController.prototype.onKeyDown = function (e) {
		switch (event.code) {
			case "ArrowUp":
				this.changePlayerCharacter(PRESKISO.Constants.KeyboardGamepadId, 1);
				break;
			case "ArrowDown":
				this.changePlayerCharacter(PRESKISO.Constants.KeyboardGamepadId, -1);
				break;
			case "ArrowLeft":
				this.movePlayerSide(PRESKISO.Constants.KeyboardGamepadId, -1);
				break;
			case "ArrowRight":
				this.movePlayerSide(PRESKISO.Constants.KeyboardGamepadId, 1);
				break;
			case "Enter":
				startGame();
				break;
			default:
				this.parent.onKeyDown.call(this, e);
		}
	}

	return PlayerPickingMenuController;
})();
