PRESKISO.MapPickingMenuController = (function () {
	function MapPickingMenuController($el) {
		PRESKISO.MenuController.call(this, $el);
		this.maps = [];
	}
	PRESKISO.Util.inherit(MapPickingMenuController, PRESKISO.MenuController);

	MapPickingMenuController.prototype.init = function () {
		this.parent.init.call(this, [
			{ id: "back", content: "Back", options: {isBackItem: true} },
		]);
	}
	
	MapPickingMenuController.prototype.show = function () {
		if (this.gamemodeId != gamemode.id) {
			this.populate();
		}
		this.parent.show.call(this);
	}

	MapPickingMenuController.prototype.populate = function () {
		this.gamemodeId = gamemode.id;
		var maps = PRESKISO.Constants.Maps;
		var $items = this.$el.find(".menu_item");
		var $lastItem = $items.eq($items.length - 1);
		var $items = $($items.splice(0, $items.length - 1));
		$items.each(function () {
			$(this).remove();
		});
		for (var i = 0; i < maps.length; i++) {
			if (maps[i].gamemodes.indexOf(gamemode.id) >= 0) {
				var map = maps[i];

				var $el = this.createItem(map.id, map.name, {submenu: "gamesettings_menu"});
				$el.data("mapIndex", map.id);
				$lastItem.before($el);
			}
		}
		this.$menuItems = this.$el.find(".menu_item");
		this.assignBasicNavigation();
		this.initNavigation();
	}

	MapPickingMenuController.prototype.selectMenuItem = function ($el) {
		this.parent.selectMenuItem.call(this, $el);
		map = PRESKISO.Constants.Maps[parseInt($el.data("mapIndex"))];
	}

	return MapPickingMenuController;
})();
