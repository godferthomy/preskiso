PRESKISO.WelcomeMenuController = (function () {
	function WelcomeMenuController($el) {
		PRESKISO.MenuController.call(this, $el);
	}
	PRESKISO.Util.inherit(WelcomeMenuController, PRESKISO.MenuController);

	WelcomeMenuController.prototype.activateMenuItem = function ($el) {
		PRESKISO.MenuNavigationController.showMenu("main_menu");
	}

	return WelcomeMenuController;
})();
