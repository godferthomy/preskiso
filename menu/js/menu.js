function loadAudio () {
	PRESKISO.AudioManager.rootFolder = "menu/sounds/";
    PRESKISO.AudioManager.init([
    	"menu_music", "../../play/sounds/impacts/body_impact", "../../play/sounds/success",
    	"../../common/sounds/open_menu", "../../common/sounds/close_menu", "../../common/sounds/navigate_menu" 
    	], {"../../play/sounds/impacts/body_impact": 3}, initPage);
}

function initPage () {

	gamepad = new Gamepad();

	$selection_outliner = $("#selection_outliner");
	
	$("#options_menu_volume_value").val(PRESKISO.AudioManager.getMasterVolume());


	var zombieApparitionDelay = 200;

	// ANIMATE PILLS
	setTimeout(function() {
		var pills = $(".pillphoto");
		pills.each(function( index ) {
			var destination = $(this).offset();
			var delay = Math.random() * zombieApparitionDelay + index * 100;
			if (index === pills.length - 1) {
				delay = zombieApparitionDelay + 500;
				var $el = $( this );
				setTimeout(function () {
					$el.addClass("enter_animate");
					setTimeout(function () {
						var sound = PRESKISO.AudioManager.playSound(undefined, "../../play/sounds/success", 1);
						sound.source.playbackRate.value = 1.3;
						PRESKISO.MenuNavigationController.init(menuControllers);
						PRESKISO.MenuNavigationController.listen();
						gamepad.init();
					}, 500);
					setTimeout(function () {
					}, 2000);
				}, delay);
			} else {
				var $el = $( this );
				setTimeout(function () {
					$el.addClass("enter_animate");
					setTimeout(function () {
						var sound = PRESKISO.AudioManager.playSound(undefined, "../../play/sounds/impacts/body_impact", 1);
						sound.source.playbackRate.value = 1.3;
					}, 500);
				}, delay);
			}
		});
	}, 100);

	$(document).on("change", ".menu_item_value", function () {
		var setter;
		if ($(this).hasClass("game_setting")) {
			setter = PRESKISO.SettingsManager.setGameSetting;
		} else {
			setter = PRESKISO.SettingsManager.setItem;
		}
		if ($(this).is("input[type='checkbox']")) {
			setter.call(PRESKISO.SettingsManager, $(this).attr("name"), $(this).is(':checked'));
		} else {
			setter.call(PRESKISO.SettingsManager, $(this).attr("name"), $(this).val());
		}
	});

	$("#menus .menu").each(function () {
		var controller;
		switch ($(this).attr("id")) {
			case "main_menu":
				controller = PRESKISO.MainMenuController;
				break;
			case "welcome_menu":
				controller = PRESKISO.WelcomeMenuController;
				break;
			case "options_menu":
				controller = PRESKISO.OptionsMenuController;
				break;
			case "gamemode_menu":
				controller = PRESKISO.GameModeMenuController;
				break;
			case "map_menu":
				controller = PRESKISO.MapPickingMenuController;
				break;
			case "gamesettings_menu":
				controller = PRESKISO.GameSettingsMenuController;
				break;
			case "playerpick_menu":
				controller = PRESKISO.PlayerPickingMenuController;
				break;
			default:
				controller = PRESKISO.MenuController;
		}
		var menuController = new controller($(this));
		menuController.init();
		menuControllers.push(menuController);
	});

}




var menuControllers = [];
var MENU_DIRECTIONS = {
	left: 0,
	top: 1,
	right: 2,
	bottom: 3,
};
var MENU_DIRECTIONS_DATA_NAMES = [
	"toleft",
	"totop",
	"toright",
	"tobottom",
];
var gamepads = [], $showingMenu, $selectedMenuItem, $selection_outliner;


var maxTeams = 4;
var minTeams = 1;
var isPlayerPicking = false;
// var playerSides;
var players;
var teams = [];
var gamemode;

function getNbTeams() {
	return Math.min(maxTeams, gamepads.length + 1);
}

function sortPlayers(a, b) {
	if (a.id > b.id) {
		return 1;
	} else if (a.id < b.id) {
		return -1;
	} else {
		return 0;
	}
}

function startGame() {
	var teamsToSend = [];
	for (var i = teams.length - 1; i >= 0; i--) {
		if (teams[i].players.length > 0) {
			teamsToSend.push(teams[i]);
		}
		teams[i].players.sort(sortPlayers);
	}
	
	var settings = {};
	if (teamsToSend.length >= minTeams) {
		var gameConfig = {
			gameMode: gamemode.id,
			teams: teamsToSend,
			map: map.id,
		};
		PRESKISO.SettingsManager.setItem("gameConfig", JSON.stringify(gameConfig));
		$("#page").fadeOut(900);
		$form = $('<form action="play" method="get"></form>');
		$(document.body).append($form);
		setTimeout(function () {
			$form.submit();
		}, 800);
	}
}


$( document ).ready( loadAudio );
